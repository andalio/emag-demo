<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
error_reporting(1);

include('db.class.php');

$db_dp = new db('andalio_dp');
$db_dp_mktp = new db('andalio_mktp_dp');



// se goleste tabelul andalio_mktp_dp.categories
if($db_dp_mktp->truncate('categories')){
	echo 'Tabelul andalio_mktp_dp.<b>categories</b> a fost golit.<br>';
}

// se preiau categoriile din andalio_dp.oc_category
$categories = $db_dp->getData("SELECT
	oc_category.category_id,
	oc_category.parent_id,
	oc_category_description.name,
	oc_category.status
	FROM andalio_dp.oc_category
	JOIN andalio_dp.oc_category_description ON oc_category_description.category_id = oc_category.category_id");

// se formeaza un array pentru insert in andalio_mktp_dp.categories
foreach ($categories as $category) {
	$dataToInsert[] = [
		'category_id' => $category['category_id'],
		'name' => $category['name'],
		'parent_id' => $category['parent_id'],
		'status' => $category['status']		
	];
}

// se insereaza noile categorii in andalio_mktp_dp.categories
if($db_dp_mktp->insertBulkData('categories', $dataToInsert)){
	echo 'Au fost inserate noile categorii.';
}
