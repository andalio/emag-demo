<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
error_reporting(1);

include('db.class.php');

$db_dp = new db('andalio_dp');
$db_dp_mktp = new db('andalio_mktp_dp');




// se preiau comenzile nesincronizate din andalio_dp.oc_order
$orders = $db_dp->getData("SELECT
	order_id
	FROM andalio_dp.oc_order
	WHERE synked_to_mktp = 0");


foreach($orders as $order){

	// trebuie sa verificam cati vendori sunt pe comanda
	$orderVendors = $db_dp->getData("SELECT
		o.order_id,
		p.vendor_id
		FROM andalio_dp.oc_order o
		JOIN andalio_dp.oc_order_product o_p ON o_p.order_id = o.order_id
		JOIN andalio_dp.oc_product p ON p.product_id = o_p.product_id
		WHERE o.order_id = " . $order['order_id'] . "
		GROUP BY vendor_id");


	// pentru fiecare combinatie de order_id - vendor_id generam o comanda in marketplace (comanda partiala, per vendor)
	foreach ($orderVendors as $orderVendor) {

		// preluam datele vendorului
		$vendor_data = $db_dp_mktp->getData("SELECT *
			FROM andalio_mktp_dp.users
			WHERE id = 1")[0];


		$dataToInsert = [];
		$error = false;

		// preluam datele comenzii
		$orderData = $db_dp->getData("SELECT *, (SELECT shipping_cost FROM andalio_dp.oc_vendor WHERE vendor_id = " . $orderVendor['vendor_id'] . ") AS shipping_cost
			FROM andalio_dp.oc_order o
			WHERE o.order_id = " . $order['order_id'])[0];

		$dataToInsert[] = [
			'dp_order_id' => $orderData['order_id'],
			'vendor_id' => $orderVendor['vendor_id'],
			'firstname' => $orderData['firstname'],
			'lastname' => $orderData['lastname'],
			'email' => $orderData['email'],
			'telephone' => $orderData['telephone'],

			'payment_firstname' => $orderData['payment_firstname'],
			'payment_lastname' => $orderData['payment_lastname'],
			'payment_company' => $orderData['payment_company'],
			'payment_address_1' => $orderData['payment_address_1'],
			'payment_address_2' => $orderData['payment_address_2'],
			'payment_city' => $orderData['payment_city'],
			'payment_country' => $orderData['payment_country'],
			'payment_zone' => $orderData['payment_zone'],
			'payment_zone_id' => $orderData['payment_zone_id'],
			'payment_method' => $orderData['payment_method'],
			'payment_code' => $orderData['payment_code'],
			
			'shipping_firstname' => $orderData['shipping_firstname'],
			'shipping_lastname' => $orderData['shipping_lastname'],
			'shipping_company' => $orderData['shipping_company'],
			'shipping_address_1' => $orderData['shipping_address_1'],
			'shipping_address_2' => $orderData['shipping_address_2'],
			'shipping_city' => $orderData['shipping_city'],
			'shipping_country' => $orderData['shipping_country'],
			'shipping_zone' => $orderData['shipping_zone'],
			'shipping_zone_id' => $orderData['shipping_zone_id'],
			'shipping_code' => $orderData['shipping_code'],
			'comment' => $orderData['comment'],
			'total' => 0,
			'shipping_cost' => $orderData['shipping_cost'],
			'order_status_id' => 1,
			'date_added_in_dp' => $orderData['date_added']
		];

		// inseram comanda cu detaliile necesare
		$order_id = $db_dp_mktp->insertBulkData('orders', $dataToInsert);

		echo 'Order ID: ' . $order_id . '<br>';
		
		if($order_id){
			// preluam produsele aferente comenzii si vendorului curent
			$orderProductData = $db_dp->getData("SELECT
				op.product_id,
				op.name,
				op.model,
				op.quantity,
				op.price,
				op.total,
				p.weight
				FROM andalio_dp.oc_order_product op
				JOIN andalio_dp.oc_product p ON p.product_id = op.product_id
				WHERE op.order_id = " . $orderVendor['order_id'] . " AND p.vendor_id = " . $orderVendor['vendor_id']);

			$dataToInsert = [];
			$totalForOrder = 0;
			$totalWeightForOrder = 0;

			foreach($orderProductData as $product){
				$dataToInsert[] = [
					'order_id' => $order_id,
					'product_id' => $product['product_id'],
					'name' => $product['name'],
					'model' => $product['model'],
					'quantity' => $product['quantity'],
					'price' => $product['price'],
					'total' => $product['total']
				];

				// calculam totalul pentru comanda curenta
				$totalForOrder += $product['total'];
				$totalWeightForOrder += $product['weight'];
			}

			// inseram produsele aferente comenzii partiale
			if(!$db_dp_mktp->insertBulkData('orders_products', $dataToInsert)){
				$error = true;
			}

			// actualizam totalul comenzii curente
			$query = "UPDATE andalio_mktp_dp.orders
			SET total = " . $totalForOrder . ",
			weight = " . $totalWeightForOrder . "
			WHERE order_id = " . $order_id;
			$db_dp_mktp->update($query);

			// Pregatim un email ce va fi trimis catre vendor
			$data_email = [];

			$data_email[] = [
				'name_from' => 'DecoPlus Marketplace',
				'mail_from' => 'marketplace@decoplus.ro',
				'name_to' => $vendor_data['company'],
				'mail_to' => $vendor_data['email'],
				'template' => 'mail.new_order',
				'subject' => 'Comanda noua in DecoPlus Marketplace ' . $order_id,
				'content_data' => json_encode([
					'order_id' => $order_id
				])
			];

			$email_id = $db_dp_mktp->insertBulkData('emails', $data_email);

			// Adaugam emailul in coada
			$data_email_sending = [];

			$data_email_sending[] = [
				'email_id' => $email_id
			];

			$db_dp_mktp->insertBulkData('emails_sendings', $data_email_sending);


		} else {
			$error = true;
		}

	}

	if(!$error){
		// marcam comanda ca sincronizata: andalio_dp.synked_to_mktp = 0
		$query = "UPDATE andalio_dp.oc_order
		SET synked_to_mktp = 1
		WHERE order_id = " . $order['order_id'];
		$db_dp->update($query);
	} else {
		echo '<br>Au aparut erori la inserarea noilor comenzi.';
		exit;
	}

}

if(!$error && $orders){
	echo '<br>Au fost inserate noile comenzi.';
} else {
	echo '<br>Nu sunt comenzi de sincronizat.';
}