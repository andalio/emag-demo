<?php

class db {

    private $conn;
    private $host;
    private $user;
    private $pass;
    private $dbname;
    private $port;
    private $debug;

    function __construct($db) {

        if($db === 'andalio_dp'){
            $this->user = 'andalio_dp';
            $this->pass = '';
            
        } elseif ($db === 'andalio_mktp_dp') {
            $this->user = 'andalio_mktp_dp';
            $this->pass = '';
        }
        $this->host ='localhost';
        $this->dbname = $db;
        $this->conn = false;
        $this->port = '3306';
        $this->debug = true;
        $this->connect();
        
    }

    function __destruct() {
        $this->disconnect();
    }

    function connect() {
        if (!$this->conn) {
            $this->conn = mysqli_connect($this->host, $this->user, $this->pass);
            mysqli_select_db($this->dbname, $this->conn);
            mysqli_set_charset('utf8', $this->conn);

            if (!$this->conn) {
                $this->status_fatal = true;
                echo 'Connection failed';
                die();
            } else {
                $this->status_fatal = false;
            }
        }

        return $this->conn;
    }

    function disconnect() {
        if ($this->conn) {
            mysqli_close($this->conn);
        }
    }

    function getData($query) {
        $cnx = $this->conn;
        if (!$cnx || $this->status_fatal) {
            echo 'GetData -> Connection failed';
            die();
        }

        mysqli_query($cnx, "SET NAMES 'utf8'");
        $cur = mysqli_query($cnx, $query);
        $return = array();

        while ($data = mysqli_fetch_assoc($cur)) {
            array_push($return, $data);
        }

        return $return;
    }

    function update($query){
        $cnx = $this->conn;
        if (!$cnx || $this->status_fatal) {
            echo 'InsertData -> Connection failed';
            die();
        }

        mysqli_query($cnx, "SET NAMES 'utf8'");
        $cur = mysqli_query($cnx, $query);

        return true;
    }

    function insertBulkData($table, $dataToInsert){
        $cnx = $this->conn;
        if (!$cnx || $this->status_fatal) {
            echo 'InsertData -> Connection failed';
            die();
        }

        foreach ($dataToInsert as $row) {
            $values = '';
            $i = 0;
            foreach ($row as $k => $v) {
                $i++;
                $values .= $k . " = '" . $v . "'";
                if($i < count($row)){
                    $values .= ", ";
                }
            }
            $query = "INSERT INTO " . $this->dbname . "." . $table . " SET " . $values;
            mysqli_query($cnx, "SET NAMES 'utf8'");
            $cur = mysqli_query($cnx, $query);
        }

        return mysqli_insert_id($cnx);
    }

    public function truncate($table){
        $cnx = $this->conn;
        if (!$cnx || $this->status_fatal) {
            echo 'Truncate -> Connection failed';
            die();
        }
        return mysqli_query($cnx, "TRUNCATE " . $this->dbname . "." . $table);
    }

}
