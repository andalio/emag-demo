<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
error_reporting(1);

include('db.class.php');

$db_dp = new db('andalio_dp');
$db_dp_mktp = new db('andalio_mktp_dp');



// se goleste tabelul andalio_mktp_dp.brands
if($db_dp_mktp->truncate('brands')){
	echo 'Tabelul andalio_mktp_dp.<b>brands</b> a fost golit.<br>';
}

// se preiau brand-urile din andalio_dp.oc_manufacturer
$brands = $db_dp->getData("SELECT manufacturer_id, name FROM andalio_dp.oc_manufacturer WHERE active = 1");

// se formeaza un array pentru insert in andalio_mktp_dp.brands
foreach ($brands as $brand) {
	$dataToInsert[] = [
		'brand_id' => $brand['manufacturer_id'],
		'name' => $brand['name']
	];
}

// se insereaza noile brand-uri in andalio_mktp_dp.brands
if($db_dp_mktp->insertBulkData('brands', $dataToInsert)){
	echo 'Au fost inserate noile brand-uri.';
}
