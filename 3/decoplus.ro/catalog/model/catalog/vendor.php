<?php
class ModelCatalogVendor extends Model {
	public function getVendor($vendor_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor m WHERE m.vendor_id = '" . (int)$vendor_id . "'");

		return $query->row;
	}

	public function getVendors($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "vendor m";

			$sort_data = array(
				'name',
				'sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$vendor_data = $this->cache->get('vendor.' . (int)$this->config->get('config_store_id'));

			if (!$vendor_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor m ORDER BY name");

				$vendor_data = $query->rows;

				$this->cache->set('vendor.' . (int)$this->config->get('config_store_id'), $vendor_data);
			}

			return $vendor_data;
		}
	}
}