<?php
class ModelExtensionShippingCustom extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/custom');

		$status = true;

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$last_vendor_id = 0;
			$total_shipping_cost = 0;

			foreach ($this->cart->getProducts() as $key => $product) {
				if($last_vendor_id !== $product['vendor_id']){
					$total_shipping_cost += $product['shipping_cost'];
				}

				$last_vendor_id = $product['vendor_id'];
			}

			$quote_data['custom'] = array(
				'code'         => 'custom.custom',
				'title'        => $this->language->get('text_description'),
				'cost'         => $total_shipping_cost,
				'tax_class_id' => 0,
				'text'         => $this->currency->format($total_shipping_cost, $this->session->data['currency'])
			);

			$method_data = array(
				'code'       => 'custom',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('shipping_custom_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}
}