<?php
class ModelExtensionTotalShipping extends Model {
	public function getTotal($total) {
		if ($this->cart->hasShipping() && isset($this->session->data['shipping_method'])) {
			$total['totals'][] = array(
				'code'       => 'shipping',
				'title'      => $this->session->data['shipping_method']['title'],
				'value'      => $this->session->data['shipping_method']['cost'],
				'sort_order' => $this->config->get('total_shipping_sort_order')
			);

			if ($this->session->data['shipping_method']['tax_class_id']) {
				$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($total['taxes'][$tax_rate['tax_rate_id']])) {
						$total['taxes'][$tax_rate['tax_rate_id']] = $tax_rate['amount'];
					} else {
						$total['taxes'][$tax_rate['tax_rate_id']] += $tax_rate['amount'];
					}
				}
			}

			$total['total'] += $this->session->data['shipping_method']['cost'];
		} else {

			$this->load->language('extension/shipping/custom');

			$last_vendor_id = 0;
			$total_shipping_cost = 0;

			foreach ($this->cart->getProducts() as $key => $product) {
				if($last_vendor_id !== $product['vendor_id']){
					$total_shipping_cost += $product['shipping_cost'];
				}

				$last_vendor_id = $product['vendor_id'];
			} 

			$total['totals'][] = array(
				'code'       => 'shipping',
				'title'      => $this->language->get('text_title'),
				'value'      => $total_shipping_cost,
				'sort_order' => $this->config->get('total_shipping_sort_order')
			);

			$total['total'] += $total_shipping_cost;
		}
	}
}