<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// error_reporting(1);

class ControllerApiProduct extends Controller {
	public function list_by_vendor() {
		$this->load->language('api/product');

		if (!isset($this->session->data['api_id'])) {
			$json_data['error'] = $this->language->get('error_permission');
		} else {
			$this->load->model('catalog/product');

			if (isset($this->request->post['vendor_id'])) {

				$products_data = $this->model_catalog_product->getProductsByVendor(
					array(
						'filter_vendor_id' => $this->request->post['vendor_id'],
						'filter_start' => $this->request->post['start'],
						'filter_length' => $this->request->post['length'],
						'filter_draw' => $this->request->post['draw'],
						'filter_status' => $this->request->post['status'],
						'filter_identifier' => $this->request->post['identifier']
					)
				);

				$json_data = $products_data;

			} else {
				$json_data['error'] = $this->language->get('error_not_found');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json_data));
	}

	public function add() {
		$this->load->language('api/product');

		if (!isset($this->session->data['api_id'])) {
			$json_data['error'] = $this->language->get('error_permission');
		} else {
			$this->load->model('catalog/product');

			$product_id = $this->model_catalog_product->addProduct($this->request->post);

			$json_data['product_id'] = $product_id;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json_data));
	}

	public function edit() {
		$this->load->language('api/product');

		if (!isset($this->session->data['api_id'])) {
			$json_data['error'] = $this->language->get('error_permission');
		} else {
			$this->load->model('catalog/product');

			$result = $this->model_catalog_product->editProduct($this->request->post);

			$json_data['msg'] = $result;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json_data));
	}

	public function get() {
		$this->load->language('api/product');

		if (!isset($this->session->data['api_id'])) {
			$json_data['error'] = $this->language->get('error_permission');
		} else {
			$this->load->model('catalog/product');

			$product = $this->model_catalog_product->getProductApi($this->request->post['product_id']);

			$json_data['product'] = $product;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json_data));
	}

	public function add_brand() {
		$this->load->language('api/product');

		if (!isset($this->session->data['api_id'])) {
			$json_data['error'] = $this->language->get('error_permission');
		} else {
			$this->load->model('catalog/product');

			$brand_id = $this->model_catalog_product->addBrand($this->request->post['name']);

			$json_data['brand_id'] = $brand_id;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json_data));
	}
}