<?php
// Text
$_['text_success']           = 'Produsele au fost preluate cu success';

// Error
$_['error_permission']       = 'Atenție: Nu ai permisiunile necesare pentru a accesa API-ul!';
$_['error_not_found']        = 'Eroare: Produsele nu au fost găsite!';