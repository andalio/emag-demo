<?php
// Heading
$_['heading_title']     = 'Găseşte vânzătorul tău favorit';

// Text
$_['text_brand']        = 'Vânzător';
$_['text_index']        = 'În ordine alfabetică:';
$_['text_error']        = 'Vânzătorul nu a fost găsit!';
$_['text_empty']        = 'Nu există niciun Vânzător.';
$_['text_quantity']     = 'Cantitate:';
$_['text_vendor'] 		= 'Vânzător:';
$_['text_model']        = 'Cod Produs:';
$_['text_points']       = 'Puncte de recompensă:';
$_['text_price']        = 'Preţ:';
$_['text_tax']          = 'Fără TVA:';
$_['text_compare']      = 'Comparare produse (%s)';
$_['text_sort']         = 'Sortare';
$_['text_default']      = 'Implicit';
$_['text_name_asc']     = 'Nume (A - Z)';
$_['text_name_desc']    = 'Nume (Z - A)';
$_['text_price_asc']    = 'Preţ (Mic &gt; Mare)';
$_['text_price_desc']   = 'Preţ (Mare &gt; Mic)';
$_['text_rating_asc']   = 'Notă (Cea mai mică)';
$_['text_rating_desc']  = 'Notă (Cea mai mare)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Afisare';