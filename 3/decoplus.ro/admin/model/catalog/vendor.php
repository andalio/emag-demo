<?php
class ModelCatalogVendor extends Model {
	public function addVendor($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "vendor SET
			name = '" . $this->db->escape($data['name']) . "',
			info = '" . $this->db->escape($data['info']) . "',
			shipping_cost = '" . $this->db->escape($data['shipping_cost']) . "',
			sort_order = '" . (int)$data['sort_order'] . "'");

		$vendor_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "vendor SET image = '" . $this->db->escape($data['image']) . "' WHERE vendor_id = '" . (int)$vendor_id . "'");
		}

		if (isset($data['vendor_store'])) {
			foreach ($data['vendor_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "vendor_to_store SET vendor_id = '" . (int)$vendor_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
				
		// SEO URL
		if (isset($data['vendor_seo_url'])) {
			foreach ($data['vendor_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'vendor_id=" . (int)$vendor_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}
		
		$this->cache->delete('vendor');

		return $vendor_id;
	}

	public function editVendor($vendor_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "vendor SET
			name = '" . $this->db->escape($data['name']) . "',
			info = '" . $this->db->escape($data['info']) . "',
			shipping_cost = '" . $this->db->escape($data['shipping_cost']) . "',
			sort_order = '" . (int)$data['sort_order'] . "'
			WHERE vendor_id = '" . (int)$vendor_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "vendor SET image = '" . $this->db->escape($data['image']) . "' WHERE vendor_id = '" . (int)$vendor_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "vendor_to_store WHERE vendor_id = '" . (int)$vendor_id . "'");

		if (isset($data['vendor_store'])) {
			foreach ($data['vendor_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "vendor_to_store SET vendor_id = '" . (int)$vendor_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE query = 'vendor_id=" . (int)$vendor_id . "'");

		if (isset($data['vendor_seo_url'])) {
			foreach ($data['vendor_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'vendor_id=" . (int)$vendor_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}

		$this->cache->delete('vendor');
	}

	public function deleteVendor($vendor_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "vendor` WHERE vendor_id = '" . (int)$vendor_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "vendor_to_store` WHERE vendor_id = '" . (int)$vendor_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE query = 'vendor_id=" . (int)$vendor_id . "'");

		$this->cache->delete('vendor');
	}

	public function getVendor($vendor_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "vendor WHERE vendor_id = '" . (int)$vendor_id . "'");

		return $query->row;
	}

	public function getVendors($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "vendor";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getVendorStores($vendor_id) {
		$vendor_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor_to_store WHERE vendor_id = '" . (int)$vendor_id . "'");

		foreach ($query->rows as $result) {
			$vendor_store_data[] = $result['store_id'];
		}

		return $vendor_store_data;
	}
	
	public function getVendorSeoUrls($vendor_id) {
		$vendor_seo_url_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'vendor_id=" . (int)$vendor_id . "'");

		foreach ($query->rows as $result) {
			$vendor_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
		}

		return $vendor_seo_url_data;
	}
	
	public function getTotalVendors() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "vendor");

		return $query->row['total'];
	}
}
