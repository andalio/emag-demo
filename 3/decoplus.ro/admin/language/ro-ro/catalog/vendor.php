<?php
// Heading
$_['heading_title']      = 'Vânzători';

// Text
$_['text_success']       = 'Succes: Ai modificat vânzătorii!';
$_['text_list']          = 'Listă Vânzători';
$_['text_add']           = 'Adaugă Vânzător';
$_['text_edit']          = 'Editează Vânzător';
$_['text_default']       = 'Implicit';
$_['text_percent']       = 'Procent';
$_['text_amount']        = 'Sumă fixă';
$_['text_keyword']           = 'Nu folositi spatii, inlocuiti spatiile cu - si asigurati-va ca seo url-ul este unic in site.';

// Column
$_['column_name']        = 'Nume Vânzători';
$_['column_sort_order']  = 'Ordine Sortare';
$_['column_action']      = 'Acţiune';

// Entry
$_['entry_name']         = 'Nume Vânzător';
$_['entry_info']         = 'Informații Vânzător';
$_['entry_shipping_cost']= 'Cost transport';
$_['entry_store']        = 'Magazine';
$_['entry_keyword']      = 'Cuvant SEO';
$_['entry_image']        = 'imagine';
$_['entry_sort_order']   = 'Ordine Sortare';
$_['entry_type']         = 'Tip';

// Help
$_['help_keyword']       = 'Nu folosii spații, in schimb inlocuiește spațiile cu - si asigură-te că acest cuvânt este unic global.';

// Error
$_['error_permission']   = 'Avertisment: Nu aveţi permisiunea de a modifica vânzătorii!';
$_['error_name']         = 'Numele vânzătorului trebuie să conțină între 2 și 64 de caractere!';
$_['error_keyword']      = 'Cuvânt SEO deja folosit!';
$_['error_product']      = 'Avertisment: Acest vânzător nu poate fi ștears pentru ca este în prezent asociat cu %s produse!';