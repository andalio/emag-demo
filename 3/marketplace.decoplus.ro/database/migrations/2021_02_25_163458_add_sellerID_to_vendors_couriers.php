<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellerIDToVendorsCouriers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_couriers', function (Blueprint $table) {
            $table->integer('seller_id')->unsigned()->nullable()->after('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_couriers', function (Blueprint $table) {
            $table->dropColumn('seller_id');
        });
    }
}
