<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCouriers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('couriers', function (Blueprint $table) {
            $table->increments('courier_id')->unsigned()->unique();
            $table->string('name', 255);
            $table->string('image', 255);
        });

        DB::update("ALTER TABLE couriers AUTO_INCREMENT = 10000;");

        DB::table('couriers')->insert(
            [
                ['courier_id' => 1, 'name' => 'Sameday', 'image' => 'sameday.png'],
                ['courier_id' => 2, 'name' => 'FAN Courier', 'image' => 'fan-courier.png'],
                ['courier_id' => 3, 'name' => 'Urgent Cargus', 'image' => 'urgent-cargus.png']
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('couriers');
    }

}
