<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrdersStatuses extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('orders_statuses', function (Blueprint $table) {
            $table->increments('order_status_id')->unsigned()->unique();

            $table->string('name', 255);
            $table->string('class', 255);
            $table->tinyInteger('user_selectable')->default(0);
        });
        DB::table('orders_statuses')->insert(
            [
                ['name' => 'Nou', 'class' => 'status_gray', 'user_selectable' => 0],
                ['name' => 'In lucru', 'class' => 'status_blue', 'user_selectable' => 1],
                ['name' => 'Pregatit', 'class' => 'status_purple', 'user_selectable' => 1],
                ['name' => 'In intarziere', 'class' => 'status_orange', 'user_selectable' => 0],
                ['name' => 'Finalizat', 'class' => 'status_green', 'user_selectable' => 1],
                ['name' => 'Anulat de client', 'class' => 'status_red', 'user_selectable' => 0],
                ['name' => 'Anulat de seller', 'class' => 'status_red', 'user_selectable' => 1],
                ['name' => 'Anulat automat', 'class' => 'status_red', 'user_selectable' => 0],
                ['name' => 'Anulat cu incident', 'class' => 'status_red', 'user_selectable' => 0],
                ['name' => 'Stornat', 'class' => 'status_black', 'user_selectable' => 1]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('orders_statuses');
    }

}
