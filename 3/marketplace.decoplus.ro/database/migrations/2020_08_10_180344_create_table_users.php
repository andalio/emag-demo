<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned()->unique();

            $table->string('last_name', 255);
            $table->string('first_name', 255);
            $table->string('email', 255);
            $table->string('phone', 255);
            $table->string('display_name', 255);
            $table->string('password', 255);
            $table->integer('role');
            $table->tinyInteger('active')->default(0);

            $table->rememberToken();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
        DB::table('users')->insert(
                [
                        ['last_name' => 'Dani', 'first_name' => 'Admin', 'email' => 'danny@andalio.ro', 'phone' => '0768409759', 'display_name' => 'Pingo Admin', 'password' => '$2y$10$LwG0GZXPZPWeuF6mcEvm1e8XvVKiGO2O1.mHMIurlFkoPr4cAaOV6', 'role' => 1, 'active' => 1],
                        ['last_name' => 'Dani', 'first_name' => 'Vendor', 'email' => 'office@andalio.ro', 'phone' => '0768409759', 'display_name' => 'BMF SRL', 'password' => '$2y$10$LwG0GZXPZPWeuF6mcEvm1e8XvVKiGO2O1.mHMIurlFkoPr4cAaOV6', 'role' => 2, 'active' => 1]
                ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }

}
