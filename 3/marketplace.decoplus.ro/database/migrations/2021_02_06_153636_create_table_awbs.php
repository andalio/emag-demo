<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAwbs extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('awbs', function (Blueprint $table) {
            $table->increments('awb_id')->unsigned()->unique();

            $table->integer('vendor_courier_id');
            $table->integer('order_id');

            $table->string('awb_number', 255);

            $table->timestamp('last_status_date')->nullable();
            $table->integer('awb_status_id');

            $table->string('shipping_contact', 255);
            $table->string('shipping_telephone', 255);
            $table->decimal('cash_on_delivery', 15, 4)->default(0);

            $table->text('awb_data');
            $table->text('awb_result');
            
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        DB::update("ALTER TABLE awbs AUTO_INCREMENT = 10000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('awbs');
    }

}
