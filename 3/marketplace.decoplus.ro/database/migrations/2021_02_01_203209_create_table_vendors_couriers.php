<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendorsCouriers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('vendors_couriers', function (Blueprint $table) {
            $table->increments('vendor_courier_id')->unsigned()->unique();

            $table->integer('vendor_id');
            $table->integer('courier_id');

            $table->string('username', 255)->nullable();
            $table->string('password', 255)->nullable();

            $table->integer('pickup_point')->nullable();
            $table->integer('contact_person')->nullable();
            $table->integer('service')->nullable();

            // $table->string('pickup_point_text', 255);
            // $table->string('contact_person_text', 255);
            // $table->string('service_text', 255);

            $table->tinyInteger('default')->default(0);

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('vendors_couriers');
    }

}
