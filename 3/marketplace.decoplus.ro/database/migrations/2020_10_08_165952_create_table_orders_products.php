<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrdersProducts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('orders_products', function (Blueprint $table) {
            $table->increments('order_product_id')->unsigned()->unique();

            $table->integer('order_id');
            $table->integer('product_id');

            $table->string('name', 255);
            $table->string('model', 255);

            $table->integer('quantity');

            $table->decimal('price', 15, 4);
            $table->decimal('total', 15, 4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('orders_products');
    }

}
