<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategories extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('category_id')->unsigned()->unique();
            $table->string('name', 255);
            $table->integer('parent_id')->default(0);
            $table->tinyInteger('status')->default(0);
        });
        DB::table('categories')->insert(
            [
                ['category_id' => 1, 'name' => 'Aaa categ', 'status' => 1],
                ['category_id' => 2, 'name' => 'Bbb categ', 'status' => 1],
                ['category_id' => 3, 'name' => 'Ccc categ', 'status' => 1],
                ['category_id' => 4, 'name' => 'Ddd categ', 'status' => 1]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('categories');
    }

}
