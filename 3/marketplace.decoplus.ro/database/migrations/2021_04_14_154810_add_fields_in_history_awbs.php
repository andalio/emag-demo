<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInHistoryAwbs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_awbs', function (Blueprint $table) {
            $table->integer('courier_awb_status_id')->after('awb_status_id');
            $table->string('courier_awb_status_name', 255)->after('courier_awb_status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_awbs', function (Blueprint $table) {
            $table->dropColumn('courier_awb_status_id');
            $table->dropColumn('courier_awb_status_name');
        });
    }
}
