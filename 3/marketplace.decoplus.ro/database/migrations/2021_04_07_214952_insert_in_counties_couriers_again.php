<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertInCountiesCouriersAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('counties_couriers')->insert(
            [
                ['county_id' => '2679', 'courier_id' => '2' ,'vendor_county_id' => '1'],
                ['county_id' => '2680', 'courier_id' => '2' ,'vendor_county_id' => '2'],
                ['county_id' => '2681', 'courier_id' => '2' ,'vendor_county_id' => '3'],
                ['county_id' => '2682', 'courier_id' => '2' ,'vendor_county_id' => '4'],
                ['county_id' => '2683', 'courier_id' => '2' ,'vendor_county_id' => '5'],
                ['county_id' => '2684', 'courier_id' => '2' ,'vendor_county_id' => '6'],
                ['county_id' => '2685', 'courier_id' => '2' ,'vendor_county_id' => '7'],
                ['county_id' => '2687', 'courier_id' => '2' ,'vendor_county_id' => '8'],
                ['county_id' => '2686', 'courier_id' => '2' ,'vendor_county_id' => '9'],
                ['county_id' => '2688', 'courier_id' => '2' ,'vendor_county_id' => '10'],
                ['county_id' => '2689', 'courier_id' => '2' ,'vendor_county_id' => '11'],
                ['county_id' => '2691', 'courier_id' => '2' ,'vendor_county_id' => '12'],
                ['county_id' => '2690', 'courier_id' => '2' ,'vendor_county_id' => '13'],
                ['county_id' => '2692', 'courier_id' => '2' ,'vendor_county_id' => '14'],
                ['county_id' => '2693', 'courier_id' => '2' ,'vendor_county_id' => '15'],
                ['county_id' => '2694', 'courier_id' => '2' ,'vendor_county_id' => '16'],
                ['county_id' => '2695', 'courier_id' => '2' ,'vendor_county_id' => '17'],
                ['county_id' => '2696', 'courier_id' => '2' ,'vendor_county_id' => '18'],
                ['county_id' => '2697', 'courier_id' => '2' ,'vendor_county_id' => '19'],
                ['county_id' => '2698', 'courier_id' => '2' ,'vendor_county_id' => '20'],
                ['county_id' => '2699', 'courier_id' => '2' ,'vendor_county_id' => '21'],
                ['county_id' => '2700', 'courier_id' => '2' ,'vendor_county_id' => '22'],
                ['county_id' => '2701', 'courier_id' => '2' ,'vendor_county_id' => '23'],
                ['county_id' => '2702', 'courier_id' => '2' ,'vendor_county_id' => '24'],
                ['county_id' => '2703', 'courier_id' => '2' ,'vendor_county_id' => '25'],
                ['county_id' => '2704', 'courier_id' => '2' ,'vendor_county_id' => '206'],
                ['county_id' => '2705', 'courier_id' => '2' ,'vendor_county_id' => '26'],
                ['county_id' => '2706', 'courier_id' => '2' ,'vendor_county_id' => '27'],
                ['county_id' => '2707', 'courier_id' => '2' ,'vendor_county_id' => '28'],
                ['county_id' => '2708', 'courier_id' => '2' ,'vendor_county_id' => '29'],
                ['county_id' => '2709', 'courier_id' => '2' ,'vendor_county_id' => '30'],
                ['county_id' => '2710', 'courier_id' => '2' ,'vendor_county_id' => '31'],
                ['county_id' => '2712', 'courier_id' => '2' ,'vendor_county_id' => '32'],
                ['county_id' => '2711', 'courier_id' => '2' ,'vendor_county_id' => '33'],
                ['county_id' => '2713', 'courier_id' => '2' ,'vendor_county_id' => '34'],
                ['county_id' => '2714', 'courier_id' => '2' ,'vendor_county_id' => '35'],
                ['county_id' => '2715', 'courier_id' => '2' ,'vendor_county_id' => '36'],
                ['county_id' => '2716', 'courier_id' => '2' ,'vendor_county_id' => '37'],
                ['county_id' => '2717', 'courier_id' => '2' ,'vendor_county_id' => '39'],
                ['county_id' => '2719', 'courier_id' => '2' ,'vendor_county_id' => '39'],
                ['county_id' => '2718', 'courier_id' => '2' ,'vendor_county_id' => '40'],
                ['county_id' => '2720', 'courier_id' => '2' ,'vendor_county_id' => '41']
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // nimic
    }
}
