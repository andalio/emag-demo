<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsFromAwbs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('awbs', function (Blueprint $table) {
            $table->dropColumn('last_status_date');
            $table->dropColumn('awb_status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('awbs', function (Blueprint $table) {
            $table->timestamp('last_status_date')->nullable()->after('awb_number');
            $table->integer('awb_status_id')->after('last_status_date');
        });
    }
}
