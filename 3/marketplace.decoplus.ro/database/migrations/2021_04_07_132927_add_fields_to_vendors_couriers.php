<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToVendorsCouriers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_couriers', function (Blueprint $table) {
            $table->string('token', 255)->nullable()->after('default');
            $table->timestamp('token_expiry')->nullable()->after('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_couriers', function (Blueprint $table) {
            $table->dropColumn('token');
            $table->dropColumn('token_expiry');
        });
    }
}
