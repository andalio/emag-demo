<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUserAndPassNullableInVendorsCouriers extends Migration
{
    // /**
    //  * Run the migrations.
    //  *
    //  * @return void
    //  */
    // public function up()
    // {
    //     Schema::table('vendors_couriers', function (Blueprint $table) {
    //         $table->string('username', 255)->nullable()->change();
    //         $table->string('password', 255)->nullable()->change();
    //     });
    // }

    // /**
    //  * Reverse the migrations.
    //  *
    //  * @return void
    //  */
    // public function down()
    // {
    //     Schema::table('vendors_couriers', function (Blueprint $table) {
    //         $table->string('username', 255)->change();
    //         $table->string('password', 255)->change();
    //     });
    // }
}
