<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBrands extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('brand_id')->unsigned()->unique();
            $table->string('name', 255);
        });
        DB::table('brands')->insert(
            [
                ['brand_id' => 1, 'name' => 'Aaa categ'],
                ['brand_id' => 2, 'name' => 'Bbb categ'],
                ['brand_id' => 3, 'name' => 'Ccc categ'],
                ['brand_id' => 4, 'name' => 'Ddd categ']
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('brands');
    }

}
