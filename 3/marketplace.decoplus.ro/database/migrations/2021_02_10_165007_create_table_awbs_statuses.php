<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAwbsStatuses extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('awbs_statuses', function (Blueprint $table) {
            $table->increments('awb_status_id')->unsigned()->unique();

            $table->string('name', 255);
            $table->string('class', 255);
        });
        DB::table('awbs_statuses')->insert(
            [
                ['awb_status_id' => 1, 'name' => 'AWB creat', 'class' => 'status_orange'],
                ['awb_status_id' => 2, 'name' => 'AWB anulat', 'class' => 'status_gray'],
                ['awb_status_id' => 3, 'name' => 'Predat curierului', 'class' => 'status_blue'],
                ['awb_status_id' => 4, 'name' => 'In drum spre client', 'class' => 'status_blue'],
                ['awb_status_id' => 5, 'name' => 'Livrat', 'class' => 'status_green'],
                ['awb_status_id' => 6, 'name' => 'Refuzat de client', 'class' => 'status_purple'],
                ['awb_status_id' => 7, 'name' => 'Returnat expeditorului', 'class' => 'status_black'],
                ['awb_status_id' => 8, 'name' => 'Deteriorat/pierdut', 'class' => 'status_red'],
                ['awb_status_id' => 9, 'name' => 'Alt status', 'class' => 'status_gray'],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('awbs_statuses');
    }

}
