<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCounties extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('counties', function (Blueprint $table) {
            $table->increments('county_id')->unsigned()->unique();

            $table->string('name', 255);
            $table->string('code', 255);
        });
        DB::table('counties')->insert(
            [
                ['county_id' => '2679', 'name' => 'Alba', 'code' => 'AB'],
                ['county_id' => '2680', 'name' => 'Arad', 'code' => 'AR'],
                ['county_id' => '2681', 'name' => 'Arges', 'code' => 'AG'],
                ['county_id' => '2682', 'name' => 'Bacau', 'code' => 'BC'],
                ['county_id' => '2683', 'name' => 'Bihor', 'code' => 'BH'],
                ['county_id' => '2684', 'name' => 'Bistrita-Nasaud', 'code' => 'BN'],
                ['county_id' => '2685', 'name' => 'Botosani', 'code' => 'BT'],
                ['county_id' => '2686', 'name' => 'Brasov', 'code' => 'BV'],
                ['county_id' => '2687', 'name' => 'Braila', 'code' => 'BR'],
                ['county_id' => '2688', 'name' => 'Bucuresti', 'code' => 'B'],
                ['county_id' => '2689', 'name' => 'Buzau', 'code' => 'BZ'],
                ['county_id' => '2690', 'name' => 'Caras-Severin', 'code' => 'CS'],
                ['county_id' => '2691', 'name' => 'Calarasi', 'code' => 'CL'],
                ['county_id' => '2692', 'name' => 'Cluj', 'code' => 'CJ'],
                ['county_id' => '2693', 'name' => 'Constanta', 'code' => 'CT'],
                ['county_id' => '2694', 'name' => 'Covasna', 'code' => 'CV'],
                ['county_id' => '2695', 'name' => 'Dimbovita', 'code' => 'DB'],
                ['county_id' => '2696', 'name' => 'Dolj', 'code' => 'DJ'],
                ['county_id' => '2697', 'name' => 'Galati', 'code' => 'GL'],
                ['county_id' => '2698', 'name' => 'Giurgiu', 'code' => 'GR'],
                ['county_id' => '2699', 'name' => 'Gorj', 'code' => 'GJ'],
                ['county_id' => '2700', 'name' => 'Harghita', 'code' => 'HR'],
                ['county_id' => '2701', 'name' => 'Hunedoara', 'code' => 'HD'],
                ['county_id' => '2702', 'name' => 'Ialomita', 'code' => 'IL'],
                ['county_id' => '2703', 'name' => 'Iasi', 'code' => 'IS'],
                ['county_id' => '2704', 'name' => 'Ilfov', 'code' => 'IF'],
                ['county_id' => '2705', 'name' => 'Maramures', 'code' => 'MM'],
                ['county_id' => '2706', 'name' => 'Mehedinti', 'code' => 'MH'],
                ['county_id' => '2707', 'name' => 'Mures', 'code' => 'MS'],
                ['county_id' => '2708', 'name' => 'Neamt', 'code' => 'NT'],
                ['county_id' => '2709', 'name' => 'Olt', 'code' => 'OT'],
                ['county_id' => '2710', 'name' => 'Prahova', 'code' => 'PH'],
                ['county_id' => '2711', 'name' => 'Satu-Mare', 'code' => 'SM'],
                ['county_id' => '2712', 'name' => 'Salaj', 'code' => 'SJ'],
                ['county_id' => '2713', 'name' => 'Sibiu', 'code' => 'SB'],
                ['county_id' => '2714', 'name' => 'Suceava', 'code' => 'SV'],
                ['county_id' => '2715', 'name' => 'Teleorman', 'code' => 'TR'],
                ['county_id' => '2716', 'name' => 'Timis', 'code' => 'TM'],
                ['county_id' => '2717', 'name' => 'Tulcea', 'code' => 'TL'],
                ['county_id' => '2718', 'name' => 'Vaslui', 'code' => 'VS'],
                ['county_id' => '2719', 'name' => 'Valcea', 'code' => 'VL'],
                ['county_id' => '2720', 'name' => 'Vrancea', 'code' => 'VN']
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('counties');
    }

}



