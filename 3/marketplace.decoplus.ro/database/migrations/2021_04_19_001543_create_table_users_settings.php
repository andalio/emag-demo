<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsersSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_settings', function (Blueprint $table) {
            $table->increments('user_setting_id')->unsigned()->unique();

            $table->integer('user_id');

            $table->string('display_name', 255);

            $table->string('company_name', 255);
            $table->integer('company_county_id');
            $table->string('company_city', 255);
            $table->string('company_address', 255);
            $table->string('company_bank', 255);
            $table->string('company_iban', 255);
            $table->string('company_phone', 255);

            $table->integer('return_period');

            $table->text('other_info');

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        DB::table('users_settings')->insert(
                [
                        [
                            'user_id' => 1,
                            'display_name' => 'BMF SRL',
                            'company_name' => 'Business Media Factory SRL',
                            'company_county_id' => 2699,
                            'company_city' => 'Targu-Jiu',
                            'company_address' => '14 Octombrie, nr. 137',
                            'company_bank' => 'ING BANK',
                            'company_iban' => 'ROING6565474747468688534',
                            'company_phone' => '0768409759',
                            'return_period' => 30,
                            'other_info' => json_encode([
                                'alt_telefon' => '0766123123'
                            ]),
                        ]
                ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_settings');
    }
}
