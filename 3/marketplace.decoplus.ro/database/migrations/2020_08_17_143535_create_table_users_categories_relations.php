<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsersCategoriesRelations extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users_categories_relations', function (Blueprint $table) {
            $table->increments('id')->unsigned()->unique();
            $table->integer('user_id');
            $table->integer('category_id');

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
        DB::table('users_categories_relations')->insert(
                [
                    ['id' => 1, 'user_id' => 1, 'category_id' => 66],
                    ['id' => 2, 'user_id' => 1, 'category_id' => 67],
                    ['id' => 3, 'user_id' => 1, 'category_id' => 68],
                    ['id' => 4, 'user_id' => 1, 'category_id' => 72],
                    ['id' => 5, 'user_id' => 1, 'category_id' => 74]
                ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users_categories_relations');
    }

}
