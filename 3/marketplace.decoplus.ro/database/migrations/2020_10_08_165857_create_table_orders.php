<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id')->unsigned()->unique();

            $table->integer('dp_order_id');
            $table->integer('vendor_id');

            $table->string('firstname', 255);
            $table->string('lastname', 255);
            $table->string('email', 255);
            $table->string('telephone', 255);

            $table->string('payment_firstname', 255);
            $table->string('payment_lastname', 255);
            $table->string('payment_company', 255);
            $table->string('payment_address_1', 255);
            $table->string('payment_address_2', 255);
            $table->string('payment_city', 255);
            $table->string('payment_country', 255);
            $table->string('payment_zone', 255);
            $table->string('payment_method', 255);
            $table->string('payment_code', 255);

            $table->string('shipping_firstname', 255);
            $table->string('shipping_lastname', 255);
            $table->string('shipping_company', 255);
            $table->string('shipping_address_1', 255);
            $table->string('shipping_address_2', 255);
            $table->string('shipping_city', 255);
            $table->string('shipping_country', 255);
            $table->string('shipping_zone', 255);
            $table->string('shipping_code', 255);
            $table->string('comment', 255);

            $table->decimal('weight', 15, 8);
            $table->decimal('total', 15, 4);
            $table->decimal('shipping_cost', 15, 4);

            $table->integer('order_status_id');

            $table->timestamp('date_added_in_dp')->nullable();

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        DB::update("ALTER TABLE orders AUTO_INCREMENT = 10000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('orders');
    }

}
