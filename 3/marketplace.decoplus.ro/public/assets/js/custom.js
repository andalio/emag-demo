$(function() {
    // Laravel js token
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});


$(function() {
    $(document).tooltip({
        items: '.tooltip-title',
        position: {
            my: "center bottom-10",
            at: "center top"
        }
    });
});

// Form processing
$('#form').ajaxForm({
    dataType: 'json',
    beforeSerialize: function () {
            // $('#form button[type=submit]').prop('disabled', true);
        },
        success: function (data) {
            if (data.error) {
                showMessage(data.error);
                $('#alert_success').addClass('hide');
            } else {
                if (data.action === 'add') {
                    window.location.replace(data.url);
                } else {
                    $('html, body').animate({scrollTop: 0}, 'slow');
                    $('.error-badge').addClass('hide');
                }
            }

            if (data.error_msg) {
                $('#alert_error .message').text(data.error_msg);
                $('#alert_error').removeClass('hide');
                $('#alert_success').addClass('hide');
            }

            if(!data.error && data.success_msg){
                $('#alert_success .message').text(data.success_msg);
                $('#alert_success').removeClass('hide');
                setTimeout(function () {
                    $('#alert_success').addClass('hide');
                }, 5000);
                $('#alert_error').addClass('hide');

                if(data.url){
                    window.location.replace(data.url);
                }
            }

            // Campuri editabile
            // if(data.editable_fields){
            //     $('.editable_field .data').each(function() {
            //         $(this).find('b').text($(this).parent().find('.field').val());
            //     });

            //     $('.editable_field .data').removeClass('hide');
            //     $('.editable_field .field').addClass('hide');
            // }

            // if(!data.error && data.error_msg){
            //    $('#alert_error .message').text(data.error_msg);
            //    $('#alert_error').removeClass('hide');
            //    $('#alert_success').addClass('hide');
            // }

            // $('#form button[type=submit]').prop('disabled', false);
        }
    });

function showMessage(data) {
    $.each(data, function (id, item) {
        if (item.code == 0) {
            $('#' + item.element_id).parent().find('.error-badge').addClass('hide');
        } else {
            $('html, body').animate({scrollTop: 0}, 'slow');
            $('#' + item.element_id).parent().find('.error-badge').html(item.msg);
            $('#' + item.element_id).parent().find('.error-badge').removeClass('hide');
        }
    });
}

function delete_entry(action) {
    // Delete modal
    $(document).on('click', '.delete-modal-sm', function () {
        $('#delete_final_button').attr('data-id', $(this).attr('data-id'));
    });
    // Confirm delete
    $(document).on('click', '#delete_final_button', function () {
        // Delete entry
        $.post(action + $(this).attr('data-id'), function (data) {
            $('.modal-header').addClass('hide');
            $('.modal-footer').addClass('hide');
            $('.modal-body').removeClass('hide');
            setTimeout(function () {
                $('.modal-header .close').trigger('click');
                window.location.reload();
            }, 1000);
        }, 'json');
    });
}

function initSelect2(){

    // select-uri
    $('select:not(.not-select2)').select2({
        language: 'ro',
        templateResult: function(data, container){
            if (data.element) {
                $(container).addClass($(data.element).attr("class"));
            }
            return data.text;
        },
        templateSelection: function(data, container){
            if (data.element) {
                $(container).attr("class", 'select2-selection__rendered ' + $(data.element).attr("class"));
            }
            return data.text;
        }
    });

    // brand
    $('#brand').select2({
        ajax: {
            url: base_url + '/product/autocomplete_brands',
            method: 'POST',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                return {
                    results: data.items
                };
            }
        },
        language: 'ro',
        tags: true
    });

    // category
    $('#category').select2({
        ajax: {
            url: base_url + '/product/autocomplete_categories',
            method: 'POST',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query;
            },
            processResults: function (data) {
                return {
                    results: data.items
                };
            }
        },
        language: 'ro',
        tags: false
    });

    // shipping_city
    $('#shipping_city').select2({
        ajax: {
            url: base_url + '/order/autocomplete_shipping_city',
            method: 'POST',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    search: params.term,
                    order_id: $('#order_id').val(),
                    selected_courier: $('input[name=selected_courier]:checked').val()
                }
                return query;
            },
            processResults: function (data) {
                return {
                    results: data.items
                };
            }
        },
        minimumInputLength: 3,
        language: 'ro',
        tags: false
    });

    $('#shipping_city').on('select2:select', function (e) {
        if(e.params.data.postalCode !== "0"){
            $('#shipping_postcode').val(e.params.data.postalCode);
        } else {
            $('#shipping_postcode').val('000000');
        }
    });

}

$(document).ready(function () {

    // Initializare select2
    initSelect2();

    // actiuni pentru lista de comenzi
    $(document).on('click', '#datatable .order_actions a.details', function () {
        // botonul de Arata/ascunde detalii
        $(this).find('.show_details, .hide_details').toggleClass('hide');

        // se genereaza un rand nou pentru detaliile comenzii la care s-a facut click pe Arata detalii
        var tr = $(this).closest('tr');
        var row = datatable.row(tr);

        if ( row.child.isShown() ) {
            row.child.hide();
        } else {
            // se preiau detaliile comenzii
            $.ajax({
                method: 'POST',
                url: base_url + '/order/get_details',
                data: {order_id: row.data().order_id},
                dataType: 'json'
            }).done(function (data) {

                var details_content = '<table class="order_details">' +
                '<tr>' +
                '<td class="shipping">' +
                'Date livrare<hr>' +
                '<span class="shipping_name">' + data.order.shipping_name + '</span>' + ' | <span class="shipping_telephone">' + data.order.shipping_telephone + '</span>' +
                '<span class="shipping_address">' + data.order.shipping_address + '</span>' +
                '<span class="shipping_city">Localitate: ' + data.order.shipping_city + '</span>' + ' | <span class="shipping_zone">Judet: ' + data.order.shipping_zone + '</span>' +
                '</td>' +
                '<td class="products">' +
                '<table>';

                $.each(data.order_products, function (id, product) {
                    details_content += '<tr class="product">';
                    details_content += '<td><b>' + product.name + '</b><br>Cod produs: ' + product.model + '</td>';
                    details_content += '<td>' + product.quantity + '</td>';
                    details_content += '<td class="product_total">' + product.total + '</td>';
                    details_content += '</tr>';
                });

                details_content += '<tr class="product"><td colspan="2">Cost livrare</td><td class="order_total">' + data.order.shipping_cost + '</td></tr>' +
                '<tr class="total"><td colspan="2">Total</td><td class="order_total">' + data.order.total + '</td></tr>' +
                '</table>' +
                '</td>' +
                '<td class="actions">' +
                '<a class="btn btn-link create_awb"><i class="fa fa-truck"></i> Genereaza AWB</a><br>' +
                '<a class="btn btn-link print_order" order_id="' + row.data().order_id + '"><i class="fa fa-print"></i> Print detalii</a>' +
                '</td>' +
                '</tr>' +
                '</table>';

                row.child(details_content, 'order_details_container').show();

            })
            .fail(function () {
                console.log('Get data failed!');
            });
        }

    });


    $('.save_changes').click(function(){
        $('form.order_changes').submit();
    });

    $(document).on('click', '.print_order', function (e) {
        $('#content_to_print').load(base_url + '/order/print/' + $(this).attr('order_id') + '/parent', function() {
            $('#content_to_print').printThis();
        });
    });

    $('.add-new-courier').click(function(){
        $('.couriers_list').toggleClass('hide');
    });

    $(document).on('click', '.create_awb_content .couriers_list .courier .logo', function () {
        if(!$(this).hasClass('disabled')){
            $('.create_awb_content .couriers_list .courier .logo').removeClass('selected');
            $(this).addClass('selected');
            $(this).parent().find('input[name=selected_courier]').prop('checked', true);
        }
    });

    $('.create_awb').click(function(){
        $('#modal-create-awb .modal-content').load(base_url + '/order/awb/' + $('#order_id').val() + '/parent', function(){
            // Initializare select2
        initSelect2();
        });
    });

    $(document).on('click', '#order_awb_save', function (e) {
        e.preventDefault();
        
        // Form processing
        $('#awb_form').ajaxForm({
            dataType: 'json',
            beforeSerialize: function () {
                // $('#form button[type=submit]').prop('disabled', true);
            },
            success: function (data) {
                if (data.error) {
                    showMessage(data.error);
                    $('#awb_alert_success').addClass('hide');
                } else {
                    if (data.action === 'add') {
                        window.location.replace(data.url);
                    } else {
                        $('html, body').animate({scrollTop: 0}, 'slow');
                        $('.error-badge').addClass('hide');
                    }
                }

                if (data.error_msg) {
                    $('#awb_alert_error .message').text(data.error_msg);
                    $('#awb_alert_error').removeClass('hide');
                    $('#awb_alert_success').addClass('hide');
                }

                if(!data.error && data.success_msg){
                    $('#awb_alert_success .message').text(data.success_msg);
                    $('#awb_alert_success').removeClass('hide');
                    setTimeout(function () {
                        $('#awb_alert_success').addClass('hide');
                    }, 5000);
                    $('#awb_alert_error').addClass('hide');

                    if(data.url){
                        window.location.replace(data.url);
                        window.location.reload();
                    }
                }

                // if(!data.error && data.error_msg){
                //     $('#awb_alert_error .message').text(data.error_msg);
                //     $('#awb_alert_error').removeClass('hide');
                //     $('#awb_alert_success').addClass('hide');
                // }

                // $('#form button[type=submit]').prop('disabled', false);
            }
        });

        $('#awb_form').submit();
    });

    $('.import_products').click(function(){
        $('#modal-import-products .modal-content').load(base_url + '/product/import/parent', function(){
            //
        });
    });

    $(document).on('change', '#import_file_xls', function (e) {
        if($(this)[0].files.length){
            $('.import_products_select_file label').text($(this)[0].files[0].name);
        } else {
            $('.import_products_select_file label').text('Selecteaza fisier XLS');
        }
    });

    $(document).on('click', '#import_products_save', function (e) {
        e.preventDefault();

        $('.import_products_select_file .overlay').removeClass('hide');
        
        // Form processing
        $('#import_form').ajaxForm({
            dataType: 'json',
            beforeSerialize: function () {
                $('#import_form button[type=submit]').prop('disabled', true);
            },
            success: function (data) {

                $('.import_products_select_file .overlay').addClass('hide');

                if (data.error) {
                    $('.import_products_select_file').addClass('hide');

                    $('.import_products_errors ul').remove();
                    $('.import_products_errors').append('<ul></ul>');

                    $.each(data.error, function (id, error) {
                        $('.import_products_errors ul').append('<li>' + error.msg + '</li>');
                    });

                    $('.import_products_errors').removeClass('hide');
                    $('#import_products_save').addClass('hide');

                    $('#import_products_alert_success').addClass('hide');
                } else {
                    if (data.action === 'add') {
                        window.location.replace(data.url);
                    } else {
                        $('html, body').animate({scrollTop: 0}, 'slow');
                        $('.error-badge').addClass('hide');
                    }
                }

                if (data.error_msg) {
                    $('#import_products_alert_error .message').text(data.error_msg);
                    $('#import_products_alert_error').removeClass('hide');
                    $('#import_products_alert_success').addClass('hide');
                }

                if(!data.error && data.success_msg){
                    $('#import_products_alert_success .message').text(data.success_msg);
                    $('#import_products_alert_success').removeClass('hide');
                    setTimeout(function () {
                        $('#import_products_alert_success').addClass('hide');
                        // $('#modal-import-products').modal('toggle');
                    }, 5000);
                    $('#import_products_alert_error').addClass('hide');

                    if(data.url){
                        window.location.replace(data.url);
                        window.location.reload();
                    }
                }

                // if(!data.error && data.error_msg){
                //     $('#import_products_alert_error .message').text(data.error_msg);
                //     $('#import_products_alert_error').removeClass('hide');
                //     $('#import_products_alert_success').addClass('hide');
                // }

                $('#import_form button[type=submit]').prop('disabled', false);
            }
        });

        $('#import_form').submit();
    });

    //daca avem hash in url incercam sa facem scrol la elemetul cu id=hash
    if(window.location.hash){
        var background_color = $(window.location.hash).css('background-color');
        
        $(window.location.hash).css('background-color', '#BBB');

        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top
        }, 'slow', function(){
            $(window.location.hash).animate({backgroundColor: 'background_color'}, 'slow');
        });
    }


    // Campuri editabile
    $('.btn.edit_fields').click(function(){
        $('.' + $(this).attr('attr-area') + ' .editable_field .data').addClass('hide');
        $('.' + $(this).attr('attr-area') + ' .editable_field .field').removeClass('hide');
        $('.' + $(this).attr('attr-area') + ' .editable_field .not-select2').removeClass('hide not-select2');

        // Initializare select2
        initSelect2();
    });

});


