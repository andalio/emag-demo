$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
    });
    
    $('#login_form').submit(function (e) {
        e.preventDefault();
        var instance = $(this);
        $(this).find('button').prop('disabled', true);
        $.post(base_url + '/authenticate', $(this).serialize(), function (data) {
            if (data.msg == 'ok') {
                window.location.replace(base_url + '/dashboard');
            } else {
                $('.auth-error').removeClass('hide');
                instance.find('button').prop('disabled', false);
            }
        }, 'json');
    });
});