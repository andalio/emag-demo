<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth
Route::get('/login', 'Auth\Login@index')->name('login');
Route::post('/authenticate', 'Auth\Login@authenticate');
Route::get('/logout', 'Auth\Login@logout')->name('logout');

// Dashboard
Route::get('/dashboard', 'Dashboard@index')->name('dashboard');

// Products
Route::get('/product/all', 'Products@all')->name('product.all');
Route::get('/product/datatables', 'Products@datatables')->name('product.datatables');
Route::get('/product/add', 'Products@add')->name('product.add');
Route::get('/product/import/{view_mode}', 'Products@import')->name('product.import');
Route::post('/product/import_process', 'Products@import_process')->name('product.import_process');
Route::get('/product/edit/{id}', 'Products@edit')->name('product.edit');
Route::get('/product/edit/{id}/{method}', 'Products@clone')->name('product.clone');
Route::post('product/process/{action}', 'Products@process')->name('product.process');
Route::post('product/images_upload', 'Products@images_upload')->name('product.images_upload');

Route::post('/product/autocomplete_brands', 'Products@autocomplete_brands')->name('product.autocomplete_brands');
Route::post('/product/autocomplete_categories', 'Products@autocomplete_categories')->name('product.autocomplete_categories');

// Orders
Route::get('/order/all', 'Orders@all')->name('order.all');
Route::post('/order/datatables', 'Orders@datatables')->name('order.datatables');
Route::post('/order/get_details', 'Orders@get_details')->name('order.get_details');
Route::get('/order/edit/{id}', 'Orders@edit')->name('order.edit');
Route::post('order/process/{action}', 'Orders@process')->name('order.process');
Route::get('/order/awb/{id}/{view_mode}', 'Orders@awb')->name('order.awb');
Route::post('order/awb_process/{action}', 'Orders@awb_process')->name('order.awb_process');
Route::get('order/print_awb/{awb_number}', 'Orders@print_awb')->name('order.print_awb');
Route::get('/order/print/{id}/{view_mode}', 'Orders@print')->name('order.print');
Route::post('/order/autocomplete_shipping_city', 'Orders@autocomplete_shipping_city')->name('order.autocomplete_shipping_city');

// Awbs
Route::get('/awbs/update_statuses', 'Awbs@update_statuses')->name('awbs.update_statuses');

// Mails
Route::get('/mails/send_emails', 'Mails@send_emails')->name('mails.send_emails');
Route::get('/mails/view_email/{id}', 'Mails@view_email')->name('mails.view_email');

// Deliveries
Route::get('/delivery/all', 'Deliveries@all')->name('delivery.all');
Route::post('/delivery/datatables', 'Deliveries@datatables')->name('delivery.datatables');

// My Account
Route::get('/myaccount/profile', 'MyAccount@profile')->name('myaccount.profile');
Route::post('myaccount/profile_save', 'MyAccount@profile_save')->name('myaccount.profile_save');
Route::get('/myaccount/couriers', 'MyAccount@couriers')->name('myaccount.couriers');
Route::get('/myaccount/add_courier/{id}', 'MyAccount@add_courier')->name('myaccount.add_courier');
Route::get('/myaccount/edit_courier/{id}', 'MyAccount@edit_courier')->name('myaccount.edit_courier');
Route::post('myaccount/courier_process/{action}/{id}', 'MyAccount@courier_process')->name('myaccount.courier_process');

// DecoPlus API
Route::post('/script/dp_api', 'Scripts@dp_api')->name('script.dp_api');