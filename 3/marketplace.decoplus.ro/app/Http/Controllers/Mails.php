<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\Emails;
use App\Models\Emails_sending;
use DB;

use Mail;
use Auth;

class Mails extends Controller {

	protected $Scripts;

	public function __construct(Scripts $Scripts) {
		$this->middleware('auth');
		$this->Scripts = $Scripts;
	}


	public function create_email($data){

		// test
		// $data = [
		// 'name_from' => 'DecoPlus Marketplace',
		// 'mail_from' => 'marketplace@decoplus.ro',
		// 	'name_to' => 'Dani Manda',
		// 	'mail_to' => 'danny@andalio.ro',
		// 	'template' => 'mail.new_order',
		// 	'subject' => 'Test creare si trimitere mail',
		// 	'content_data' => json_encode([
		// 		'name' => 'danny'
		// 	])
		// ];

		if(!isset($data['name_from'])){
			$data['name_from'] = 'DecoPlus Marketplace';
		}

		if(!isset($data['mail_from'])){
			$data['mail_from'] = 'marketplace@decoplus.ro';
		}

		// inserare in emails + emails_sendings
		$email_id = Emails::insertGetId($data);

		if($email_id){
			$email_sending_id = Emails_sending::insertGetId(['email_id' => $email_id]);

			if(!$email_sending_id){
				// Save log
				$log_data = [
					'action' => 'create_email_sending',
					'details' => json_encode([
						'info' => 'Nu s-a reusit adaugarea mailului in emails_sendings.',
						'email_id' => $email_id
					])
				];
				$this->Scripts->save_log($log_data);

			}
		} else {
			// Save log
			$log_data = [
				'action' => 'create_email',
				'details' => json_encode([
					'info' => 'Nu s-a reusit crearea mailului.',
					'data' => $data
				])
			];
			$this->Scripts->save_log($log_data);
		}

	}



	public function send_emails(){

		// avem un tabel care stocheaza mailurile ce vor trebui trimise - emails_sendings
		// acest script va rula in cron, va verifica tabelul si va trimite mailurile
		// dupa ce mailul a fost trimis, va fi elimintat din emails_sendings

		// Preluam emailurile netrimise
		$emails = Emails_sending::select()
		->join('emails', 'emails.email_id', 'emails_sendings.email_id')
		->get();

        // Luam emailurile netrimise pe rand
		foreach ($emails as $key => $email) {

        	// Salvam emailul curent intr-o proprietate
			$this->email = $email;

        	// Trimitem emailul
			$email_sent = Mail::send(
				[
					'html' => $this->email['template']
				],
				json_decode($this->email['content_data'], true),
				function($message) {
					$message->subject($this->email['subject']);
					$message->to($this->email['mail_to'], $this->email['name_to']);
					$message->from($this->email['mail_from'], $this->email['name_from']);
				}
			);

        	// Daca nu avem erori la trimiterea mailurilor
			if(count(Mail::failures()) === 0){

        		// Stergem emailul deja trimis din emails_sendings
				$delete_email = Emails_sending::where('email_sending_id', $this->email['email_sending_id'])->delete();

        		// Daca stergea nu s-a efectuat salvam in log
				if(!$delete_email){
					
        			// Save log
					$log_data = [
						'action' => 'send_mail',
						'details' => json_encode([
							'info' => 'Nu s-a sters emailul din emails_sendings.',
							'email_id' => $this->email['email_id'],
							'email_sending_id' => $this->email['email_sending_id']
						])
					];

					$this->Scripts->save_log($log_data);

				}

				// Actualizam sent_at din emails
				Emails::where('email_id', $this->email['email_id'])->update(['sent_at' => date('Y-m-d H:i:s')]);

				echo $key . '. ' . $this->email['subject'] . ' - trimis<br>';

			} else {
				// Save log
				$log_data = [
					'action' => 'send_mail',
					'details' => json_encode([
						'info' => 'S-a blocat trimiterea de mailuri.'
					])
				];

				$this->Scripts->save_log($log_data);

				echo $key . '. ' . $this->email['subject'] . ' - netrimis<br>';
			}

		}

	}

	public function view_email($email_id){
		
		$data = Emails::select()
		->where('email_id', $email_id)
		->first();

		if(!$data){
			header('Location: ' . url('/'));
			exit;
		}

		return view($data['template'], json_decode($data['content_data'], true));
	}

}