<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\Brand;
use App\Models\Category;
use DB;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Auth;

class Products extends Controller {

    protected $Scripts;

    public function __construct(Scripts $Scripts) {
        $this->middleware('auth');
        $this->Scripts = $Scripts;
    }

    public function all() {
        return view('product.all');
    }

    public function add() {
        return view('product.add');
    }

    public function edit($product_id) {

        $post_data = [
            'product_id' => $product_id
        ];

        $ch = curl_init();

        $url = 'https://www.decoplus.ro/index.php?route=api/product/get&api_token=' . $this->Scripts->getToken();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

        $result = json_decode(curl_exec($ch));

        curl_close($ch);

        $data['product'] = $result->product;

        return view('product.edit', $data);
    }

    public function clone($product_id) {

        $post_data = [
            'product_id' => $product_id
        ];

        $ch = curl_init();

        $url = 'https://www.decoplus.ro/index.php?route=api/product/get&api_token=' . $this->Scripts->getToken();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

        $result = json_decode(curl_exec($ch));

        curl_close($ch);

        $data['product'] = $result->product;

        return view('product.clone', $data);
    }

    public function process($action) {
        $error = [];
        $error_state = 0;

        if (!isset(request()->category)) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'category', 'msg' => 'Trebuie sa selectezi o categorie!']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'category']);
        }

        if (strlen(request('name')) < 10) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'name', 'msg' => 'Acest camp trebuie sa contina minim 10 caractere!']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'name']);
        }

        if (!isset(request()->brand)) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'brand', 'msg' => 'Trebuie sa selectezi un brand!']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'brand']);
        }

        if (strlen(request('model')) < 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'model', 'msg' => 'Acest camp trebuie sa contina minim 3 caractere!']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'model']);
        }

        if (strlen(request('description')) < 30) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'description', 'msg' => 'Descrierea trebuie sa contina minim 30 de caractere!']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'description']);
        }

        if (!isset(request()->selling_price)) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'selling_price', 'msg' => 'Acest camp este obligatoriu!']);
        } else {
            if(!is_numeric(request()->selling_price)){
                array_push($error, ['code' => '1', 'element_id' => 'selling_price', 'msg' => 'Acest camp trebuie sa fie numeric!']);
            } else {
                array_push($error, ['code' => '0', 'element_id' => 'selling_price']);
            }
        }

        if (isset(request()->recommended_price)) {
            if(!is_numeric(request()->recommended_price)){
                array_push($error, ['code' => '1', 'element_id' => 'recommended_price', 'msg' => 'Acest camp trebuie sa fie numeric!']);
            } else {
                array_push($error, ['code' => '0', 'element_id' => 'recommended_price']);
            }
        }

        if (!isset(request()->quantity)) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'quantity', 'msg' => 'Acest camp este obligatoriu!']);
        } else {
            if(!is_numeric(request()->quantity)){
                array_push($error, ['code' => '1', 'element_id' => 'quantity', 'msg' => 'Acest camp trebuie sa fie numeric!']);
            } else {
                array_push($error, ['code' => '0', 'element_id' => 'quantity']);
            }
        }

        if (!isset(request()->weight)) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'weight', 'msg' => 'Acest camp este obligatoriu!']);
        } else {
            if(!is_numeric(request()->weight)){
                array_push($error, ['code' => '1', 'element_id' => 'weight', 'msg' => 'Acest camp trebuie sa fie numeric!']);
            } else {
                array_push($error, ['code' => '0', 'element_id' => 'weight']);
            }
        }

        if (!isset(request()->uploaded_images)) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'product_images', 'msg' => 'Trebuie sa selectezi cel putin o imagine!']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'product_images']);
        }




        if ($error_state) {
            $data['error'] = $error;
            return ($data);
        } else {

            if ($action == 'add') {

                $post_data = [
                    'vendor_id' => Auth::user()->id,
                    'model' => request()->model,
                    'quantity' => request()->quantity,
                    'manufacturer_id' => $this->getBrand(request()->brand),
                    'price' => request()->recommended_price,
                    'weight' => request()->weight,
                    'status' => request()->active === 'on' ? 1 : '0',
                    'product_description' => [
                        2 => [
                            'name' => request()->name,
                            'meta_title' => request()->name,
                            'description' => request()->description,
                            'meta_description' => 'Deco Plus ' . request()->name
                        ]
                    ],
                    'product_special' => [
                        0 =>[
                            'price' => request()->selling_price
                        ]
                    ],
                    'product_category' => [
                        0 => request()->category
                    ],
                    'product_images' => request()->uploaded_images
                ];

                $ch = curl_init();

                $url = 'https://www.decoplus.ro/index.php?route=api/product/add&api_token=' . $this->Scripts->getToken();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

                $product_id = json_decode(curl_exec($ch));

                curl_close($ch);

                $data = [];
                $data['success'] = ['id' => $product_id];
                $data['action'] = 'add';
                $data['url'] = route('product.all');
                return $data;

            } elseif ($action == 'edit') {
                $post_data = [
                    'product_id' => request()->product_id,
                    'model' => request()->model,
                    'quantity' => request()->quantity,
                    'manufacturer_id' => $this->getBrand(request()->brand),
                    'price' => request()->recommended_price,
                    'weight' => request()->weight,
                    'status' => request()->active === 'on' ? 1 : '0',
                    'product_description' => [
                        2 => [
                            'name' => request()->name,
                            'meta_title' => request()->name,
                            'description' => request()->description,
                            'meta_description' => 'Deco Plus ' . request()->name
                        ]
                    ],
                    'product_special' => [
                        0 =>[
                            'price' => request()->selling_price
                        ]
                    ],
                    'product_category' => [
                        0 => request()->category
                    ],
                    'product_images' => request()->uploaded_images
                ];


                $ch = curl_init();

                $url = 'https://www.decoplus.ro/index.php?route=api/product/edit&api_token=' . $this->Scripts->getToken();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

                $result = json_decode(curl_exec($ch));

                curl_close($ch);

                $data = [];
                $data['msg'] = $result->msg;
                $data['success_msg'] = 'Modificarile au fost salvate cu succes!';
                $data['action'] = 'edit';
                // $data['url'] = route('product.all');
                return $data;
            }
        }
    }

    public function import($view_mode){

        // daca este deschis ca Modal sau singur in pagina
        if($view_mode === 'parent'){
            $data['view_mode'] = '_parent';
        }  elseif($view_mode === 'single') {
            $data['view_mode'] = '';
        } else{
            // Save log
            $log_data = [
                'action' => 'import_products',
                'details' => json_encode([
                    'info' => 'Nu stiu ce incerci sa faci, dar s-ar putea sa te fi prins!'
                ])
            ];
            $this->Scripts->save_log($log_data);

            echo 'Nu stiu ce incerci sa faci, dar s-ar putea sa te fi prins!';
            exit;
        }

        return view('product.import', $data);
    }

    public function import_process(){

        $run_start = microtime(true);

        if(request()->import_file_xls){

            $extension = request()->import_file_xls->getClientOriginalExtension();
            
            if(!in_array($extension, ['xls', 'xlsx'])){
                // Save log
                $log_data = [
                    'action' => 'import_products',
                    'details' => json_encode([
                        'info' => 'S-a selectat un fisier cu o extensie nepermisa!',
                        'filename' => request()->import_file_xls->getClientOriginalName()
                    ])
                ];
                $this->Scripts->save_log($log_data);

                $data['error_msg'] = 'Ai selectat un fisier nepermis!';
            } else {
                // denumire fisier
                $fileName = time() . '_' . request()->import_file_xls->getClientOriginalName();

                // salvam fisierul
                request()->import_file_xls->move(public_path() . '/uploads/imports/excel/', $fileName);

                // prelucram fisierul
                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(public_path() . '/uploads/imports/excel/' . $fileName);
                $worksheet = $spreadsheet->getActiveSheet();
                $excel_rows = $worksheet->toArray();

                // eliminam primele randuri din rezultate
                unset($excel_rows[0]);
                unset($excel_rows[1]);
                unset($excel_rows[3]);
                unset($excel_rows[4]);

                // facem niste prelucrari
                // schimbat key-urile din array-uri cu denumirile parametrilor din excel
                foreach ($excel_rows as $k_r => $row) {
                    foreach ($row as $k_c => $cell) {
                        $excel_products[$k_r][$excel_rows[2][$k_c]] = $cell;
                    }
                }

                // eliminam si array-ul cu denumirile parametrilor
                unset($excel_products[2]);

                // resetam key-urile array-urilor principale
                $excel_products = array_values($excel_products);

                $error = [];
                $error_state = 0;
                
                // luam produsele pe rand si facem verificari
                foreach ($excel_products as $key => $product) {
                    if(!isset($product['category_id'])){
                        $error_state = 1;
                        array_push($error, ['item' => 'category_id', 'msg' => 'Campul <b>Categorie</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    } else {
                        if(!is_numeric($product['category_id'])){
                            $error_state = 1;
                            array_push($error, ['item' => 'category_id', 'msg' => 'Campul <b>Categorie</b> trebuie sa fie numeric! [randul ' . ($key+6) . ']']);
                        }
                    }

                    if(!isset($product['name']) || strlen($product['name']) < 10) {
                        $error_state = 1;
                        array_push($error, ['item' => 'name', 'msg' => 'Campul <b>Denumire</b> este obligatoriu! Minim 10 caractere [randul ' . ($key+6) . ']']);
                    }

                    if(!isset($product['brand'])) {
                        $error_state = 1;
                        array_push($error, ['item' => 'brand', 'msg' => 'Campul <b>Brand</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    }

                    if(!isset($product['model'])) {
                        $error_state = 1;
                        array_push($error, ['item' => 'model', 'msg' => 'Campul <b>Cod produs</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    }

                    if(!isset($product['description']) || strlen($product['description']) < 30) {
                        $error_state = 1;
                        array_push($error, ['item' => 'name', 'msg' => 'Campul <b>Descriere</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    }

                    if(!isset($product['quantity'])) {
                        $error_state = 1;
                        array_push($error, ['item' => 'quantity', 'msg' => 'Campul <b>Cantitate stoc</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    }

                    if(!isset($product['weight'])){
                        $error_state = 1;
                        array_push($error, ['item' => 'weight', 'msg' => 'Campul <b>Greutate</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    } else {
                        if(!is_numeric($product['weight'])){
                            $error_state = 1;
                            array_push($error, ['item' => 'weight', 'msg' => 'Campul <b>Greutate</b> trebuie sa fie numeric! [randul ' . ($key+6) . ']']);
                        }
                    }

                    if(!isset($product['active'])){
                        $error_state = 1;
                        array_push($error, ['item' => 'active', 'msg' => 'Campul <b>Activ</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    } else {
                        if(!is_numeric($product['active'])){
                            $error_state = 1;
                            array_push($error, ['item' => 'active', 'msg' => 'Campul <b>Activ</b> trebuie sa fie numeric! [randul ' . ($key+6) . ']']);
                        }
                    }

                    if(!isset($product['main_image_url'])) {
                        $error_state = 1;
                        array_push($error, ['item' => 'main_image_url', 'msg' => 'Campul <b>URL imagine principala</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    } else {
                        $image_url_parts = explode('.', $product['main_image_url']);

                        $extension = end($image_url_parts);

                        if(!in_array($extension, ['jpg', 'jpeg', 'png'])){
                            $error_state = 1;
                            array_push($error, ['item' => 'main_image_url', 'msg' => 'Campul <b>URL imagine principala</b> permite extensiile: jpg, jpeg, png! [randul ' . ($key+6) . ']']);
                        }
                    }

                    if(isset($product['other_image_url_1'])) {
                        $image_url_parts = explode('.', $product['other_image_url_1']);

                        $extension = end($image_url_parts);

                        if(!in_array($extension, ['jpg', 'jpeg', 'png'])){
                            $error_state = 1;
                            array_push($error, ['item' => 'other_image_url_1', 'msg' => 'Campul <b>URL imagine secundara 1</b> permite extensiile: jpg, jpeg, png! [randul ' . ($key+6) . ']']);
                        }
                    }

                    if(isset($product['other_image_url_2'])) {
                        $image_url_parts = explode('.', $product['other_image_url_2']);

                        $extension = end($image_url_parts);

                        if(!in_array($extension, ['jpg', 'jpeg', 'png'])){
                            $error_state = 1;
                            array_push($error, ['item' => 'other_image_url_2', 'msg' => 'Campul <b>URL imagine secundara 2</b> permite extensiile: jpg, jpeg, png! [randul ' . ($key+6) . ']']);
                        }
                    }

                    if(isset($product['other_image_url_3'])) {
                        $image_url_parts = explode('.', $product['other_image_url_3']);

                        $extension = end($image_url_parts);

                        if(!in_array($extension, ['jpg', 'jpeg', 'png'])){
                            $error_state = 1;
                            array_push($error, ['item' => 'other_image_url_3', 'msg' => 'Campul <b>URL imagine secundara 3</b> permite extensiile: jpg, jpeg, png! [randul ' . ($key+6) . ']']);
                        }
                    }

                    if(!isset($product['selling_price'])){
                        $error_state = 1;
                        array_push($error, ['item' => 'selling_price', 'msg' => 'Campul <b>Pret vanzare (pret nou)</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    } else {
                        if(!is_numeric($product['selling_price'])){
                            $error_state = 1;
                            array_push($error, ['item' => 'selling_price', 'msg' => 'Campul <b>Pret vanzare (pret nou)</b> trebuie sa fie numeric! [randul ' . ($key+6) . ']']);
                        }
                    }

                    if(!isset($product['recommended_price'])){
                        $error_state = 1;
                        array_push($error, ['item' => 'recommended_price', 'msg' => 'Campul <b>Pret recomandat (pret vechi)</b> este obligatoriu! [randul ' . ($key+6) . ']']);
                    } else {
                        if(!is_numeric($product['recommended_price'])){
                            $error_state = 1;
                            array_push($error, ['item' => 'recommended_price', 'msg' => 'Campul <b>Pret recomandat (pret vechi)</b> trebuie sa fie numeric! [randul ' . ($key+6) . ']']);
                        }
                    }
                }

                // daca au aparut erori, le trimitem ca raspuns
                if ($error_state) {
                    // Save log
                    $log_data = [
                        'action' => 'import_products',
                        'details' => json_encode([
                            'info' => 'Erori import produse din fisier excel.',
                            'import_file' => public_path() . '/uploads/imports/excel/' . $fileName,
                            'errors' => $error
                        ])
                    ];
                    $this->Scripts->save_log($log_data);

                    $data['error'] = $error;
                    $data['error_msg'] = 'Te rugam sa corectezi erorile de mai jos si sa incerci din nou!';
                } else {
                    // daca nu am avut erori, trecem la inserare produse
                    foreach ($excel_products as $key => $product) {
                        $post_data = [
                            'vendor_id' => Auth::user()->id,
                            'model' => $product['model'],
                            'quantity' => $product['quantity'],
                            'manufacturer_id' => $this->getBrand($product['brand']),
                            'price' => $product['recommended_price'],
                            'weight' => $product['weight'],
                            'status' => $product['active'],
                            'product_description' => [
                                2 => [
                                    'name' => $product['name'],
                                    'meta_title' => $product['name'],
                                    'description' => $product['description'],
                                    'meta_description' => 'Deco Plus ' . $product['name']
                                ]
                            ],
                            'product_special' => [
                                0 =>[
                                    'price' => $product['selling_price']
                                ]
                            ],
                            'product_category' => [
                                0 => $product['category_id']
                            ],
                            'product_images' => [
                                0 => $product['main_image_url'],
                                1 => $product['other_image_url_1'],
                                2 => $product['other_image_url_2'],
                                3 => $product['other_image_url_3']
                            ],
                            'add_type' => 'import'
                        ];

                        $ch = curl_init();

                        $url = 'https://www.decoplus.ro/index.php?route=api/product/add&api_token=' . $this->Scripts->getToken();

                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

                        $product_id = json_decode(curl_exec($ch));

                        curl_close($ch);

                        if(!$product_id){
                            // Save log
                            $log_data = [
                                'action' => 'import_products',
                                'details' => json_encode([
                                    'info' => 'Eroare la adaugare produs prin API Decoplus.',
                                    'import_file' => public_path() . '/uploads/imports/excel/' . $fileName
                                ])
                            ];
                            $this->Scripts->save_log($log_data);

                            $data['error_msg'] = 'A aparut o eroare la importul produselor. Te rugam sa ne contactezi!';

                            return $data;
                        }

                    }

                    $data['success_msg'] = count($excel_products) . ' produse au fost importate cu succes!';

                    $execution_time = round(microtime(true) - $run_start, 2);

                    // Save log
                    $log_data = [
                        'action' => 'import_products',
                        'details' => json_encode([
                            'info' => 'Import produse cu succes!',
                            'import_file' => public_path() . '/uploads/imports/excel/' . $fileName,
                            'products_number' => count($excel_products),
                            'execution_time' => $execution_time
                        ])
                    ];
                    $this->Scripts->save_log($log_data);
                }

            }

        } else {
            $data['error_msg'] = 'Nu ai selectat un fisier!';
        }
        

        return $data;

    }

    public function autocomplete_brands() {
        $brands = Brand::select('brand_id AS id', 'name AS text')
        ->where('name', 'like', '%' . request('search') . '%')
        ->get();

        $data['items'] = $brands;

        return json_encode($data);
    }

    public function autocomplete_categories() {
        $categories = Category::select(
            'categories.category_id AS id',
            DB::raw('CONCAT(categories.name," [",categories.category_id,"]") as text')
        )
        ->join('users_categories_relations', 'users_categories_relations.category_id', 'categories.category_id')
        ->where('users_categories_relations.user_id', Auth::user()->id)
        ->where('name', 'like', '%' . request('search') . '%')
        ->get();

        $data['items'] = $categories;

        return json_encode($data);
    }


    public function images_upload() {

        $images = [];

        foreach (request()->files as $files) {
            foreach ($files as $key => $file) {
                $images[] = $this->Scripts->upload_image($file);
            }
        }

        return json_encode($images);
    }


    public function getBrand($brand){

        // verificam daca brandul exista
        $brands = Brand::select()
        ->where('brand_id', request()->brand)
        ->first();

        // daca nu exista il generam
        if(!$brands){
            $post_data = [
                'name' => $brand
            ];

            $ch = curl_init();

            $url = 'https://www.decoplus.ro/index.php?route=api/product/add_brand&api_token=' . $this->Scripts->getToken();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

            $brand = json_decode(curl_exec($ch));

            curl_close($ch);

            return $brand->brand_id;
        } else{
            return $brand;
        }

    }



}
