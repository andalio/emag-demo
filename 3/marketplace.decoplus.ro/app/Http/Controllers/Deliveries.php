<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\Awbs_status;
use App\Models\Awb;
use DB;

use Auth;

class Deliveries extends Controller {

    protected $Scripts;

    public function __construct(Scripts $Scripts) {
        $this->middleware('auth');
        $this->Scripts = $Scripts;
    }

    public function all() {
        // statusuri awb
        $data['awbs_statuses'] = Awbs_status::select()->get();

        return view('delivery.all', $data);
    }


    public function datatables() {
        $recordsTotal = AWB::select()
        ->join('orders', 'orders.order_id', 'awbs.order_id')
        ->where('orders.vendor_id', Auth::user()->id)
        ->whereNull('awbs.deleted_at')
        ->count();

        $awbs_query = AWB::select(
            'awbs.awb_id',
            'awbs.awb_number',
            'awbs.order_id',
            'awbs.shipping_contact',
            'awbs.shipping_telephone',
            'couriers.name AS courier_name',
            DB::raw('CONCAT(FORMAT(cash_on_delivery, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS cash_on_delivery'),
            DB::raw('DATE_FORMAT(awbs.created_at, "%d-%m-%Y") AS created_at_day'),
            DB::raw('DATE_FORMAT(awbs.created_at, "%H:%i:%s") AS created_at_hour'),
            'awbs_statuses.name AS awb_status',
            'awbs_statuses.class AS awb_status_class',
            DB::raw('DATE_FORMAT(history_awbs.created_at, "%d-%m-%Y") AS last_status_day'),
            DB::raw('DATE_FORMAT(history_awbs.created_at, "%H:%i:%s") AS last_status_hour')
        )
        ->join('vendors_couriers', 'vendors_couriers.vendor_courier_id', 'awbs.vendor_courier_id')
        ->join('couriers', 'couriers.courier_id', 'vendors_couriers.courier_id')
        ->join('history_awbs', 'history_awbs.history_id', 'awbs.history_id')
        ->join('awbs_statuses', 'awbs_statuses.awb_status_id', 'history_awbs.awb_status_id')
        ->join('orders', 'orders.order_id', 'awbs.order_id')
        ->where('orders.vendor_id', Auth::user()->id)
        ->whereNull('awbs.deleted_at');

        // filtru Numar AWB
        if(request()->filter_awb_id){
            $awbs_query->where('awbs.awb_id', request()->filter_awb_id);
        }

        // filtru ID comanda
        if(request()->filter_order_id){
            $awbs_query->where('awbs.order_id', request()->filter_order_id);
        }

        // filtru Status
        if(request()->filter_awb_status_id){
            $awbs_query->where('history_awbs.awb_status_id', request()->filter_awb_status_id);
        }

        $recordsFiltered = $awbs_query->count();

        $orders_data = $awbs_query
        ->limit(request()->length)
        ->offset(request()->start)
        ->orderby(request()->columns[request()->order[0]['column']]['data'], request()->order[0]['dir'])
        ->get();

        $data['draw'] = request()->draw;
        $data['recordsTotal'] = $recordsTotal;
        $data['recordsFiltered'] = $recordsFiltered;
        $data['data'] = $orders_data;

        return json_encode($data);
    }
    

}
