<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\Product;

use Auth;

class Dashboard extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {

		$data['products'] = Product::select()
		->where('vendor_id', Auth::user()->id)
		->where('status', 1)
		->where('valid', 1)
		->count();

		print_r($data['products']);

		return view('dashboard.index');
	}

}
