<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Order;
use App\Models\Orders_product;
use App\Models\Orders_status;
use App\Models\Courier;
use App\Models\Awb;
use App\Models\History_awb;
use App\Models\County;
use DB;

use Auth;

class Orders extends Controller {

    protected $Scripts;

    public function __construct(Scripts $Scripts) {
        $this->middleware('auth');
        $this->Scripts = $Scripts;
    }

    public function all() {
        // statusuri comenzi
        $data['orders_statuses'] = Orders_status::select()->get();

        return view('order.all', $data);
    }

    public function edit($order_id) {
        $data['order'] = Order::select(
            'order_id',
            DB::raw('DATE_FORMAT(date_added_in_dp, "%d-%m-%Y") AS date_added_day'),
            'payment_method',
            'payment_company',
            'payment_firstname',
            'payment_lastname',
            DB::raw('CONCAT(payment_firstname," ",payment_lastname) as payment_name'),
            'telephone AS payment_telephone',
            'payment_address_1 AS payment_address',
            'payment_zone_id',
            DB::raw('(SELECT name FROM counties WHERE county_id = orders.payment_zone_id) as payment_zone'),
            'payment_city',
            'shipping_company',
            'shipping_firstname',
            'shipping_lastname',
            DB::raw('CONCAT(shipping_firstname," ",shipping_lastname) as shipping_name'),
            'telephone AS shipping_telephone',
            'shipping_address_1 AS shipping_address',
            'shipping_zone_id',
            DB::raw('(SELECT name FROM counties WHERE county_id = orders.shipping_zone_id) as shipping_zone'),
            'shipping_city',
            DB::raw('CONCAT(FORMAT(total + shipping_cost, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS total'),
            DB::raw('CONCAT(FORMAT(shipping_cost, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS shipping_cost'),
            'order_status_id'
        )
        ->where('order_id', $order_id)
        ->where('vendor_id', Auth::user()->id)
        ->whereNull('deleted_at')
        ->first();

        // daca nu am gasit date despre comanda inseamna ca ceva nu e in regula
        // facem redirect la lista de comenzi
        if(!$data['order']){
            header('Location: ' . route('order.all'));
            exit;
        }

        $data['order_products'] = Orders_product::select(
            'name',
            'model',
            DB::raw('CONCAT(quantity, " ", "' . Config::get('constants.QUANTITY_UNIT') . '") AS quantity'),
            DB::raw('CONCAT(FORMAT(price, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS price'),
            DB::raw('CONCAT(FORMAT(total, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS total')
        )
        ->where('order_id', $order_id)
        ->get();

        $data['order_awbs'] = AWB::select(
            'couriers.courier_id AS courier_id',
            'couriers.name AS courier_name',
            'awbs.awb_number',
            DB::raw('DATE_FORMAT(awbs.created_at, "%d-%m-%Y") AS created_at_day'),
            DB::raw('DATE_FORMAT(awbs.created_at, "%H:%i:%s") AS created_at_hour'),
            'awbs_statuses.name AS awb_status',
            'awbs_statuses.class AS awb_status_class',
            DB::raw('DATE_FORMAT(history_awbs.created_at, "%d-%m-%Y") AS last_status_day'),
            DB::raw('DATE_FORMAT(history_awbs.created_at, "%H:%i:%s") AS last_status_hour')
        )
        ->join('vendors_couriers', 'vendors_couriers.vendor_courier_id', 'awbs.vendor_courier_id')
        ->join('couriers', 'couriers.courier_id', 'vendors_couriers.courier_id')
        ->join('history_awbs', 'history_awbs.history_id', 'awbs.history_id')
        ->join('awbs_statuses', 'awbs_statuses.awb_status_id', 'history_awbs.awb_status_id')
        ->where('order_id', $order_id)
        ->whereNull('awbs.deleted_at')
        ->orderby('awbs.awb_id', 'DESC')
        ->get();

        $data['orders_statuses'] = Orders_status::select()->get();

        $data['counties'] = County::select(
            'county_id',
            'name'
        )
        ->get();

        return view('order.edit', $data);
    }

    public function print($order_id, $view_mode) {

        // daca este deschis ca Modal sau singur in pagina
        if($view_mode === 'parent'){
            $data['view_mode'] = '_parent';
        }  elseif($view_mode === 'single') {
            $data['view_mode'] = '';
        } else{
            // Save log
            $log_data = [
                'action' => 'print_awb',
                'details' => json_encode([
                    'info' => 'Nu stiu ce incerci sa faci, dar s-ar putea sa te fi prins!'
                ])
            ];
            $this->Scripts->save_log($log_data);

            echo 'Nu stiu ce incerci sa faci, dar s-ar putea sa te fi prins!';
            exit;
        }

        $data['order'] = Order::select(
            'order_id',
            DB::raw('DATE_FORMAT(date_added_in_dp, "%d-%m-%Y") AS date_added_day'),
            'payment_method',
            'payment_company',
            DB::raw('CONCAT(payment_firstname," ",payment_lastname) as payment_name'),
            'telephone AS payment_telephone',
            'payment_address_1 AS payment_address',
            DB::raw('(SELECT name FROM counties WHERE county_id = orders.payment_zone_id) as payment_zone'),
            'payment_city',
            'shipping_company',
            DB::raw('CONCAT(shipping_firstname," ",shipping_lastname) as shipping_name'),
            'telephone AS shipping_telephone',
            'shipping_address_1 AS shipping_address',
            DB::raw('(SELECT name FROM counties WHERE county_id = orders.shipping_zone_id) as shipping_zone'),
            'shipping_city',
            DB::raw('CONCAT(FORMAT(total + shipping_cost, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS total'),
            DB::raw('CONCAT(FORMAT(shipping_cost, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS shipping_cost'),
            'order_status_id'
        )
        ->where('order_id', $order_id)
        ->where('vendor_id', Auth::user()->id)
        ->whereNull('deleted_at')
        ->first();

        // daca nu am gasit date despre comanda inseamna ca ceva nu e in regula
        // facem redirect la lista de comenzi
        if(!$data['order']){
            header('Location: ' . route('order.all'));
            exit;
        }

        $data['order_products'] = Orders_product::select(
            'name',
            'model',
            DB::raw('CONCAT(quantity, " ", "' . Config::get('constants.QUANTITY_UNIT') . '") AS quantity'),
            DB::raw('CONCAT(FORMAT(price, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS price'),
            DB::raw('CONCAT(FORMAT(total, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS total')
        )
        ->where('order_id', $order_id)
        ->get();

        $data['order_awbs'] = AWB::select(
            'couriers.name AS courier_name',
            'awbs.awb_number',
            DB::raw('DATE_FORMAT(awbs.created_at, "%d-%m-%Y") AS created_at_day'),
            DB::raw('DATE_FORMAT(awbs.created_at, "%H:%i:%s") AS created_at_hour'),
            'awbs_statuses.name AS awb_status',
            'awbs_statuses.class AS awb_status_class',
            DB::raw('DATE_FORMAT(history_awbs.created_at, "%d-%m-%Y") AS last_status_day'),
            DB::raw('DATE_FORMAT(history_awbs.created_at, "%H:%i:%s") AS last_status_hour')
        )
        ->join('vendors_couriers', 'vendors_couriers.vendor_courier_id', 'awbs.vendor_courier_id')
        ->join('couriers', 'couriers.courier_id', 'vendors_couriers.courier_id')
        ->join('history_awbs', 'history_awbs.history_id', 'awbs.history_id')
        ->join('awbs_statuses', 'awbs_statuses.awb_status_id', 'history_awbs.awb_status_id')
        ->where('order_id', $order_id)
        ->orderby('awbs.awb_id', 'DESC')
        ->get();

        $data['orders_statuses'] = Orders_status::select()->get();

        return view('order.print', $data);

    }

    public function print_awb($awb_number){

        $awb = Awb::select(
            'awbs.*',
            'vendors_couriers.courier_id AS courier_id',
            'vendors_couriers.username AS username',
            'vendors_couriers.password AS password'
        )
        ->join('vendors_couriers', 'vendors_couriers.vendor_courier_id', 'awbs.vendor_courier_id')
        ->where('awbs.awb_number', $awb_number)
        ->first();

        // Sameday
        if($awb['courier_id'] === 1){

            // Redirectionare
            header('Location: ' . Config::get('constants.url_sameday') . 'awb/download-pdf/' . $awb_number . '/A4'); 

        }

        // Fan Courier
        if($awb['courier_id'] === 2){

            $post_data = [
                'nr' => $awb_number,
                'page' => 'A4',
                'language' => 'Ro'
            ];
            
            // Generare PDF
            $awb_result = $this->Scripts->do_requests_fan_courier($post_data, 'get_pdf');

            if(strpos(strtolower($awb_result), 'error') === false){

                header("Content-type:application/pdf");
                echo $awb_result;

            } else {
                // Save log
                $log_data = [
                    'action' => 'print_awb',
                    'details' => json_encode([
                        'info' => 'Eroare la printarea AWB-ului Fan Courier.',
                        'awb_number' => $awb_number
                    ])
                ];
                $this->Scripts->save_log($log_data);

                echo 'A aparut o eroare la generearea PDF-ului!';
            }


        }

        // Urgent Cargus
        if($awb['courier_id'] === 3){

            // Generare PDF
            $awb_result = $this->Scripts->do_requests_urgent_cargus('AwbDocuments?barCodes=' . $awb_number . '&type=PDF&format=0', [], 'GET');

            if($awb_result['status'] == '200'){

                header("Content-type:application/pdf");
                echo base64_decode($awb_result['message']);

            } else {
                // Save log
                $log_data = [
                    'action' => 'print_awb',
                    'details' => json_encode([
                        'info' => 'Eroare la printarea AWB-ului Urgent Cargus.',
                        'awb_number' => $awb_number
                    ])
                ];
                $this->Scripts->save_log($log_data);

                echo 'A aparut o eroare la generearea PDF-ului!';
            }


        }

    }

    public function process($action) {
        $error = [];
        $error_state = 0;

        if (strlen(request('order_payment_firstname')) < 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_payment_firstname', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_payment_firstname']);
        }

        if (strlen(request('order_payment_lastname')) < 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_payment_lastname', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_payment_lastname']);
        }

        // company ???

        if (strlen(request('order_payment_telephone')) < 10) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_payment_telephone', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_payment_telephone']);
        }

        if (strlen(request('order_payment_address')) < 10) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_payment_address', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_payment_address']);
        }

        if (strlen(request('order_payment_city')) < 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_payment_city', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_payment_city']);
        }


        if (strlen(request('order_shipping_firstname')) < 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_shipping_firstname', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_shipping_firstname']);
        }

        if (strlen(request('order_shipping_lastname')) < 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_shipping_lastname', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_shipping_lastname']);
        }

        // company ???

        if (strlen(request('order_shipping_telephone')) < 10) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_shipping_telephone', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_shipping_telephone']);
        }

        if (strlen(request('order_shipping_address')) < 10) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_shipping_address', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_shipping_address']);
        }

        if (strlen(request('order_shipping_city')) < 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'order_shipping_city', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'order_shipping_city']);
        }







        if ($error_state) {
            $data['error'] = $error;
            return ($data);
        } else {
            if ($action == 'edit') {

                // strangem datele pentru actualizare
                $data_update = [
                    'payment_company' => request()->order_payment_company,
                    'payment_firstname' => request()->order_payment_firstname,
                    'payment_lastname' => request()->order_payment_lastname,
                    'payment_address_1' => request()->order_payment_address,
                    'payment_zone_id' => request()->order_payment_zone_id,
                    'payment_city' => request()->order_payment_city,

                    'shipping_company' => request()->order_shipping_company,
                    'shipping_firstname' => request()->order_shipping_firstname,
                    'shipping_lastname' => request()->order_shipping_lastname,
                    'shipping_address_1' => request()->order_shipping_address,
                    'shipping_zone_id' => request()->order_shipping_zone_id,
                    'shipping_city' => request()->order_shipping_city,

                    'telephone' => request()->order_payment_telephone,

                    'order_status_id' => request()->order_status_id
                ];

                // payment_zone ??? - acum se primeste direct payment_zone_id

                // Actualizam in baza de date
                if(Order::where('order_id', request('order_id'))->update($data_update)){
                    $data['success_msg'] = 'Modificarile au fost salvate!';
                } else {
                    // Save log
                    $log_data = [
                        'action' => 'edit_order',
                        'details' => json_encode([
                            'info' => 'Eroare la editarea unei comenzi.',
                            'order_id' => request('order_id'),
                            'data_update' => $data_update
                        ])
                    ];
                    $this->Scripts->save_log($log_data);

                    $data['error_msg'] = 'A aparut o eroare la salvarea modificarilor!';
                }

                return $data;
            }
        }

    }

    public function datatables() {
        $recordsTotal = Order::select()
        ->where('vendor_id', Auth::user()->id)
        ->whereNull('deleted_at')
        ->count();

        $orders_query = Order::select(
            'order_id',
            DB::raw('CONCAT(firstname," ",lastname) as client_name'),
            'payment_method',
            DB::raw('DATE_FORMAT(date_added_in_dp, "%d-%m-%Y") AS date_added_day'),
            DB::raw('DATE_FORMAT(date_added_in_dp, "%H:%i:%s") AS date_added_hour'),
            'telephone',
            DB::raw('CONCAT(FORMAT(total + shipping_cost, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS total'),
            'orders_statuses.name AS status_name',
            'orders_statuses.class AS status_class',
            'date_added_in_dp'
        )
        ->join('orders_statuses', 'orders_statuses.order_status_id', 'orders.order_status_id')
        ->where('vendor_id', Auth::user()->id)
        ->whereNull('deleted_at');

        // filtru ID comanda
        if(request()->filter_order_id){
            $orders_query->where('orders.order_id', request()->filter_order_id);
        }

        // filtru Status
        if(request()->filter_order_status_id){
            $orders_query->where('orders.order_status_id', request()->filter_order_status_id);
        }

        $recordsFiltered = $orders_query->count();

        $orders_data = $orders_query
        ->limit(request()->length)
        ->offset(request()->start)
        ->orderby(request()->columns[request()->order[0]['column']]['data'], request()->order[0]['dir'])
        ->get();

        $data['draw'] = request()->draw;
        $data['recordsTotal'] = $recordsTotal;
        $data['recordsFiltered'] = $recordsFiltered;
        $data['data'] = $orders_data;

        return json_encode($data);
    }

    public function get_details(){

        $order = Order::select(
            'order_id',
            DB::raw('CONCAT(shipping_firstname," ",shipping_lastname) AS shipping_name'),
            'telephone AS shipping_telephone',
            'shipping_address_1 AS shipping_address',
            'shipping_city',
            DB::raw('(SELECT name FROM counties WHERE county_id = orders.shipping_zone_id) as shipping_zone'),
            DB::raw('CONCAT(FORMAT(total + shipping_cost, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS total'),
            DB::raw('CONCAT(FORMAT(shipping_cost, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS shipping_cost')
        )
        ->where('order_id', request('order_id'))
        ->whereNull('deleted_at')
        ->first();

        $products = Orders_product::select(
            'name',
            'model',
            DB::raw('CONCAT(quantity, " ", "' . Config::get('constants.QUANTITY_UNIT') . '") AS quantity'),
            DB::raw('CONCAT(FORMAT(total, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS total')
        )
        ->where('order_id', request('order_id'))
        ->get();

        $data['order'] = $order;
        $data['order_products'] = $products;

        return json_encode($data);
    }

    public function awb($order_id, $view_mode){

        // daca este deschis ca Modal sau singur in pagina
        if($view_mode === 'parent'){
            $data['view_mode'] = '_parent';
        }  elseif($view_mode === 'single') {
            $data['view_mode'] = '';
        } else {
            // Save log
            $log_data = [
                'action' => 'view_awb',
                'details' => json_encode([
                    'info' => 'Nu stiu ce incerci sa faci, dar s-ar putea sa te fi prins!'
                ])
            ];
            $this->Scripts->save_log($log_data);

            echo 'Nu stiu ce incerci sa faci, dar s-ar putea sa te fi prins!';
            exit;
        }

        // conturi de curier active
        $data['vendor_couriers'] = Courier::select(
            'vendors_couriers.courier_id AS courier_id',
            'couriers.name AS courier_name',
            'couriers.image AS image',
            'vendors_couriers.default AS default',
            'vendors_couriers.vendor_courier_id AS vendor_courier_id'
        )
        ->join('vendors_couriers', 'vendors_couriers.courier_id', '=', 'couriers.courier_id', 'left outer')
        ->whereNull('vendors_couriers.deleted_at')
        ->orderby('couriers.courier_id', 'ASC')
        ->get();

        $data['order'] = Order::select(
            'order_id',
            DB::raw('CONCAT(shipping_firstname," ",shipping_lastname) as shipping_name'),
            'telephone AS shipping_telephone',
            'shipping_address_1 AS shipping_address',
            DB::raw('(SELECT name FROM counties WHERE county_id = orders.shipping_zone_id) as shipping_zone'),
            'shipping_city',
            'shipping_company',
            'payment_code',
            DB::raw('FORMAT(weight, 0) AS weight'),
            DB::raw('CONCAT(FORMAT(total + shipping_cost, 2), " ", "' . Config::get('constants.CURRENCY') . '") AS total'),
            DB::raw('FORMAT(total, 2) AS cash_on_delivery')
        )
        ->where('order_id', $order_id)
        ->whereNull('deleted_at')
        ->first();

        return view('order.awb', $data);
    }


    public function autocomplete_shipping_city() {

        // date comanda
        $order = Order::select(
            DB::raw('(SELECT name FROM counties WHERE county_id = orders.shipping_zone_id) as shipping_zone'),
            'orders.shipping_zone_id AS shipping_zone_id',
            'counties_couriers.vendor_county_id AS vendor_county_id'
        )
        ->join('counties_couriers', 'counties_couriers.county_id', '=','orders.shipping_zone_id')
        ->where('orders.order_id', request('order_id'))
        ->where('counties_couriers.courier_id', (int)request('selected_courier'))
        ->first();

        // setari curier
        $courier = Courier::select()
        ->join('vendors_couriers', 'vendors_couriers.courier_id', '=', 'couriers.courier_id', 'left outer')
        ->where('vendors_couriers.courier_id', (int)request('selected_courier'))
        ->where('vendors_couriers.vendor_id', Auth::user()->id)
        ->whereNull('vendors_couriers.deleted_at')
        ->first();



        // Sameday
        if((int)request('selected_courier') === 1){

            // Preluam toate localitatile din judet
            $localities = json_decode($this->Scripts->do_requests_sameday('api/geolocation/city?county=' . $order['vendor_county_id'] . '&countPerPage=1000', [], "GET"))->data;

            // Pentru fiecare localitate preluata
            foreach($localities as $result){

                // Verificam daca se potriveste cu caracterele introduse in campul shipping_city
                $search_term = str_replace('-', ' ', strtolower(request('search')));
                if(strpos(strtolower($result->name), $search_term) !== false){
                    $data['items'][] = [
                        'id' => $result->id,
                        'postalCode' => $result->postalCode,
                        'text' => $result->name . ' [' . $order['shipping_zone'] . '] (' . $result->id . ')'
                    ];
                }

            }

            return json_encode($data);

        }

        // Fan Courier
        if((int)request('selected_courier') === 2){

            $post_data = [
                'judet' => strtolower($order['shipping_zone'])
            ];

            // Preluam toate localitatile din judet
            $localities = $this->Scripts->do_requests_fan_courier($post_data, 'get_localities');

            // Pentru fiecare localitate preluata
            foreach($localities as $result){

                // Verificam daca se potriveste cu caracterele introduse in campul shipping_city
                $search_term = str_replace('-', ' ', strtolower(request('search')));
                if(strpos(strtolower($result[1]), $search_term) !== false){
                    $data['items'][] = [
                        'id' => $result[1],
                        'postalCode' => '',
                        'text' => $result[1] . ' [' . $order['shipping_zone'] . '] (' . $result[5] . ')'
                    ];
                }

            }

            return json_encode($data);
        }

        // Urgent Cargus
        if((int)request('selected_courier') === 3){

            // Preluam toate localitatile din judet
            $localities = $this->Scripts->do_requests_urgent_cargus('Localities?countryId=1&countyID=' . $order['vendor_county_id'], [], 'GET');

            // Pentru fiecare localitate preluata
            foreach((array)json_decode($localities['message']) as $result){

                // Verificam daca se potriveste cu caracterele introduse in campul shipping_city
                $search_term = str_replace('-', ' ', strtolower(request('search')));
                if(strpos(strtolower($result->Name), $search_term) !== false){
                    $data['items'][] = [
                        'id' => $result->LocalityId,
                        'postalCode' => $result->PostalCode,
                        'text' => $result->Name . ' [' . $order['shipping_zone'] . '] (' . $result->LocalityId . ')'
                    ];
                }

            }

            return json_encode($data);

        }


        




    }


    public function awb_process($action) {

        $error = [];
        $error_state = 0;

        if(!(int)request('selected_courier')){
            $data['error_msg'] = 'Selecteaza un curier!';
        }

        if (request('shipping_type') == '') {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'shipping_type', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'shipping_type']);
        }

        if (strlen(request('shipping_name')) < 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'shipping_name', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'shipping_name']);
        }

        if (strlen(request('shipping_contact')) < 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'shipping_contact', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'shipping_contact']);
        }

        if (strlen(request('shipping_telephone')) < 10) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'shipping_telephone', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'shipping_telephone']);
        }

        if (strlen(request('shipping_address')) < 6) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'shipping_address', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'shipping_address']);
        }

        if (strlen(request('shipping_city')) < 1) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'shipping_city', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Acest camp este obligatoriu!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'shipping_city']);
        }

        if (!is_numeric(request('cash_on_delivery'))) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'cash_on_delivery', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Incorect!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'cash_on_delivery']);
        }

        if (!is_numeric(request('insured_value'))) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'insured_value', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Incorect!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'insured_value']);
        }

        if (!is_numeric(request('package_number')) || (int)request('package_number') < 1) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'package_number', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Trebuie sa fie un numar mai mare decat 0!"></i>']);
        } elseif((int)request('package_number') > 1 && (int)request('selected_courier') === 3) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'package_number', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Urgent Cargus nu accepta colete multiple!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'package_number']);
        }

        if (!is_numeric(request('package_weight')) || (int)request('package_weight') < 1) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'package_weight', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Trebuie sa fie un numar mai mare decat 0!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'package_weight']);
        }

        if (strlen(request('observations')) > 150) {
            $error_state = 1;
            array_push($error, ['code' => '1', 'element_id' => 'observations', 'msg' => '<i class="fa fa-exclamation tooltip-title" title="Prea multe caractere. Maxim 150!"></i>']);
        } else {
            array_push($error, ['code' => '0', 'element_id' => 'observations']);
        }


        if ($error_state) {
            $data['error'] = $error;
            $data['error_msg'] = 'Te rugam sa verifici daca ai completat corect toate campurile!';
            return ($data);
        } else {

            // setari curier
            $order = Order::select(
                'email',
                DB::raw('(SELECT name FROM counties WHERE county_id = orders.shipping_zone_id) as shipping_zone'),
                'shipping_zone_id',
            )
            ->where('order_id', request('order_id'))
            ->whereNull('deleted_at')
            ->first();

            $courier = Courier::select()
            ->join('vendors_couriers', 'vendors_couriers.courier_id', '=', 'couriers.courier_id', 'left outer')
            ->join('counties_couriers', 'counties_couriers.courier_id', '=', 'couriers.courier_id')
            ->where('vendors_couriers.courier_id', (int)request('selected_courier'))
            ->where('vendors_couriers.vendor_id', Auth::user()->id)
            ->where('counties_couriers.county_id', $order->shipping_zone_id)
            ->whereNull('vendors_couriers.deleted_at')
            ->first();

            // Sameday
            if((int)request('selected_courier') === 1){

                $awb_data = [
                    // adresa ridicare
                    'pickupPoint' => $courier->pickup_point, 
                    // persona de contact
                    'contactPerson' => $courier->contact_person, 
                    // 0 - colet, 1 - plic, 2 - colet mare
                    'packageType' => (int)request('package_type'), 
                    // numar trimiteri
                    'packageNumber' => (int)request('package_number'), 
                    // greutate
                    'packageWeight' => (int)request('package_weight'), 
                    // 7 - NextDay - 24H
                    'service' => $courier->service, 
                    // 1 - expeditor, 2 - destinatar
                    'awbPayment' => '1', 
                    // ramburs
                    'cashOnDelivery' => (float)request('cash_on_delivery'),
                    // 1 - client, 0 - tert 
                    'cashOnDeliveryReturns' => '1', 
                    // valoare asigurata
                    'insuredValue' => (float)request('insured_value'), 
                    // ridicare de la tert
                    'thirdPartyPickup' => '0', 
                    'awbRecipient[name]' => request('shipping_contact'),
                    'awbRecipient[phoneNumber]' => request('shipping_telephone'),
                    'awbRecipient[email]' => $order->email,
                    // 0 - persoana fizica, 1 - firma
                    'awbRecipient[personType]' => request('shipping_type'), 
                    // string oras
                    // 'awbRecipient[cityString]' => request('shipping_city'), 
                    // string judet
                    'awbRecipient[county]' => $courier->vendor_county_id, 
                    'awbRecipient[address]' => request('shipping_address'),
                    'awbRecipient[postalCode]' => request('shipping_postcode'),
                    'parcels[1][weight]' => (int)request('package_weight'),
                    'observation' => request('observations'),
                    'sellerId' => $courier->seller_id
                ];

                if((int)request('shipping_type') === 1){
                    $awb_data['awbRecipient[companyName]'] = request('shipping_name');
                }

                // Daca la localitate s-a modificat continutul campului inseamna ca acum avem id-ul localitatii de la curier
                if(is_numeric(request('shipping_city'))){
                    $awb_data['awbRecipient[city]'] = request('shipping_city');
                } else {
                    $awb_data['awbRecipient[cityString]'] = request('shipping_city');
                }

                // Generare AWB
                $awb_result = json_decode($this->Scripts->do_requests_sameday('api/awb', $awb_data, "POST"));

                // daca s-a creat AWB-ul salvam informatiile in baza de date
                if(isset($awb_result->awbNumber)){

                    // salvam AWB-ul
                    $data['awb_id'] = Awb::insertGetId(
                        [
                            'vendor_courier_id' => $courier->vendor_courier_id,
                            'order_id' => request('order_id'),
                            'awb_number' => $awb_result->awbNumber,
                            'shipping_contact' => request('shipping_contact'),
                            'shipping_telephone' => request('shipping_telephone'),
                            'cash_on_delivery' => request('cash_on_delivery'),
                            'awb_data' => json_encode($awb_data),
                            'awb_result' => json_encode($awb_result)
                        ]
                    );

                    // salvam statusul initial
                    $this->setInitialStatus($data['awb_id']);

                    $data['success_msg'] = 'AWB-ul a fost creat!';
                    $data['url'] = route('order.edit', request('order_id')) . '#order-awbs';
                } else {
                    // Save log
                    $log_data = [
                        'action' => 'create_awb',
                        'details' => json_encode([
                            'info' => 'Eroare generere AWB Sameday.',
                            'order_id' => request('order_id'),
                            'vendor_courier_id' => $courier->vendor_courier_id,
                            'awb_result' => $awb_result
                        ])
                    ];
                    $this->Scripts->save_log($log_data);

                    // returneaza un mesaj de eroare
                    $data['error_msg'] = 'A aparut o eroare la generarea AWB-ului!';
                }

            }

            // Fan Courier
            if((int)request('selected_courier') === 2){

                $parcels = 0;
                $envelopes = 0;

                if((int)request('package_type') === 0){
                    $parcels = (int)request('package_number');
                } elseif((int)request('package_type') === 1){
                    $envelopes = (int)request('package_number');
                }

                // Setul de date pentru awb
                $csv_data = [
                    'Tip serviciu' => '',
                    'Banca' => '',
                    'IBAN' => '',
                    'Nr. Plicuri' => $envelopes,
                    'Nr. Colete' => $parcels,
                    'Greutate' => request('package_weight'),
                    'Plata expeditie' => 'expeditor',
                    'Ramburs(bani)' => (float)request('cash_on_delivery'),
                    'Plata ramburs la' => 'expeditor',
                    'Valoare declarata' => (float)request('insured_value'),
                    'Persoana contact expeditor' => '',
                    'Observatii' => request('observations'),
                    'Continut' => '',
                    'Nume destinatar' => request('shipping_name'),
                    'Persoana contact' => request('shipping_contact'),
                    'Telefon' => request('shipping_telephone'),
                    'Fax' => '',
                    'Email' => '',
                    'Judet' => $order->shipping_zone,
                    'Localitatea' => request('shipping_city'),
                    'Strada' => str_replace(',', ' ', request('shipping_address')),
                    'Nr' => '',
                    'Cod postal' => request('shipping_postcode'),
                    'Bloc' => '',
                    'Scara' => '',
                    'Etaj' => '',
                    'Apartament' => '',
                    'Inaltime pachet' => '',
                    'Latime pachet' => '',
                    'Lungime pachet' => '',
                    'Restituire' => '',
                    'Centru Cost' => '',
                    'Optiuni' => '',
                    'Packing' => '',
                    'Date personale' => ''
                ];

                if((float)request('cash_on_delivery') > 0){
                    $csv_data['Tip serviciu'] = 'Cont Colector';
                } else {
                    $csv_data['Tip serviciu'] = 'Standard';
                }

                if(request('open_package')){
                    $csv_data['Optiuni'] .= 'A';
                }

                if(request('saturday_delivery')){
                    $csv_data['Optiuni'] .= 'S';
                }

                // Generam fisierul csv

                $csv_header = '';
                $csv_line = '';

                foreach ($csv_data as $k => $v) {
                    $csv_header .= $k . ',';
                    $csv_line .= $v . ',';
                }

                $csv_file = date('Ymdhis') . '_' . rand(10000,99999) . '.csv';

                file_put_contents('/home/andalio/marketplace.decoplus.ro/z_files/awb/fancourier/csv/' . $csv_file, $csv_header . "\n" . $csv_line, FILE_APPEND);

                $awb_data = [
                    'fisier' => curl_file_create('/home/andalio/marketplace.decoplus.ro/z_files/awb/fancourier/csv/' . $csv_file)
                ];
                
                // Generare AWB
                $awb_result = $this->Scripts->do_requests_fan_courier($awb_data, 'create_awb');

                // daca s-a creat AWB-ul salvam informatiile in baza de date
                if($awb_result[1] == 1){

                    // salvam AWB-ul
                    $data['awb_id'] = Awb::insertGetId(
                        [
                            'vendor_courier_id' => $courier->vendor_courier_id,
                            'order_id' => request('order_id'),
                            'awb_number' => $awb_result[2],
                            'shipping_contact' => request('shipping_contact'),
                            'shipping_telephone' => request('shipping_telephone'),
                            'cash_on_delivery' => request('cash_on_delivery'),
                            'awb_data' => json_encode($awb_data),
                            'awb_result' => json_encode($awb_result)
                        ]
                    );

                    // salvam statusul initial
                    $this->setInitialStatus($data['awb_id']);

                    $data['success_msg'] = 'AWB-ul a fost creat!';
                    $data['url'] = route('order.edit', request('order_id')) . '#order-awbs';
                    
                } else {

                    if($awb_result[2] === 'Probleme la localitate'){
                        // returneaza mesajul de eroare primit
                        $data['error'][] = [
                            'code' => '1',
                            'element_id' => 'shipping_city',
                            'msg' => '<i class="fa fa-exclamation tooltip-title" title="Incercati sa rescrieti denumirea localitatii!"></i>'
                        ];
                        $data['error_msg'] = 'A aparut o eroare la generarea AWB-ului!';
                    } else {
                        // Save log
                        $log_data = [
                            'action' => 'create_awb',
                            'details' => json_encode([
                                'info' => 'Eroare generere AWB Fan Courier.',
                                'order_id' => request('order_id'),
                                'vendor_courier_id' => $courier->vendor_courier_id,
                                'awb_result' => $awb_result
                            ])
                        ];
                        $this->Scripts->save_log($log_data);

                        // returneaza un mesaj de eroare
                        $data['error_msg'] = 'A aparut o eroare la generarea AWB-ului: ' . $awb_result[2];
                    }
                }

            }

            // Urgent Cargus
            if((int)request('selected_courier') === 3){

                $parcels = 0;
                $envelopes = 0;

                if((int)request('package_type') === 0){
                    $parcels = (int)request('package_number');
                } elseif((int)request('package_type') === 1){
                    $envelopes = (int)request('package_number');
                }

                $awb_data = [
                    'Sender' => [
                        'LocationId' => $courier->pickup_point
                    ],
                    'Recipient' => [
                        'Name' => request('shipping_name'),
                        'CountyId' => $courier->vendor_county_id,
                        'AddressText' => request('shipping_address'),
                        'ContactPerson' => request('shipping_contact'),
                        'PhoneNumber' => request('shipping_telephone'),
                        'Email' => $order->email
                    ],
                    'Parcels' => $parcels,
                    'Envelopes' => $envelopes,
                    'DeclaredValue' => (float)request('insured_value'),
                    'BankRepayment' => (float)request('cash_on_delivery'),
                    'OpenPackage' => request('open_package') ? true : false,
                    'ShipmentPayer' => 1,
                    'ServiceId' => 34,
                    'SaturdayDelivery' => request('saturday_delivery') ? true : false,
                    'Observations' => request('observations')
                ];


                // Daca avem colete
                if($parcels){
                    // Setam greutatea totala
                     $awb_data['TotalWeight'] = (int)request('package_weight');

                    // Pentru fiecare colet trebuie sa completam ParcelCodes
                    for ($i = 1; $i <= $parcels; $i++) {
                        $awb_data['ParcelCodes'][] = [
                            'Code'=> 0,
                            'Type' => 1,
                            'Weight' => round((int)request('package_weight') / $parcels),
                            'Length' => 1,
                            'Width' => 1,
                            'Height' => 1
                        ];
                    }
                } else {
                    // Daca avem doar plicuri
                    $awb_data['TotalWeight'] = 1;

                    // Pentru fiecare plic trebuie sa completam ParcelCodes
                    for ($i = 1; $i <= $envelopes; $i++) {
                        $awb_data['ParcelCodes'][] = [
                            'Code'=> 0,
                            'Type' => 0,
                            'Weight' => 1,
                            'Length' => 20,
                            'Width' => 30,
                            'Height' => 1
                        ];
                    }
                }

                // Daca la localitate s-a modificat continutul campului inseamna ca acum avem id-ul localitatii de la curier
                if(is_numeric(request('shipping_city'))){
                    $awb_data['Recipient']['LocalityId'] = request('shipping_city');
                } else {
                    $awb_data['Recipient']['LocalityName'] = request('shipping_city');
                }

                // La fel si pentru cod postal, ar trebui sa-l avem, dar daca nu este de 6 caractere punem implicit 000000
                if(strlen(request('shipping_postcode')) === 6){
                    $awb_data['Recipient']['CodPostal'] = request('shipping_postcode');
                } else {
                    $awb_data['Recipient']['CodPostal'] = "000000";
                }


                // Generare AWB
                $awb_result = $this->Scripts->do_requests_urgent_cargus('Awbs', $awb_data, 'POST');

                if($awb_result['status'] == '200'){

                    // salvam AWB-ul
                    $data['awb_id'] = Awb::insertGetId(
                        [
                            'vendor_courier_id' => $courier->vendor_courier_id,
                            'order_id' => request('order_id'),
                            'awb_number' => trim($awb_result['message'], '"'),
                            'shipping_contact' => request('shipping_contact'),
                            'shipping_telephone' => request('shipping_telephone'),
                            'cash_on_delivery' => request('cash_on_delivery'),
                            'awb_data' => json_encode($awb_data),
                            'awb_result' => json_encode($awb_result)
                        ]
                    );

                    // salvam statusul initial
                    $this->setInitialStatus($data['awb_id']);

                    $data['success_msg'] = 'AWB-ul a fost creat!';
                    $data['url'] = route('order.edit', request('order_id')) . '#order-awbs';
                } elseif($awb_result['status'] == '409'){ 
                    // returneaza mesajul de eroare primit
                    if(json_decode($awb_result['message'])->Error === "The recipient's locality could not be matched!"){
                        $data['error'][] = [
                            'code' => '1',
                            'element_id' => 'shipping_city',
                            'msg' => '<i class="fa fa-exclamation tooltip-title" title="Incercati sa rescrieti denumirea localitatii!"></i>'
                        ];
                        $data['error_msg'] = 'A aparut o eroare la generarea AWB-ului!';
                    } else {
                        // Save log
                        $log_data = [
                            'action' => 'create_awb',
                            'details' => json_encode([
                                'info' => 'Eroare generere AWB Sameday.',
                                'order_id' => request('order_id'),
                                'vendor_courier_id' => $courier->vendor_courier_id,
                                'awb_result' => $awb_result
                            ])
                        ];
                        $this->Scripts->save_log($log_data);
                    
                        $data['error_msg'] = json_decode($awb_result['message'])->Error;
                    }
                } elseif($awb_result['status'] == '500'){ 
                    // returneaza un mesaj de eroare
                    $data['error_msg'] = 'A aparut o eroare la generarea AWB-ului! Verifica setarile curierului!';
                } else {
                    // Save log
                    $log_data = [
                        'action' => 'create_awb',
                        'details' => json_encode([
                            'info' => 'Eroare generere AWB Sameday.',
                            'order_id' => request('order_id'),
                            'vendor_courier_id' => $courier->vendor_courier_id,
                            'awb_result' => $awb_result
                        ])
                    ];
                    $this->Scripts->save_log($log_data);

                    // returneaza un mesaj de eroare
                    $data['error_msg'] = 'A aparut o eroare la generarea AWB-ului!';
                }

            }




            return $data;

        }
    }

    public function setInitialStatus($awb_id){
        // Inseram statusul initial
        $history_id = History_awb::insertGetId(
            [
                'awb_id' => $awb_id,
                'awb_status_id' => 1
            ]
        );

        // Salvam id-ul statusului din istoric in 'awbs'
        Awb::where('awb_id', $awb_id)->update(['history_id' => $history_id]);
    }

}
