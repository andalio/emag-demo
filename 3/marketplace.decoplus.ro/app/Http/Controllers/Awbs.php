<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\Awb;
use App\Models\History_awb;
use DB;

use Auth;

class Awbs extends Controller {

	protected $Scripts;

	public function __construct(Scripts $Scripts) {
		$this->middleware('auth');
		$this->Scripts = $Scripts;
	}



	public function update_statuses(){

        $run_start = microtime(true);

		// Actualizam statusul AWB-urilor
		// Doar daca e diferit de cel existent
		// Pentru AWB-urile care n-au fost finalizate/livrate
		// si al caror status n-a mai fost actualizat in ultimele 30 de zile.

        $awbs = AWB::select(
            'awbs.awb_id',
            'awbs.awb_number',
            'awbs_statuses.awb_status_id',
            'awbs_statuses.name AS awb_status_name',
            'vendors_couriers.courier_id AS courier_id'
        )
        ->join('history_awbs', 'history_awbs.history_id', 'awbs.history_id')
        ->join('awbs_statuses', 'awbs_statuses.awb_status_id', 'history_awbs.awb_status_id')
        ->join('vendors_couriers', 'vendors_couriers.vendor_courier_id', 'awbs.vendor_courier_id')
        // nu le luam pe cele cu status Finalizat
        ->where('history_awbs.awb_status_id', '!=', 5)
        // nu le luam pe cele cu ultima actualizare de status mai veche de 30 de zile
        ->where('history_awbs.created_at', '>=', date('Y-m-d H:i:s', strtotime('-30 days')))
        ->whereNull('awbs.deleted_at')
        ->orderBy('awbs.awb_id', 'DESC')
        ->get();


        // 1   AWB creat
        // 2   AWB Anulat
        // 3   Predat curierului
        // 4   In drum spre client
        // 5   Livrat
        // 6   Refuzat de client
        // 7   Returnat expeditorului
        // 8   Deteriorat/pierdut
        // 9   Alt status

        $count = 0;

        foreach($awbs as $awb){

            $new_awb_status_id = 0;
            $courier_awb_status_id = '';
            $courier_awb_status_name = '';

        	// Sameday
            if($awb['courier_id'] === 1){

                // Preluare informatii status
                $status_result = json_decode($this->Scripts->do_requests_sameday('api/client/awb/' . $awb['awb_number'] . '/status?sellerId=33', [], "GET"))->expeditionStatus;

                if($status_result){

                    // AWB creat
                    if(in_array($status_result->statusId, [1,30])){
                        $new_awb_status_id = 1;
                    }

                    // AWB Anulat
                    if(in_array($status_result->statusId, [15,111])){
                        $new_awb_status_id = 2;
                    }

                    // Predat curierului
                    if(in_array($status_result->statusId, [15,111])){
                        $new_awb_status_id = 3;
                    }

                    // In drum spre client
                    if(in_array($status_result->statusId, [33])){
                        $new_awb_status_id = 4;
                    }

                    // Livrat
                    if(in_array($status_result->statusId, [9])){
                        $new_awb_status_id = 5;
                    }

                    // Refuzat de client
                    if(in_array($status_result->statusId, [11])){
                        $new_awb_status_id = 6;
                    }

                    // Returnat expeditorului
                    if(in_array($status_result->statusId, [16])){
                        $new_awb_status_id = 7;
                    }

                    // Deteriorat/pierdut
                    if(in_array($status_result->statusId, [21,22])){
                        $new_awb_status_id = 8;
                    }

                    // Alt status
                    if(!$new_awb_status_id){
                        $new_awb_status_id = 9;
                    }

                    $courier_awb_status_id = $status_result->statusId;
                    $courier_awb_status_name = $status_result->status . ' | ' . $status_result->statusLabel . ' | ' . $status_result->statusState;

                }

            }


            // Fan Courier
            if($awb['courier_id'] === 2){

                $post_data = [
                    'AWB' => $awb['awb_number'],
                    'display_mode' => 1
                ];

                // Preluare informatii status
                $status_result = $this->Scripts->do_requests_fan_courier($post_data, 'get_status');

                if($status_result){

                    // AWB creat
                    if(in_array($status_result[0], [0])){
                        $new_awb_status_id = 1;
                    }

                    // AWB Anulat
                    if(in_array($status_result[0], [38])){
                        $new_awb_status_id = 2;
                    }

                    // Predat curierului
                    if(in_array($status_result[0], [])){
                        $new_awb_status_id = 3;
                    }

                    // In drum spre client
                    if(in_array($status_result[0], [1,3,4,5,8,9,10,11,12,13,19,20,21,25,27,28,35,46])){
                        $new_awb_status_id = 4;
                    }

                    // Livrat
                    if(in_array($status_result[0], [2])){
                        $new_awb_status_id = 5;
                    }

                    // Refuzat de client
                    if(in_array($status_result[0], [6,7,15,22,24,50,30])){
                        $new_awb_status_id = 6;
                    }

                    // Returnat expeditorului
                    if(in_array($status_result[0], [33,42,43])){
                        $new_awb_status_id = 7;
                    }

                    // Deteriorat/pierdut
                    if(in_array($status_result[0], [37])){
                        $new_awb_status_id = 8;
                    }
                    
                    // Alt status
                    if(!$new_awb_status_id){
                        $new_awb_status_id = 9;
                    }

                    $courier_awb_status_id = $status_result[0];
                    $courier_awb_status_name = $status_result[1];

                }

            }


            // Urgent Cargus
            if($awb['courier_id'] === 3){

                // Preluare informatii status
                $status_result = $this->Scripts->do_requests_urgent_cargus('AwbTrace?barCode=[804115458,804346669]', [], 'GET');

                print_r($status_result);

            }





            // Daca noul status este diferit de cel vechi
            if($new_awb_status_id != $awb['awb_status_id']){

                // facem insert in history_awbs
                $history_id = History_awb::insertGetId(
                    [
                        'awb_id' => $awb['awb_id'],
                        'awb_status_id' => $new_awb_status_id,
                        'courier_awb_status_id' => $courier_awb_status_id,
                        'courier_awb_status_name' => $courier_awb_status_name
                    ]
                );

                // salvam id-ul statusului din istoric in 'awbs'
                Awb::where('awb_id', $awb['awb_id'])->update(['history_id' => $history_id]);

                echo 'AWB number: ' . $awb['awb_number'] . ' -> ' . $courier_awb_status_name . '<br>';
            }


            $count++;

        }

        $execution_time = round(microtime(true) - $run_start, 2);

        echo '<br><br>';

        echo 'AWB-uri procesate: ' . $count;

        echo '<br><br>';

        echo 'Timp executie: ' . $execution_time . ' secunde';
    }
}