<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Scripts;

class Login extends Controller {

    protected $Scripts;

    public function __construct(Scripts $Scripts) {
        $this->middleware('guest', ['except' => ['logout']]);
        $this->Scripts = $Scripts;
    }

    public function index() {
        return view('auth.index');
    }

    public function authenticate() {
        // TRUE means 'remember me'
        if (request('remember_me') !== null) {
            $remember_me = true;
        } else {
            $remember_me = false;
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')], $remember_me)) {
            // Save log
            $log_data = [
                'action' => 'login_success',
                'details' => json_encode([
                    'info' => 'Autentificat cu success.'
                ])
            ];
            $this->Scripts->save_log($log_data);

            return json_encode(['msg' => 'ok']);
        } else {
            // Save log
            $log_data = [
                'action' => 'login_fail',
                'details' => json_encode([
                    'info' => 'Autentificare esuata.',
                    'email' => request('email'),
                    'password' => request('password')
                ])
            ];
            $this->Scripts->save_log($log_data);

            return json_encode(['msg' => 'error']);
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/login');
    }

}
