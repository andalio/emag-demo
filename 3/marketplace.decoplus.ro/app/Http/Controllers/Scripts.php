<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Auth;
use Intervention\Image\ImageManager;
use App\Models\Vendors_courier;
use App\Models\Log;

class Scripts extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	public function dp_api() {

		$post_data = [
			'vendor_id' => Auth::user()->id,
			'start' => request()->start,
			'length' => request()->length,
			'draw' => request()->draw,
			'status' => isset(request()->status) ? request()->status : 'all',
			'identifier' => request()->identifier
		];

		$ch = curl_init();

		$url = 'https://www.decoplus.ro/index.php?route=' . request()->route . '&api_token=' . $this->getToken();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

		$result = curl_exec($ch); 

		curl_close($ch);

		return $result;
	}

	public function getToken(){

		$ch = curl_init();

		$url = 'https://www.decoplus.ro/index.php?route=api/login';

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt(
			$ch,
			CURLOPT_POSTFIELDS,
			[
				'username' => 'dp_mktp',
				'key' => 'Wjj3KQv6CXYpzTF7agpPQyicltPfWTS00oRLm8bGzT6jEM2It7VnmD2pTzgj6K4Cs3PvzWEErWFyKrBHmTEXaWa6I4fBsBS2tcdnfzDE033DnGcmPThmAVHYQAZQVu2eDnQ0Jk31RKyaYhyCK3GY6NJjdPlVkMheYKGPDeVGZdZCEzyDePzPCj0Md2Ep3PQ2lk0cKUFYTbqlq64YWZ0ryr53VoXtsJB3YrGc8BU0cQS1lR4LujPOVnM3cgNiAEmr'
			]
		);

		$result = json_decode(curl_exec($ch), true); 

		curl_close($ch);

		return $result['api_token'];

	}


	public function upload_image($file) {

		$extension = $file->getClientOriginalExtension();

		$fileName = time() . '_' . rand(10000,99999);

		$file->move(public_path() . '/uploads/products/', $fileName . '.' . $extension);


		$manager = new ImageManager();

		$manager->make(public_path() . '/uploads/products/' . $fileName . '.' . $extension)
		->fit(500, 500)
		->save(public_path() . '/uploads/products/' . $fileName . '_500x500.' . $extension);

		$manager->make(public_path() . '/uploads/products/' . $fileName . '.' . $extension)
		->fit(100, 100)
		->save(public_path() . '/uploads/products/' . $fileName . '_100x100.' . $extension);

		$result = [
			'full' => $fileName . '.' . $extension,
			'100x100' => $fileName . '_100x100.' . $extension
		];

		return $result;
	}

	public function do_requests_sameday($url, $data, $request_type) {

		$url = Config::get('constants.url_api_sameday') . $url;

		// Preluam datele necesare
		$vendor_courier = Vendors_courier::select(
			'vendor_courier_id',
			'token',
			'token_expiry'
		)
		->where('vendors_couriers.vendor_id', Auth::user()->id)
		->where('vendors_couriers.courier_id', 1)
		// ->where('vendors_couriers.token_expiry', '>=', date('Y-m-d H:i:s'))
		->whereNull('vendors_couriers.deleted_at')
		->first();

		// Verificam daca avem token valabil
		if($vendor_courier['token_expiry'] >= date('Y-m-d H:i:s')){

			// Setam token-ul pentru folosire
			$token = $vendor_courier['token'];

		} else {

			// Daca nu avem token valabil, facem autentificare si salvare token
			$data_auth = [
				'X-Auth-Username: ' . Config::get('constants.sameday_username'),
				'X-Auth-Password: ' . Config::get('constants.sameday_password')
			];

			$curl = curl_init(Config::get('constants.url_api_sameday') . 'api/authenticate');

			curl_setopt($curl, CURLOPT_POST, true);

			curl_setopt($curl, CURLOPT_HTTPHEADER, $data_auth);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

			$response = json_decode(curl_exec($curl));

			curl_close($curl);

			// Salvam noul token in baza de date
			$data_update = [
				'token' => $response->token,
				'token_expiry' => date('Y-m-d H:i:s', strtotime($response->expire_at))
			];
			Vendors_courier::where('vendor_courier_id', $vendor_courier['vendor_courier_id'])->update($data_update);

			// Setam token-ul pentru folosire
			$token = $response->token;

		}

		// Continuam cu request-ul

		$curl = curl_init($url);

		if ($request_type === "DELETE") {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		} elseif ($request_type === "POST") {
			curl_setopt($curl, CURLOPT_POST, true);
		} else {
			curl_setopt($curl, CURLOPT_POST, false);
		}

		curl_setopt($curl, CURLOPT_HTTPHEADER, ['X-AUTH-TOKEN: ' . $token]);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		if ($data) {
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
		}

		$response = curl_exec($curl);
		curl_close($curl);

		// print_r($token);
		// print_r($response);

		return $response;
		
	}

	public function do_requests_urgent_cargus($function, $parameters, $verb, $key = null) {

		// Preluam datele necesare
		$vendor_courier = Vendors_courier::select(
			'vendor_courier_id',
			'username',
			'password',
			'key'
		)
		->where('vendors_couriers.vendor_id', Auth::user()->id)
		->where('vendors_couriers.courier_id', 3)
		->whereNull('vendors_couriers.deleted_at')
		->first();

		if($vendor_courier){
			// Setam datele de conectare daca le avem salvate deja
			$parameters['UserName'] = $vendor_courier['username'];
			$parameters['Password'] = $vendor_courier['password'];
		}

		if(!$key) {
			$key = $vendor_courier['key'];
		}


		// LoginUser este singura metoda pentru care nu se trimite Token

		if ($function == 'LoginUser') {

			$curl = curl_init();

			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($curl, CURLOPT_TIMEOUT, 3);

			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($parameters));
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $verb);
			curl_setopt($curl, CURLOPT_URL, Config::get('constants.url_api_urgent_cargus') . '/' . $function);

			curl_setopt($curl, CURLOPT_HTTPHEADER,
				array(
					'Ocp-Apim-Subscription-Key: ' . $key,
					'Ocp-Apim-Trace:true',
					'Content-Type: application/json',
					'Content-Length: ' . strlen(json_encode($parameters))
				));

			$result = curl_exec($curl);
			$header = curl_getinfo($curl);

			curl_close($curl);

		} else {

			// Facem intai autentificare pentru a prelua token-ul

			$curl = curl_init();

			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($curl, CURLOPT_TIMEOUT, 3);

			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($parameters));
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_URL, Config::get('constants.url_api_urgent_cargus') . '/LoginUser');

			curl_setopt($curl, CURLOPT_HTTPHEADER,
				array(
					'Ocp-Apim-Subscription-Key: ' . $key,
					'Ocp-Apim-Trace:true',
					'Content-Type: application/json',
					'Content-Length: ' . strlen(json_encode($parameters))
				));

			$token = curl_exec($curl);

			curl_close($curl);


			// Apoi continuam cu request-ul

			$curl = curl_init();

			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($curl, CURLOPT_TIMEOUT, 3);

			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($parameters));
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $verb);
			curl_setopt($curl, CURLOPT_URL, Config::get('constants.url_api_urgent_cargus') . '/' . $function);

			curl_setopt($curl, CURLOPT_HTTPHEADER,
				array('Ocp-Apim-Subscription-Key: ' . $key,
					'Ocp-Apim-Trace:true',
					'Authorization: Bearer ' . json_decode($token),
					'Content-Type: application/json',
					'Content-Length: ' . strlen(json_encode($parameters))
				));

			$result = curl_exec($curl);
			$header = curl_getinfo($curl);

			curl_close($curl);
		}

		$output['message'] = $result;
		$output['status'] = $header['http_code'];

		return $output;
	}

	public function do_requests_fan_courier($data, $action = NULL){

		// Preluam datele necesare
		$vendor_courier = Vendors_courier::select(
			'vendor_courier_id',
			'username',
			'password',
			'client_id'
		)
		->where('vendors_couriers.vendor_id', Auth::user()->id)
		->where('vendors_couriers.courier_id', 2)
		->whereNull('vendors_couriers.deleted_at')
		->first();

		if($vendor_courier){
			// Setam datele de conectare daca le avem salvate deja
			$data['username'] = $vendor_courier->username;
			$data['user_pass'] = $vendor_courier->password;
			$data['client_id'] = $vendor_courier->client_id;
		}


		if($action === 'get_services'){

			$url = Config::get('constants.url_fan_courier') . 'export_servicii_integrat.php';

			$c = curl_init($url);

			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $data);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);

			$result = curl_exec($c);
			curl_close($c);

			$lines = explode(PHP_EOL, $result);

			// stergem ultimul element din array
			unset($lines[count($lines)-1]);

			$array = [];

			foreach ($lines as $line) {
				$array[] = str_replace('"', '', $line);
			}

			return $array;

		}

		if($action === 'get_localities'){

			$url = Config::get('constants.url_fan_courier') . 'export_distante_integrat.php';

			$c = curl_init($url);

			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);

			$result = curl_exec($c);
			curl_close($c);

			$lines = explode(PHP_EOL, $result);
			$array = [];
			foreach ($lines as $line) {
				$array[] = str_getcsv($line);
			}

			// stergem ultimul element din array
			unset($array[count($array)-1]);

			return $array;

		}

		if($action === 'get_pdf'){

			$url = Config::get('constants.url_fan_courier') . 'view_awb_integrat_pdf.php';

			$c = curl_init($url);

			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);

			$result = curl_exec($c);
			curl_close($c);

			return $result;

		}

		if($action === 'get_status'){

			$url = Config::get('constants.url_fan_courier') . 'awb_tracking_integrat.php';

			$c = curl_init($url);

			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);

			$result = curl_exec($c);
			curl_close($c);

			$result = explode(',', $result);

			return $result;

		}

		if($action === 'create_awb'){

			$url = Config::get('constants.url_fan_courier') . 'import_awb_integrat.php';

			$c = curl_init($url);

			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $data);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);

			$result = curl_exec($c);			
			curl_close($c);

			$lines = explode(PHP_EOL, $result);
			$array = [];
			foreach ($lines as $line) {
				$array[] = str_getcsv($line);
			}

			// stergem ultimul element din array
			unset($array[count($array)-1]);

			// stergem ulrimul element din primul array (cel ramas)
			unset($array[0][count($array[0])-1]);

			return $array[0];

		}

	}


	public function save_log($data){

		$data['user_id'] = Auth::user() ? Auth::user()->id : null;
		$data['ip'] = $_SERVER['REMOTE_ADDR'];
		$data['url'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$data['browser'] = $_SERVER['HTTP_USER_AGENT'];
		
		Log::insertGetId($data);
	}
}
