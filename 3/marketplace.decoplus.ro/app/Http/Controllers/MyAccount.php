<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\Courier;
use App\Models\Vendors_courier;
use App\Models\Users_setting;
use DB;

use Auth;

class MyAccount extends Controller {

    protected $Scripts;

    public function __construct(Scripts $Scripts) {
        $this->middleware('auth');
        $this->Scripts = $Scripts;
    }

    public function profile() {

        // conturi de curier active
        $data['user_setting'] = Users_setting::select(
            'users_settings.*',
            'counties.name AS company_county_name',
            DB::raw('TIMESTAMPDIFF(MONTH, users_settings.created_at, CURDATE()) AS account_lifetime'),
            DB::raw('CONCAT(users.first_name," ",users.last_name) as contact_name'),
            'users.phone AS contact_phone'
        )
        ->where('users_settings.user_id', Auth::user()->id)
        ->join('counties', 'counties.county_id', 'users_settings.company_county_id')
        ->join('users', 'users.id', 'users_settings.user_id')
        ->first();


        return view('myaccount.profile', $data);
    }

    public function profile_save() {


    }

    public function couriers() {
        // conturi de curier
        $data['couriers'] = Courier::select()
        ->whereNotIn('couriers.courier_id', function($query){
           $query->select('courier_id')
           ->from('vendors_couriers')
           ->where('vendors_couriers.vendor_id', Auth::user()->id);
       })
        ->get();

        // conturi de curier active
        $data['vendor_couriers'] = Vendors_courier::select(
            'vendors_couriers.vendor_courier_id AS vendor_courier_id',
            'vendors_couriers.courier_id AS courier_id',
            'couriers.name AS courier_name',
            'vendors_couriers.default AS default'
        )
        ->join('couriers', 'couriers.courier_id', 'vendors_couriers.courier_id')
        ->where('vendors_couriers.vendor_id', Auth::user()->id)
        ->whereNull('vendors_couriers.deleted_at')
        ->get();

        return view('myaccount.couriers', $data);
    }

    public function add_courier($courier_id) {

        $data['courier'] = Courier::select()
        ->where('courier_id', $courier_id)
        ->first();


        return view('myaccount.add_courier', $data);
    }

    public function edit_courier($vendor_courier_id) {

        $data['courier'] = Vendors_courier::select()
        ->join('couriers', 'couriers.courier_id', 'vendors_couriers.courier_id')
        ->where('vendors_couriers.vendor_courier_id', $vendor_courier_id)
        ->whereNull('vendors_couriers.deleted_at')
        ->first();

        // Sameday
        if($data['courier']['courier_id'] === 1){

            // preluare puncte de ridicare
            $data['pickup_points'] = json_decode($this->Scripts->do_requests_sameday('api/client/pickup-points?sellerId=' . $data['courier']['seller_id'], [], "GET"))->data;

            // servicii
            $data['services'] = json_decode($this->Scripts->do_requests_sameday('api/client/services?sellerId=' . $data['courier']['seller_id'], [], "GET"))->data;

            // print_r($data['services']);
            // exit;

        }

        // Fan Courier
        if($data['courier']['courier_id'] === 2){

            // servicii
            $services = $this->Scripts->do_requests_fan_courier([], 'get_services');

            if($services){
                foreach ($services as $k => $service) {
                    $data['services'][] = (object) [
                        'id' => $k,
                        'name' => $service,
                    ];
                }
            }

        }


        // Urgent Cargus
        if($data['courier']['courier_id'] === 3){


            // preluare puncte de ridicare
            $data['pickup_points'] = json_decode($this->Scripts->do_requests_urgent_cargus('PickupLocations/GetForClient', [], 'GET')['message']);


        }

        




        return view('myaccount.edit_courier', $data);
    }

    public function courier_process($action, $courier_id) {
        $error = [];
        $error_state = 0;

        if($action === 'add'){

            // Sameday
            if((int)$courier_id === 1){

                if (!is_numeric(request('seller_id'))) {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'seller_id', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'seller_id']);
                }

            }

            // Fan Courier
            if((int)$courier_id === 2){
                if (strlen(request('username')) < 3) {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'username', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'username']);
                }

                if (strlen(request('password')) < 3) {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'password', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'password']);
                }

                if (strlen(request('client_id')) < 3) {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'client_id', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'client_id']);
                }
            }

            // Urgent Cargus
            if((int)$courier_id === 3){
                if (strlen(request('username')) < 3) {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'username', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'username']);
                }

                if (strlen(request('password')) < 3) {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'password', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'password']);
                }

                if (strlen(request('key')) < 10) {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'key', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'key']);
                }
            }
        }

        if($action === 'edit'){

            // Sameday
            if((int)$courier_id === 1){

                if (request('pickup_point_contact_person') == '') {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'pickup_point_contact_person', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'pickup_point_contact_person']);
                }

                if (request('service') == '') {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'service', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'service']);
                }
            }

            // Urgent Cargus
            if((int)$courier_id === 3){

                if (request('pickup_point') == '') {
                    $error_state = 1;
                    array_push($error, ['code' => '1', 'element_id' => 'pickup_point', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                } else {
                    array_push($error, ['code' => '0', 'element_id' => 'pickup_point']);
                }

                // if (request('service') == '') {
                //     $error_state = 1;
                //     array_push($error, ['code' => '1', 'element_id' => 'service', 'msg' => 'Acest camp trebuie este obligatoriu!']);
                // } else {
                //     array_push($error, ['code' => '0', 'element_id' => 'service']);
                // }
            }

        }

        if ($error_state) {
            $data['error'] = $error;
            return ($data);
        } else {

            if($action === 'add'){

                // Sameday
                if((int)$courier_id === 1){

                    // verificam daca exista deja
                    $courier_exists = Vendors_courier::select()
                    ->where('vendors_couriers.courier_id', 1) // courier_id Sameday
                    ->where('vendors_couriers.vendor_id', Auth::user()->id)
                    ->whereNull('vendors_couriers.deleted_at')
                    ->first();

                    if($courier_exists){
                        $data['error_msg'] = 'Curierul pe care incerci sa-l adaugi exista deja!';
                    } else {

                        // preluare puncte de ridicare
                        $pickup_points = json_decode($this->Scripts->do_requests_sameday('api/client/pickup-points?sellerId=' . request('seller_id'), [], "GET"));

                        // verificam preluarea punctelor de ridicare ca sa confirmam ca SellerID introdus este bun
                        if(isset($pickup_points->data)){

                            // salvam datele in baza de date
                            $data['vendor_courier_id'] = Vendors_courier::insertGetId(
                                [
                                    'vendor_id' => Auth::user()->id,
                                    'courier_id' => $courier_id,
                                    'seller_id' => request('seller_id')
                                ]
                            );

                            $data['success_msg'] = 'Conectare reusita!';
                            $data['url'] = route('myaccount.edit_courier', $data['vendor_courier_id']); 

                        } elseif(isset($pickup_points->error)){
                            // Save log
                            $log_data = [
                                'action' => 'add_courier',
                                'details' => json_encode([
                                    'info' => 'Eroare la salvarea curierului - Sameday.',
                                    'error' => $pickup_points->error
                                ])
                            ];
                            $this->Scripts->save_log($log_data);

                            // daca datele nu sunt bune
                            $data['error_msg'] = 'Datele de autentificare nu sunt corecte!';
                        } else {
                            // Save log
                            $log_data = [
                                'action' => 'add_courier',
                                'details' => json_encode([
                                    'info' => 'Eroare necunoscuta la salvarea curierului - Sameday.'
                                ])
                            ];
                            $this->Scripts->save_log($log_data);

                            // daca a aparut o alta eroare
                            $data['error_msg'] = 'A aparut o eroare neidentificata, te rugam sa ne contactezi!';
                        }

                    }

                    return $data;
                }


                // Fan Courier
                if((int)$courier_id === 2){

                    // verificam daca exista deja
                    $courier_exists = Vendors_courier::select()
                    ->where('vendors_couriers.courier_id', 2) // courier_id Fan Courier
                    ->where('vendors_couriers.vendor_id', Auth::user()->id)
                    ->whereNull('vendors_couriers.deleted_at')
                    ->first();

                    if($courier_exists){
                        $data['error_msg'] = 'Curierul pe care incerci sa-l adaugi exista deja!';
                    } else {

                        $post_data = [
                            'username' => request('username'),
                            'user_pass' => request('password'),
                            'client_id' => request('client_id')
                        ];

                        // preluare servicii
                        $services = $this->Scripts->do_requests_fan_courier($post_data, 'get_services');

                        // verificam preluarea serviciilor ca sa confirmam ca datele sunt bune
                        if(count($services) > 0){

                            // salvam datele in baza de date
                            $data['vendor_courier_id'] = Vendors_courier::insertGetId(
                                [
                                    'vendor_id' => Auth::user()->id,
                                    'courier_id' => $courier_id,
                                    'username' => request('username'),
                                    'password' => request('password'),
                                    'client_id' => request('client_id')
                                ]
                            );

                            $data['success_msg'] = 'Curierul a fost adaugat!';
                            $data['url'] = route('myaccount.edit_courier', $data['vendor_courier_id']);

                        } else {
                            // Save log
                            $log_data = [
                                'action' => 'add_courier',
                                'details' => json_encode([
                                    'info' => 'Eroare la salvarea curierului - probabil date incorecte - Fan Courier.'
                                ])
                            ];
                            $this->Scripts->save_log($log_data);

                            // daca datele nu sunt bune
                            $data['error_msg'] = 'Datele de autentificare nu sunt corecte!';
                        }


                    }

                    return $data;

                }


                // Urgent Cargus
                if((int)$courier_id === 3){

                    // verificam daca exista deja
                    $courier_exists = Vendors_courier::select()
                    ->where('vendors_couriers.courier_id', 3) // courier_id Urgent Cargus
                    ->where('vendors_couriers.vendor_id', Auth::user()->id)
                    ->whereNull('vendors_couriers.deleted_at')
                    ->first();

                    if($courier_exists){
                        $data['error_msg'] = 'Curierul pe care incerci sa-l adaugi exista deja!';
                    } else {

                        // date de autentificare
                        $login_data = [
                            'UserName' => request('username'),
                            'Password'=> request('password')
                        ];

                        // auth
                        $login = $this->Scripts->do_requests_urgent_cargus('LoginUser', $login_data, 'POST', request('key'));

                        // daca autentficarea s-a facut cu success
                        if ($login['status'] == "200") {

                            // salvam datele in baza de date
                            $data['vendor_courier_id'] = Vendors_courier::insertGetId(
                                [
                                    'vendor_id' => Auth::user()->id,
                                    'courier_id' => $courier_id,
                                    'username' => request('username'),
                                    'password' => request('password'),
                                    'key' => request('key')
                                ]
                            );

                            $data['success_msg'] = 'Conectare reusita!';

                            $data['url'] = route('myaccount.edit_courier', $data['vendor_courier_id']); 

                        } else {
                            // Save log
                            $log_data = [
                                'action' => 'add_courier',
                                'details' => json_encode([
                                    'info' => 'Eroare la salvarea curierului - probabil date incorecte - Urgent Cargus.'
                                ])
                            ];
                            $this->Scripts->save_log($log_data);

                            // daca datele nu sunt bune
                            $data['error_msg'] = 'Datele de autentificare nu sunt corecte!';
                        }

                    }

                    return $data;
                }


            }

            if($action === 'edit'){

                // Sameday
                if((int)$courier_id === 1){

                    $data_update = [];

                    $data_update['pickup_point'] =  explode('|', request('pickup_point_contact_person'))[0];
                    $data_update['contact_person'] =  explode('|', request('pickup_point_contact_person'))[1];
                    $data_update['service'] =  request('service');
                    $data_update['default'] = request('default') ? 1 : 0;
                    
                    // Actualizam in baza de date
                    $update_result = Vendors_courier::where('vendor_courier_id', request('vendor_courier_id'))->update($data_update);

                    if($update_result){

                        // Daca curierul curent s-a setat implicit, atunci ii setam pe ceilalti cu default 0
                        if(request('default') == 1){
                            Vendors_courier::where('vendor_courier_id', '!=', request('vendor_courier_id'))->update(['default' => 0]);
                        }

                        $data['success_msg'] = 'Modificarile au fost salvate!';
                    }

                    return $data;

                }

                // Fan Courier
                if((int)$courier_id === 2){

                    $data_update = [];
                    $data_update['service'] =  request('service');
                    $data_update['default'] = request('default') ? 1 : 0;

                    // Actualizam in baza de date
                    $update_result = Vendors_courier::where('vendor_courier_id', request('vendor_courier_id'))->update($data_update);

                    if($update_result){

                        // Daca curierul curent s-a setat implicit, atunci ii setam pe ceilalti cu default 0
                        if(request('default') == 1){
                            Vendors_courier::where('vendor_courier_id', '!=', request('vendor_courier_id'))->update(['default' => 0]);
                        }

                        $data['success_msg'] = 'Modificarile au fost salvate!';
                    }

                    return $data;

                }


                // Urgent Cargus
                if((int)$courier_id === 3){

                    $data_update = [];

                    $data_update['pickup_point'] =  request('pickup_point');
                    $data_update['default'] = request('default') ? 1 : 0;

                    // Actualizam in baza de date
                    $update_result = Vendors_courier::where('vendor_courier_id', request('vendor_courier_id'))->update($data_update);

                    if($update_result){

                        // Daca curierul curent s-a setat implicit, atunci ii setam pe ceilalti cu default 0
                        if(request('default') == 1){
                            Vendors_courier::where('vendor_courier_id', '!=', request('vendor_courier_id'))->update(['default' => 0]);
                        }

                        $data['success_msg'] = 'Modificarile au fost salvate!';
                    }

                    return $data;

                }



            }




        }


    }


}
