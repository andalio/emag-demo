@extends('main_base' . $view_mode)

@section('content')
<form action="{{ route('order.awb_process', 'add') }}" method="post" id="awb_form" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="modal-header">
		<h5 class="modal-title">Genereaza AWB</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Inchide">
			<span aria-hidden="true">×</span>
		</button>
	</div>
	<div class="modal-body">
		<section class="content container-fluid create_awb_content">
			<div class="alert alert-success hide" id="awb_alert_success">
				<i class="icon fa fa-check"></i>  <span class="message"></span>
			</div>
			<div class="alert alert-error hide" id="awb_alert_error">
				<i class="icon fa fa-check"></i>  <span class="message"></span>
			</div>
			<div class="box box-primary">
				<div class="box-body couriers_list">
					<div class="row">
						@foreach ($vendor_couriers as $vendor_courier)
						<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 courier">
							<img src="{{ url('assets/img/couriers/' . $vendor_courier->image) }}" title="{{ $vendor_courier->courier_name }}" class="logo @if(!$vendor_courier->vendor_courier_id) disabled @endif @if($vendor_courier->default) selected @endif" width="50">

							<input type="radio" name="selected_courier" value="{{ $vendor_courier->courier_id }}" class="hide" @if($vendor_courier->default) checked @endif>
						</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<div class="box box-primary">
						<div class="box-body">
							<h5>Informatii expeditor</h5>

							<div class="form-group">
								Pentru setari mergi in pagina <a href="{{ route('myaccount.couriers') }}" target="_blank">conturi de curier</a>.
							</div>

<!-- 						<div class="form-group">
							<label for="pickup_point">Punct de ridicare <span class="required">*</span></label>
							<span class="badge bg-red error-badge"></span>
							<select id="pickup_point" name="pickup_point" class="form-control select2" disabled>
								<option value="111" selected>trebuie preluat in functie de curierul selectat</option>
							</select>
						</div> -->
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
						<h5>Informatii destinatar</h5>

						<div class="form-group">
							<label for="shipping_type">Tip client <span class="required">*</span></label>
							<span class="badge bg-red error-badge"></span>
							<select id="shipping_type" name="shipping_type" class="form-control select2">
								<option value="0" @if(!$order->shipping_company) selected @endif>Persoana fizica</option>
								<option value="1" @if($order->shipping_company) selected @endif>Persoana juridica</option>
							</select>
						</div>
						<div class="form-group">
							<label for="shipping_name">Nume <span class="required">*</span></label>
							<span class="badge bg-red error-badge"></span>
							<input type="text" name="shipping_name" id="shipping_name" class="form-control" value="@if($order->shipping_company){{ $order->shipping_company}}@else{{ $order->shipping_name }}@endif">
						</div>
						<div class="form-group">
							<label for="shipping_contact">Persoana contact <span class="required">*</span></label>
							<span class="badge bg-red error-badge"></span>
							<input type="text" name="shipping_contact" id="shipping_contact" class="form-control" value="{{ $order->shipping_name }}">
						</div>
						<div class="form-group">
							<label for="shipping_telephone">Telefon <span class="required">*</span></label>
							<span class="badge bg-red error-badge"></span>
							<input type="text" name="shipping_telephone" id="shipping_telephone" class="form-control" value="{{ $order->shipping_telephone }}">
						</div>
						<div class="form-group">
							<label for="shipping_address">Adresa <span class="required">*</span></label>
							<span class="badge bg-red error-badge"></span>
							<input type="text" name="shipping_address" id="shipping_address" class="form-control" value="{{ $order->shipping_address }}">
						</div>
						<div class="form-group row">
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<label for="shipping_city">Localitate <span class="required">*</span> (<i class="fa fa-info tooltip-title" title="Atentie la scrierea denumirilor, pot exista diferente la curieri."></i>)</label>
								<span class="badge bg-red error-badge"></span>
								<select id="shipping_city" name="shipping_city" class="form-control select2">
									<option value="{{ $order->shipping_city }}">{{ $order->shipping_city }}</option>
								</select>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<label for="shipping_postcode">Cod postal <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="text" name="shipping_postcode" id="shipping_postcode" class="form-control" value="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
						<h5>Informatii comanda</h5>

						<div class="form-group row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label for="cash_on_delivery">Ramburs <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="text" name="cash_on_delivery" id="cash_on_delivery" class="form-control" value="@if($order->payment_code === 'cod'){{ $order->cash_on_delivery }}@else{{'0'}}@endif">
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label for="insured_value">Valoare asigurata</label>
								<span class="badge bg-red error-badge"></span>
								<input type="text" name="insured_value" id="insured_value" class="form-control" value="0">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
								<label for="package_type">Tip colet <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<select id="package_type" name="package_type" class="form-control select2">
									<option value="0" selected>Colet</option>
									<option value="1">Plic</option>
								</select>
							</div>

							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
								<label for="package_number">Numar colete <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="number" name="package_number" id="package_number" class="form-control" value="1">
							</div>

							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
								<label for="package_weight">Greutate <span class="required">*</span> (<i class="fa fa-info tooltip-title" title="Bazat pe suma greutatilor produselor din comanda!"></i>)</label>
								<span class="badge bg-red error-badge"></span>
								<input type="text" name="package_weight" id="package_weight" class="form-control" value="{{ $order->weight }}">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
								<input type="checkbox" name="open_package" id="open_package">
								<label for="open_package">Deschidere colet</label>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
								<input type="checkbox" name="saturday_delivery" id="saturday_delivery">
								<label for="saturday_delivery">Livrare Sambata</label>
							</div>
						</div>
						<div class="form-group">
							<label for="observations">Observatii</label>
							<span class="badge bg-red error-badge"></span>
							<textarea name="observations" id="observations" class="form-control"></textarea>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" id="order_id" name="order_id" value="{{ $order->order_id }}">
		</div>
	</section>
</div>
<div class="modal-footer justify-content-between">
	<button type="button" class="btn btn-link float-left" data-dismiss="modal"><i class="fa fa-remove"></i> Anuleaza</button>
	<button type="submit" class="btn btn-success" id="order_awb_save"><i class="fa fa-check"></i> Salveaza</button>
</div>
</form>
@stop

@section('title','AWB')