@extends('main_base' . $view_mode)

@section('content')

<div class="edit-order-page print-order-page">
	<h1>Comanda #{{ $order->order_id }} din <i class="fa fa-calendar"></i> {{ $order->date_added_day }}</h1>

	<section class="content container-fluid">

		<div class="alert alert-success hide" id="alert_success">
			<i class="icon fa fa-check"></i>  <span class="message"></span>
		</div>

		<div class="alert alert-error hide" id="alert_error">
			<i class="icon fa fa-check"></i>  <span class="message"></span>
		</div>

		<div class="box box-primary">
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<div class="box-header with-border">
						<b>Detalii comanda</b>
					</div>
					<div class="details-content">
						<div>Metoda de plata: {{ $order->payment_method }}</div>
						<div>Status comanda: 
							@foreach ($orders_statuses as $status)
							@if($status->order_status_id === $order->order_status_id) {{ $status->name }} @endif
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<div class="box-header with-border">
						<b>Date facturare</b>
					</div>
					<div class="details-content">
						@if($order->payment_company)
						{{ $order->payment_company }}, 
						@else
						{{ $order->payment_name }}, 
						@endif
						{{ $order->payment_telephone }}, 
						Adresa: {{ $order->payment_zone }}, {{ $order->payment_city }}, {{ $order->payment_address }}
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<div class="box-header with-border">
						<b>Date livrare</b>
					</div>
					<div class="details-content">
						@if($order->shipping_company)
						{{ $order->shipping_company }}, 
						@else
						{{ $order->shipping_name }}, 
						@endif
						{{ $order->shipping_telephone }}, 
						Adresa: {{ $order->shipping_zone }}, {{ $order->shipping_city }}, {{ $order->shipping_address }}
					</div>
				</div>
			</div>
		</div>

		<div class="box box-primary order-products" id="order-products">
			<table id="products-list">
				<tr class="header">
					<td>Produs</td>
					<td class="product-quantity">Cantitate</td>
					<td class="product-price">Pret</td>
					<td class="product-total">Total</td>
				</tr>
				@foreach ($order_products as $product)
				<tr class="item">
					<td><b>{{ $product->name }}</b><br>Cod produs: {{ $product->model }}</td>
					<td class="product-quantity">{{ $product->quantity }}</td>
					<td class="product-price">{{ $product->price }}</td>
					<td class="product-total">{{ $product->total }}</td>
				</tr>
				@endforeach
				<tr class="item">
					<td colspan="3">Cost livrare</td>
					<td class="order-total">{{ $order->shipping_cost }}</td>
				</tr>
				<tr class="total">
					<td colspan="3">Total</td>
					<td class="order-total">{{ $order->total }}</td>
				</tr>
			</table>
		</div>

		<div class="box box-primary order-awbs" id="order-awbs">
			<table id="awbs-list">
				<tr class="header">
					<td>Curier</td>
					<td>Numar AWB</td>
					<td>Data emitere</td>
					<td>Status AWB</td>
					<td>Data status</td>
				</tr>
				@foreach ($order_awbs as $awb)
				<tr class="awb">
					<td>{{ $awb->courier_name }}</td>
					<td>{{ $awb->awb_number }} <a class="btn btn-link download_awb"><i class="fa fa-download"></i></a></td>
					<td>
						<span><i class="fa fa-calendar"></i> {{ $awb->created_at_day }}</span>
						<span><i class="fa fa-clock-o"></i> {{ $awb->created_at_hour }}</span>
					</td>
					<td><span class="labelled_text {{ $awb->awb_status_class }}">{{ $awb->awb_status }}</span></td>
					<td>
						<span><i class="fa fa-calendar"></i> {{ $awb->last_status_day }}</span>
						<span><i class="fa fa-clock-o"></i> {{ $awb->last_status_hour }}</span>
					</td>
				</tr>
				@endforeach
			</table>
		</div>

	</section>

	<div class="modal fade" id="modal-create-awb" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"><div class="loading"></div></div>
		</div>
	</div>
</div>

@stop

@section('title','Detalii comanda - decoplus.ro Marketplace')