@extends('main')

@section('content')

<div class="content-wrapper edit-order-page">
	<section class="content-header">
		<h1>
			Comanda #{{ $order->order_id }} din <i class="fa fa-calendar"></i> {{ $order->date_added_day }}
		</h1>
		<button type="button" class="btn btn-sm btn-link cancel_changes"><i class="fa fa-remove"></i> Anuleaza</button>
		<button type="button" class="btn btn-sm btn-success save_changes"><i class="fa fa-check-circle"></i> Salveaza modificari</button>
		<button type="button" class="btn btn-sm btn-primary create_awb" data-toggle="modal" data-target="#modal-create-awb"><i class="fa fa-truck"></i> Genereaza AWB</button>
	</section>

	<section class="content container-fluid">

		<div class="alert alert-success hide" id="alert_success">
			<i class="icon fa fa-check"></i>  <span class="message"></span>
		</div>

		<div class="alert alert-error hide" id="alert_error">
			<i class="icon fa fa-check"></i>  <span class="message"></span>
		</div>

		<div class="box box-primary">
			<form action="{{ route('order.process', 'edit') }}" method="post" id="form" enctype="multipart/form-data" class="order_changes">
				<div class="row">
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="box-header with-border">
							<h3 class="box-title">Detalii comanda</h3>
						</div>
						<div class="details-content">
							<div>Metoda de plata: <b>{{ $order->payment_method }}</b></div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="box-header with-border">
							<h3 class="box-title">Date facturare</h3>
							<a class="btn btn-link edit_fields" attr-area="billing_fileds"><i class="fa fa-edit"></i> Modifica</a>
						</div>
						<div class="details-content billing_fileds">
							@if($order->payment_company)
							<div class="details-row">Persoana juridica: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->payment_company }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_payment_company" id="order_payment_company" value="{{ $order->payment_company }}">
									</span>
								</div>
							</div>
							@else
							<div class="details-row">Persoana fizica: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->payment_name }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_payment_firstname" id="order_payment_firstname" value="{{ $order->payment_firstname }}"></span> 
										<span><span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_payment_lastname" id="order_payment_lastname" value="{{ $order->payment_lastname }}">
									</span>
								</div>
							</div>
							@endif
							<div class="details-row">Telefon: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->payment_telephone }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_payment_telephone" id="order_payment_telephone" value="{{ $order->payment_telephone }}">
									</span>
									<span class="badge bg-red error-badge"></span>
								</div>
							</div>
							<div class="details-row">Adresa: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->payment_address }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_payment_address" id="order_payment_address" value="{{ $order->payment_address }}">
									</span>
								</div>
							</div>
							<div class="details-row">Judet: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->payment_zone }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<select id="order_payment_zone_id" name="order_payment_zone_id" class="hide not-select2">
											@foreach ($counties as $county)
											<option value="{{ $county->county_id }}" @if($county->county_id === $order->payment_zone_id) selected @endif>{{ $county->name }}</option>
											@endforeach
										</select>
									</span> | Localitate: 
									<span class="data"><b>{{ $order->payment_city }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_payment_city" id="order_payment_city" value="{{ $order->payment_city }}">
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="box-header with-border">
							<h3 class="box-title">Date livrare</h3>
							<a class="btn btn-link edit_fields" attr-area="shippping_fileds"><i class="fa fa-edit"></i> Modifica</a>
						</div>
						<div class="details-content shippping_fileds">
							@if($order->shipping_company)
							<div class="details-row">Persoana juridica: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->shipping_company }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_shipping_company" id="order_shipping_company" value="{{ $order->shipping_company }}">
									</span>
								</div>
							</div>
							@else
							<div class="details-row">Persoana fizica: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->shipping_name }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_shipping_firstname" id="order_shipping_firstname" value="{{ $order->shipping_firstname }}"></span> 
										<span><span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_shipping_lastname" id="order_shipping_lastname" value="{{ $order->shipping_lastname }}">
									</span>
								</div>
							</div>
							@endif
							<div class="details-row">Telefon: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->shipping_telephone }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_shipping_telephone" id="order_shipping_telephone" value="{{ $order->shipping_telephone }}">
									</span>
									<span class="badge bg-red error-badge"></span>
								</div>
							</div>
							<div class="details-row">Adresa: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->shipping_address }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_shipping_address" id="order_shipping_address" value="{{ $order->shipping_address }}">
									</span>
								</div>
							</div>
							<div class="details-row">Judet: 
								<div class="editable_field">
									<span class="data"><b>{{ $order->shipping_zone }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<select id="order_shipping_zone_id" name="order_shipping_zone_id" class="hide not-select2">
											@foreach ($counties as $county)
											<option value="{{ $county->county_id }}" @if($county->county_id === $order->shipping_zone_id) selected @endif>{{ $county->name }}</option>
											@endforeach
										</select>
									</span> | Localitate: 
									<span class="data"><b>{{ $order->shipping_city }}</b></span>
									<span>
										<span class="badge bg-red error-badge"></span>
										<input type="text" class="form-control field hide" name="order_shipping_city" id="order_shipping_city" value="{{ $order->shipping_city }}">
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-lg-10 col-md-6 col-sm-12 col-xs-12 order_status_id">	
						<select id="order_status_id" name="order_status_id">
							@foreach ($orders_statuses as $status)
							<option value="{{ $status->order_status_id }}" class="status_option {{ $status->class }}" @if($status->order_status_id === $order->order_status_id) selected @endif @if($status->user_selectable === 0) disabled @endif>{{ $status->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-1 col-md-6 col-sm-12 col-xs-12">	
						<a class="btn btn-link upload_invoice"><i class="fa fa-upload"></i> Incarca factura</a>
					</div>
					<div class="col-lg-1 col-md-6 col-sm-12 col-xs-12">	
						<a class="btn btn-link print_order" order_id="{{ $order->order_id }}"><i class="fa fa-print"></i> Print detalii</a>
						<div id="content_to_print"></div>
					</div>
				</div>
				<input type="hidden" id="order_id" name="order_id" value="{{ $order->order_id }}">
			</form>
		</div>

		<div class="box box-primary order-products" id="order-products">
			<table id="products-list">
				<tr class="header">
					<td>Produs</td>
					<td class="product-quantity">Cantitate</td>
					<td class="product-price">Pret</td>
					<td class="product-total">Total</td>
				</tr>
				@foreach ($order_products as $product)
				<tr class="item">
					<td><b>{{ $product->name }}</b><br>Cod produs: {{ $product->model }}</td>
					<td class="product-quantity">{{ $product->quantity }}</td>
					<td class="product-price">{{ $product->price }}</td>
					<td class="product-total">{{ $product->total }}</td>
				</tr>
				@endforeach
				<tr class="item">
					<td colspan="3">Cost livrare</td>
					<td class="order-total">{{ $order->shipping_cost }}</td>
				</tr>
				<tr class="total">
					<td colspan="3">Total</td>
					<td class="order-total">{{ $order->total }}</td>
				</tr>
			</table>
		</div>

		<div class="box box-primary order-awbs" id="order-awbs">
			<table id="awbs-list">
				<tr class="header">
					<td>Curier</td>
					<td>Numar AWB</td>
					<td>Data emitere</td>
					<td>Status AWB</td>
					<td>Data status</td>
				</tr>
				@foreach ($order_awbs as $awb)
				<tr class="awb">
					<td>{{ $awb->courier_name }}</td>
					<td>{{ $awb->awb_number }} <a href="{{ route('order.print_awb', $awb->awb_number) }}" target="_blank" class="btn btn-link download_awb"><i class="fa fa-download"></i></a></td>
					<td>
						<span><i class="fa fa-calendar"></i> {{ $awb->created_at_day }}</span>
						<span><i class="fa fa-clock-o"></i> {{ $awb->created_at_hour }}</span>
					</td>
					<td><span class="labelled_text {{ $awb->awb_status_class }}">{{ $awb->awb_status }}</span></td>
					<td>
						<span><i class="fa fa-calendar"></i> {{ $awb->last_status_day }}</span>
						<span><i class="fa fa-clock-o"></i> {{ $awb->last_status_hour }}</span>
					</td>
				</tr>
				@endforeach
			</table>
		</div>

	</section>

	<div class="modal fade" id="modal-create-awb" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"><div class="loading"></div></div>
		</div>
	</div>
</div>

@stop

@section('js')
<script src="{{ URL::asset('assets/plugins/printThis/printThis.js') }}"></script>
@stop

@section('title','Detalii comanda')