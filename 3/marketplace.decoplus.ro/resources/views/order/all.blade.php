@extends('main')

@section('content')

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Comenzile mele
		</h1>
	</section>

	<section class="content container-fluid">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Filtre</h3>
			</div>

			<div class="row box-filters">
				<div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<label for="filter_order_id">Identificare comanda</label>
					<input type="text" id="filter_order_id" placeholder="ID comanda">
				</div>
				<div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<label for="filter_order_status_id">Status</label>
					<select id="filter_order_status_id">
						<option value="" selected>Toate</option>
						@foreach ($orders_statuses as $status)
						<option value="{{ $status->order_status_id }}" class="status_option {{ $status->class }}">{{ $status->name }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="box-footer with-border">
				<div class="pull-left">
					<div id="dataTables_info_new"></div>
				</div>
				<div class="pull-right">
					<button id="search" class="btn btn-primary"><i class="fa fa-search"></i> Cauta</button> 
					<button id="reset" class="btn btn-default"><i class="fa fa-remove"></i> Reseteaza</i></button>
				</div>
			</div>

		</div>

		<table id="datatable" class="display" style="width:100%">
			<thead>
				<tr>
					<th>ID</th>
					<th>Detalii client</th>
					<th>Data plasarii</th>
					<th>Mod de plata</th>
					<th>Status</th>
					<th>Total</th>
					<th>Actiuni</th>
				</tr>
			</thead>
		</table>
	</section>
</div>

<div id="content_to_print"></div>

@stop

@section('js')
<script src="{{ URL::asset('assets/plugins/DataTables/datatables.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		datatable = $('#datatable').DataTable({
			processing: true,
			serverSide: true,
			bFilter: false,
			sDom: "itrlp",
			initComplete: (settings, json) => {
				$('.dataTables_info').appendTo('#dataTables_info_new');
			},
			ajax: {
				url: "{{ route('order.datatables') }}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: function (d) {
					d.filter_order_status_id = $('#filter_order_status_id').val();
					d.filter_order_id = $('#filter_order_id').val();
				}
			},
			order: [[ 0, "desc" ]],
			columns: [
			{
				data: "order_id"
			},
			{
				data: 'client_details', render: function (data, type, row) {
					return '<span class="client_name">' + row.client_name + '</span><span class="telephone">' + row.telephone + '</span>';
				},
				orderable: false
			},
			{
				data: 'date_added_in_dp', render: function (data, type, row) {
					return '<span class="date_added_day"><i class="fa fa-calendar"></i> ' + row.date_added_day + '</span><span class="date_added_hour"><i class="fa fa-clock-o"></i> ' + row.date_added_hour + '</span>';
				}
			},
			{
				data: "payment_method"
			},
			{
				data: 'status_name', render: function (data, type, row) {
					return '<span class="labelled_text ' + row.status_class + '">' + data + '</span>';
				}
			},
			{
				data: "total",
				orderable: false
			},
			{
				data: 'actions', render: function (data, type, row) {
					return '<div class="order_actions">' +
					'<a href="{{ url("/order/edit") }}/' + row.order_id + '" target="_blank" class="btn btn-xs btn-primary" title="Editeaza"><i class="fa fa-pencil"></i></a> ' +
					'<a class="btn btn-xs btn-warning details" title="Arata/ascunde detalii">' +
					'<span class="show_details"><i class="fa fa-caret-down"></i></span>' +
					'<span class="hide_details hide"><i class="fa fa-caret-up"></i></span>' +
					'</a>' +
					'</div>';
				},
				orderable: false
			},
			],
			language: {
				url: "{{ URL::asset('assets/plugins/DataTables/lang/Romanian.json') }}"
			}
		});

		$('#search').on("click", function () {
			datatable.draw();
		});

		$('#reset').on("click", function () {
			$('.box-filters input[type=text], .box-filters select').val(null).trigger('change');
			datatable.draw();
		});
	});
</script>
<script src="{{ URL::asset('assets/plugins/printThis/printThis.js') }}"></script>
@stop

@section('css')
<link href="{{ URL::asset('assets/plugins/DataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('title','Comenzile mele')