@extends('mail.main')

@section('content')

Salut,
<br><br>
Felicitari, ai primit o comanda noua in DecoPlus Marketplace si are numarul <a href="{{ route('order.edit', $order_id) }}">{{ $order_id }}</a>.
<br><br>
Poti administra si livra comanda rapid folosind link-ul:
<br><br>
<a href="{{ route('order.edit', $order_id) }}">{{ route('order.edit', $order_id) }}</a>
<br><br>
Echipa DecoPlus Marketplace 

@stop

@section('css')

@stop