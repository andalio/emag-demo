<style type="text/css">
    body{
        margin: 0;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
    }
    .main-container{

    }
    .logo{
        text-align: center;
        border: 1px solid #CCC;
        border-bottom: 0;
        max-width: 740px;
        margin: 0 auto;
    }
    .logo img{
        height: 60px;
        margin: 20px 0;
    }
    .content{
        max-width: 700px;
        margin: 0 auto;
        border: 1px solid #CCC;
        border-top: 0;
        padding: 20px;
    }
    .footer{
        text-align: center;
        color: #999;
        margin-top: 10px;
    }
    .header{
        text-align: center;
        font-size: 20px;
        font-weight: bold;
        background: #f0f0f0;
        padding: 25px 0;
        border-top: 2px solid #fad013;
        border-bottom: 2px solid #fad013;
    }
    .header .title{
        color: #6c6c6c;
    }
</style>

@yield('css')

<div class="main-container">

    <div class="logo">
        <a href="{{ url('/') }}"><img src="{{ url('assets/img/logo_marketplace.png') }}"></a>
    </div>

    <div class="header">
        <div class="title">Comanda noua in DecoPlus Marketplace {{ $order_id }}</div>
    </div>

    <div class="content">

        @yield('content')

    </div>

    <div class="footer">
        © {{ date('Y') }} | Business Media Factory SRL, Reg. Com. J18/155/2018, CUI: 38858410
    </div>
</div>