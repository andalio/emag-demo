<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>@yield('title','DecoPlus Marketplace')</title>

        <!-- Base url -->
        <script>var base_url = "{{ url('/') }}";</script>
        <!-- csrf token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Language -->
        <script>var locale = "{{App::getLocale()}}";</script>

        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <!-- Bootstrap -->
        <link href="{{ URL::asset('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ URL::asset('assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- Ionicons -->
        <link href="{{ URL::asset('assets/vendors/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
        <!-- jQuery UI -->
        <link href="{{ URL::asset('assets/vendors/jquery-ui/themes/base/jquery-ui.css') }}" rel="stylesheet">
        <!-- jQuery Select2 -->
        <link href="{{ URL::asset('assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
        <!-- Theme style -->
        <link href="{{ URL::asset('assets/css/AdminLTE.min.css') }}" rel="stylesheet">
        <!-- Theme custom -->
        <link href="{{ URL::asset('assets/css/skins/skin-custom.css') }}" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        @yield('css')
    </head>

    <body class="nav-md skin-custom sidebar-mini">

        <div class="wrapper">

            <header class="main-header">

                <a href="{{ route('dashboard') }}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>DP</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>DP</b> Marketplace</span>
                </a>

                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="hidden-xs">{{ Auth::user()->display_name }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Dashboard</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{ route('logout') }}" class="btn btn-default btn-flat"><i class="fa fa-power-off"></i> Iesire</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">

                <section class="sidebar">

                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="active"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-user"></i> <span>Contul meu</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('myaccount.profile') }}"><i class="fa fa-folder-o"></i> Profil</a></li>
                                <li><a href="{{ route('myaccount.couriers') }}"><i class="fa fa-folder-o"></i> Conturi de curier</a></li>
                                <li><a href="#"><i class="fa fa-folder-o"></i> Documente legale</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-list-ul"></i> <span>Produse</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('product.all') }}"><i class="fa fa-folder-o"></i> Produsele mele</a></li>
                                <li><a href="{{ route('product.add') }}"><i class="fa fa-plus"></i> Adauga produs</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('order.all') }}"><i class="fa fa-shopping-cart"></i> <span>Comenzi</span></a></li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-truck"></i> <span>Logistica</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('delivery.all') }}"><i class="fa fa-folder-o"></i> Livrarile mele</a></li>
                                <li><a href="{{ route('myaccount.couriers') }}"><i class="fa fa-folder-o"></i> Conturi de curier</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="fa fa-undo"></i> <span>Retururi</span></a></li>
                        <li><a href="#"><i class="fa fa-file-text-o"></i> <span>Financiar</span></a></li>
                    </ul>
                </section>
            </aside>

            @yield('content')

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <a href="https://www.decoplus.ro" target="_blank">decoplus.ro</a>
                </div>
                © {{ date('Y') }} | Business Media Factory SRL, Reg. Com. J18/155/2018, CUI: 38858410
            </footer>

        </div>
    </div>

    <!-- jQuery -->
    <script src="{{ URL::asset('assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- jQuery Form Plugin -->
    <script src="{{ URL::asset('assets/vendors/jquery-form/jquery.form.min.js') }}"></script>
    <!-- jQuery Form Plugin -->
    <script src="{{ URL::asset('assets/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- jQuery Select2 -->
    <script src="{{ URL::asset('assets/vendors/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/select2/dist/js/i18n/ro.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/select2/dist/js/i18n/en.js') }}"></script>
    <!-- jQuery Migrate -->
    <script src="{{ URL::asset('assets/js/jquery-migrate-3.1.0.min.js') }}"></script>

    @yield('js')

    <!-- AdminLTE App -->
    <script src="{{ URL::asset('assets/js/adminlte.min.js') }}"></script>
    <!-- Custom -->
    <script src="{{ URL::asset('assets/js/custom.js') }}"></script>

</body>
</html>