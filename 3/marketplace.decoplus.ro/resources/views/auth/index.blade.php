<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Deco Plus Marketplace</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <!-- Bootstrap -->
        <link href="{{ URL::asset('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ URL::asset('assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- Ionicons -->
        <link href="{{ URL::asset('assets/vendors/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
        <!-- Theme style -->
        <link href="{{ URL::asset('assets/css/AdminLTE.min.css') }}" rel="stylesheet">
        <!-- iCheck -->
        <link href="{{ URL::asset('assets/plugins/iCheck/square/blue.css') }}" rel="stylesheet">
        <!-- Theme custom -->
        <link href="{{ URL::asset('assets/css/skins/skin-custom.css') }}" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <!-- Base url -->
        <script>var base_url = '{{ url('') }}';</script>
        <!-- csrf token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <b>Deco Plus</b> Marketplace
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Autentifica-te</p>

                <h4 class="auth-error text-center hide">Eroare autentificare</h4>

                <form role="form" method="post" id="login_form">
                    <div class="form-group has-feedback">
                        <input type="email" name="email" class="form-control" placeholder="email" required>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="parola" required>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember_me"> Tine-ma minte
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">autentificare</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <a href="#recover">Ai uitat parola?</a><br>
                <a href="#register" class="text-center">Nu ai cont?</a>

            </div>
            <!-- /.login-box-body -->

            <div class="login-footer text-center">
                <br><strong>Copyright &copy; 2018-{{ date('Y') }}
                    <br>Business Media Factory SRL</strong>
                <br>Toate drepturile rezervate
            </div>
        </div>
        <!-- /.login-box -->

        <!-- jQuery -->
        <script src="{{ URL::asset('assets/vendors/jquery/dist/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        <script src="{{ URL::asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- iCheck -->
        <script src="{{ URL::asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
        <!-- jQuery Form Plugin -->
        <script src="{{ URL::asset('assets/vendors/jquery-form/jquery.form.min.js') }}"></script>
        <!-- Custom -->
        <script src="{{ URL::asset('assets/js/custom.js') }}"></script>
        <!-- Custom auth -->
        <script src="{{ URL::asset('assets/js/custom_auth.js') }}"></script>
    </body>
</html>
