<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>@yield('title','DecoPlus Marketplace')</title>

    <!-- Base url -->
    <script>var base_url = "{{ url('/') }}";</script>
    <!-- csrf token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Language -->
    <script>var locale = "{{App::getLocale()}}";</script>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap -->
    <link href="{{ URL::asset('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ URL::asset('assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="{{ URL::asset('assets/vendors/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <!-- jQuery UI -->
    <link href="{{ URL::asset('assets/vendors/jquery-ui/themes/base/jquery-ui.css') }}" rel="stylesheet">
    <!-- jQuery Select2 -->
    <link href="{{ URL::asset('assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ URL::asset('assets/css/AdminLTE.min.css') }}" rel="stylesheet">
    <!-- Theme custom -->
    <link href="{{ URL::asset('assets/css/skins/skin-custom.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    @yield('css')
</head>

<body class="skin-custom">
    @yield('content')
</div>

<!-- jQuery -->
<script src="{{ URL::asset('assets/vendors/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ URL::asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- jQuery Form Plugin -->
<script src="{{ URL::asset('assets/vendors/jquery-form/jquery.form.min.js') }}"></script>
<!-- jQuery Form Plugin -->
<script src="{{ URL::asset('assets/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- jQuery Select2 -->
<script src="{{ URL::asset('assets/vendors/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ URL::asset('assets/vendors/select2/dist/js/i18n/ro.js') }}"></script>
<script src="{{ URL::asset('assets/vendors/select2/dist/js/i18n/en.js') }}"></script>
<!-- jQuery Migrate -->
<script src="{{ URL::asset('assets/js/jquery-migrate-3.1.0.min.js') }}"></script>

@yield('js')

<!-- AdminLTE App -->
<script src="{{ URL::asset('assets/js/adminlte.min.js') }}"></script>
<!-- Custom -->
<script src="{{ URL::asset('assets/js/custom.js') }}"></script>

</body>
</html>