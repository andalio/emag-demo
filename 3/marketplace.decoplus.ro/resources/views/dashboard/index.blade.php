@extends('main')

@section('content')

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Dashboard
		</h1>
	</section>

	<section class="content container-fluid">


		<div class="row">
			<div class="col-lg-2 col-xs-6">
				<div class="small-box bg-green">
					<div class="inner">
						<h3>150</h3>
						<p>Produse publicate</p>
					</div>
					<a href="#" class="small-box-footer">
						Vezi produsele <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-2 col-xs-6">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>150</h3>
						<p>Produse in validare</p>
					</div>
					<a href="#" class="small-box-footer">
						Vezi produsele <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-2 col-xs-6">
				<div class="small-box bg-red">
					<div class="inner">
						<h3>150</h3>
						<p>Produse respinse</p>
					</div>
					<a href="#" class="small-box-footer">
						Vezi produsele <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
		</div>


	</section>
</div>

@stop

@section('title','Dashboard')