@extends('main')

@section('content')

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Livrarile mele
		</h1>
	</section>

	<section class="content container-fluid">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Filtre</h3>
			</div>

			<div class="row box-filters">
				<div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<label for="filter_awb_id">Numar AWB</label>
					<input type="text" id="filter_awb_id">
				</div>
				<div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<label for="filter_order_id">Numar comanda</label>
					<input type="text" id="filter_order_id">
				</div>
				<div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<label for="filter_awb_status_id">Status</label>
					<select id="filter_awb_status_id">
						<option value="" selected>Toate</option>
						@foreach ($awbs_statuses as $status)
						<option value="{{ $status->awb_status_id }}" class="status_option {{ $status->class }}">{{ $status->name }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="box-footer with-border">
				<div class="pull-left">
					<div id="dataTables_info_new"></div>
				</div>
				<div class="pull-right">
					<button id="search" class="btn btn-primary"><i class="fa fa-search"></i> Cauta</button> 
					<button id="reset" class="btn btn-default"><i class="fa fa-remove"></i> Reseteaza</i></button>
				</div>
			</div>

		</div>

		<table id="datatable" class="display" style="width:100%">
			<thead>
				<tr>
					<th>Curier</th>
					<th>Numar AWB</th>
					<th>Comanda</th>
					<th>Data emitere</th>
					<th>Status</th>
					<th>Data status</th>
					<th>Info livrare</th>
					<th>Ramburs</th>
					<th>Actiuni</th>
				</tr>
			</thead>
		</table>
	</section>
</div>

@stop

@section('js')
<script src="{{ URL::asset('assets/plugins/DataTables/datatables.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		datatable = $('#datatable').DataTable({
			processing: true,
			serverSide: true,
			bFilter: false,
			sDom: "itrlp",
			initComplete: (settings, json) => {
				$('.dataTables_info').appendTo('#dataTables_info_new');
			},
			ajax: {
				url: "{{ route('delivery.datatables') }}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: function (d) {
					d.filter_awb_status_id = $('#filter_awb_status_id').val();
					d.filter_awb_id = $('#filter_awb_id').val();
					d.filter_order_id = $('#filter_order_id').val();
				}
			},
			columns: [
			{
				data: "courier_name"
			},
			{
				data: "awb_number"
			},
			{
				data: 'order_id', render: function (data, type, row) {
					return '<a href="{{ url("/order/edit") }}/' + data + '" target="_blank">' + data + '</a>';
				},
				orderable: false
			},
			{
				data: 'created_at_day', render: function (data, type, row) {
					return '<span><i class="fa fa-calendar"></i> ' + row.created_at_day + '</span> ' + 
					'<span><i class="fa fa-clock-o"></i> ' + row.created_at_hour + '</span>';
				},
				orderable: false
			},
			{
				data: 'awb_status', render: function (data, type, row) {
					return '<span class="labelled_text ' + row.awb_status_class + '">' + row.awb_status + '</span>';
				},
				orderable: false
			},
			{
				data: 'awb_status', render: function (data, type, row) {
					return '<span><i class="fa fa-calendar"></i> ' + row.last_status_day + '</span> ' +
					'<span><i class="fa fa-clock-o"></i> ' + row.last_status_hour + '</span>';
				},
				orderable: false
			},
			{
				data: 'shipping_contact', render: function (data, type, row) {
					return '<b>' + row.shipping_contact + '</b><br>' + row.shipping_telephone;
				},
				orderable: false
			},
			{
				data: "cash_on_delivery"
			},
			{
				data: 'actions', render: function (data, type, row) {
					return '<div class="awb_actions">' +
					'<a href="{{ URL::asset("order/print_awb/") }}/' + row.awb_number + '" target="_blank" class="btn btn-link download_awb"><i class="fa fa-download"></i></a>' +
					'</div>';
				},
				orderable: false
			},
			],
			language: {
				url: "{{ URL::asset('assets/plugins/DataTables/lang/Romanian.json') }}"
			}
		});

		$('#search').on("click", function () {
			datatable.draw();
		});

		$('#reset').on("click", function () {
			$('.box-filters input[type=text], .box-filters select').val(null).trigger('change');
			datatable.draw();
		});
	});
</script>
@stop

@section('css')
<link href="{{ URL::asset('assets/plugins/DataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('title','Livrarile mele')