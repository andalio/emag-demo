@extends('main')

@section('content')

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Adauga curier - {{ $courier->name }}
		</h1>
	</section>

	<section class="content container-fluid">
		<div class="alert alert-success hide" id="alert_success">
            <i class="icon fa fa-check"></i>  <span class="message"></span>
        </div>
        <div class="alert alert-error hide" id="alert_error">
            <i class="icon fa fa-check"></i>  <span class="message"></span>
        </div>
		<div class="box box-primary">
			<div class="box-body">
				<div class="col-lg-1 col-md-6 col-sm-12 col-xs-12 courier">
					<img src="{{url('assets/img/couriers/' . $courier->image)}}" title="{{$courier->name}}">
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 courier">
					<form action="{{ route('myaccount.courier_process', ['action' => 'add','id' => $courier->courier_id]) }}" method="post" id="form" enctype="multipart/form-data">
						<div class="box-body">

							@if ($courier->courier_id === 1)
							<div class="form-group">
								<label for="seller_id">Seller ID <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="text" name="seller_id" id="seller_id" class="form-control" placeholder="Seller ID primit de la Sameday">
							</div>
							@endif

							@if ($courier->courier_id === 2)
							<div class="form-group">
								<label for="username">Utilizator <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="text" name="username" id="username" class="form-control" placeholder="username">
							</div>
							<div class="form-group">
								<label for="password">Parola <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="password" name="password" id="password" class="form-control" placeholder="password">
							</div>
							<div class="form-group">
								<label for="client_id">Cod client <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="text" name="client_id" id="client_id" class="form-control" placeholder="client id">
							</div>
							@endif

							@if ($courier->courier_id === 3)
							<div class="form-group">
								<label for="username">Utilizator <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="text" name="username" id="username" class="form-control" placeholder="username">
							</div>
							<div class="form-group">
								<label for="password">Parola <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="password" name="password" id="password" class="form-control" placeholder="password">
							</div>
							<div class="form-group">
								<label for="key">API Primary Key <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<input type="text" name="key" id="key" class="form-control" placeholder="primary key">
							</div>
							@endif


                            <button type="submit" class="btn btn-primary">Conectare</button>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>

@stop

@section('js')

@stop

@section('css')

@stop

@section('title','Adauga curier')