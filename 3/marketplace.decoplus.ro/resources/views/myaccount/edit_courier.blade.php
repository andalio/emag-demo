@extends('main')

@section('content')

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Setari curier - {{ $courier->name }}
		</h1>
	</section>

	<section class="content container-fluid">
		<div class="alert alert-success hide" id="alert_success">
			<i class="icon fa fa-check"></i>  <span class="message"></span>
		</div>
		<div class="alert alert-error hide" id="alert_error">
			<i class="icon fa fa-check"></i>  <span class="message"></span>
		</div>
		<div class="box box-primary">
			<div class="box-body">
				<div class="col-lg-1 col-md-6 col-sm-12 col-xs-12 courier">
					<img src="{{url('assets/img/couriers/' . $courier->image)}}" title="{{$courier->name}}">
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 courier">
					<form action="{{ route('myaccount.courier_process', ['action' => 'edit','id' => $courier->courier_id]) }}" method="post" id="form" enctype="multipart/form-data">
						<div class="box-body">

							@if ($courier->courier_id === 1)
							<div class="form-group">
								<label for="pickup_point_contact_person">Punct de ridicare implicit <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<select id="pickup_point_contact_person" name="pickup_point_contact_person" class="form-control select2">
									<option value="">Selecteaza</option>
									@foreach ($pickup_points as $pickup_point)
									<option value="{{ $pickup_point->id . '|' .$pickup_point->pickupPointContactPerson[0]->id }}" @if($pickup_point->id === $courier->pickup_point) selected @endif>{{ $pickup_point->alias . ' (' . $pickup_point->address . ')' }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="service">Serviciu <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<select id="service" name="service" class="form-control select2">
									<option value="">Selecteaza</option>
									@foreach ($services as $service)
									<option value="{{ $service->id }}" @if($service->id === $courier->service) selected @endif>{{ $service->deliveryType->name . ' - ' . $service->name }}</option>
									@endforeach
								</select>
							</div>
							@endif

							{{--
							@if ($courier->courier_id === 2)
							<div class="form-group">
								<label for="service">Serviciu <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<select id="service" name="service" class="form-control select2">
									<option value="">Selecteaza</option>
									@foreach ($services as $service)
									<option value="{{ $service->id }}" @if($service->id === $courier->service) selected @endif>{{ $service->name }}</option>
									@endforeach
								</select>
							</div>
							@endif
							--}}

							@if ($courier->courier_id === 3)
							<div class="form-group">
								<label for="pickup_point">Punct de ridicare implicit <span class="required">*</span></label>
								<span class="badge bg-red error-badge"></span>
								<select id="pickup_point" name="pickup_point" class="form-control select2">
									<option value="">Selecteaza</option>
									@foreach ($pickup_points as $pickup_point)
									<option value="{{ $pickup_point->LocationId }}" @if($pickup_point->LocationId === $courier->pickup_point) selected @endif>{{ $pickup_point->Name . ' (' . $pickup_point->AddressText . ', ' . $pickup_point->LocalityName . ', ' . $pickup_point->CountyName . ')' }}</option>
									@endforeach
								</select>
							</div>
							@endif

							<div class="checkbox">
								<label>
									<input type="checkbox" name="default" value="1" @if($courier->default == 1) checked @endif> implicit
								</label>
							</div>

							<input type="hidden" name="vendor_courier_id" value="{{ $courier->vendor_courier_id }}">
							<button type="submit" class="btn btn-primary">Salveaza</button>
						</div>
					</form>
				</div>
			</div>
			
		</div>
	</section>
</div>

@stop

@section('js')

@stop

@section('css')

@stop

@section('title','Setari curier')