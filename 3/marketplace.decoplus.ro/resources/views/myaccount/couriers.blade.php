@extends('main')

@section('content')

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Conturi de curier
		</h1>
	</section>

	<section class="content container-fluid">
		<div class="box box-primary">
			<div class="box-body">
				<a href="#" class="btn btn-sm btn-success add-new-courier"><i class="fa fa-plus"></i> Adauga cont nou de curier</a>
			</div>
			<div class="box-body couriers_list hide">
				<div class="row">
					@foreach ($couriers as $courier)
					<div class="col-lg-1 col-md-6 col-sm-12 col-xs-12 courier">
						<a href="{{ route('myaccount.add_courier', $courier->courier_id) }}">
							<img src="{{url('assets/img/couriers/' . $courier->image)}}" title="{{$courier->name}}">
						</a>
					</div>
					@endforeach
				</div>
			</div>

			@if($vendor_couriers->count() > 0)
			<div class="box-body">
				<table id="couriers-list">
					<tr class="header">
						<td>ID</td>
						<td>Curier</td>
						<td>Curier preferat</td>
						<td>Actiuni</td>
					</tr>
					@foreach ($vendor_couriers as $vendor_courier)
					<tr class="item">
						<td>{{ $vendor_courier->vendor_courier_id }}</td>
						<td>{{ $vendor_courier->courier_name }}</td>
						<td>@if($vendor_courier->default) <i class="fa fa-check-circle"></i> @endif</td>
						<td><a href="{{ route('myaccount.edit_courier', $vendor_courier->vendor_courier_id) }}" class="btn btn-xs btn-primary" title="Setari"><i class="fa fa-cogs"></i></a> <button class="btn btn-xs btn-danger" title="Sterge"><i class="fa fa-trash"></i></button></td>
					</tr>
					@endforeach
				</table>
			</div>
			@endif
		</div>
	</section>
</div>

@stop

@section('js')

@stop

@section('css')

@stop

@section('title','Conturi de curier')