@extends('main')

@section('content')

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Profil vanzator
		</h1>
	</section>

	<section class="content container-fluid">
		<div class="box box-primary">
			<div class="box-body">
				<form action="{{ route('myaccount.profile_save') }}" method="post" id="form" enctype="multipart/form-data">

					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label for="display_name">Nume afisat</label>
								<input type="text" name="display_name" id="display_name" class="form-control" value="{{ $user_setting->display_name }}" disabled>
							</div>

							<div class="form-group">
								<label for="account_lifetime">Vechime cont</label>
								<input type="text" name="account_lifetime" id="account_lifetime" class="form-control" value="{{ $user_setting->account_lifetime }} luni" disabled>
							</div>
						</div>

						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label for="company_name">Companie</label>
								<input type="text" name="company_name" id="company_name" class="form-control" value="{{ $user_setting->company_name }}" disabled>
							</div>

							<div class="form-group">
								<label for="company_county_name">Judet</label>
								<input type="text" name="company_county_name" id="company_county_name" class="form-control" value="{{ $user_setting->company_county_name }}" disabled>
							</div>

							<div class="form-group">
								<label for="company_city">Localitate</label>
								<input type="text" name="company_city" id="company_city" class="form-control" value="{{ $user_setting->company_city }}" disabled>
							</div>

							<div class="form-group">
								<label for="company_address">Adresa</label>
								<input type="text" name="company_address" id="company_address" class="form-control" value="{{ $user_setting->company_address }}" disabled>
							</div>
						</div>

						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label for="company_bank">Banca</label>
								<input type="text" name="company_bank" id="company_bank" class="form-control" value="{{ $user_setting->company_bank }}" disabled>
							</div>

							<div class="form-group">
								<label for="company_iban">IBAN</label>
								<input type="text" name="company_iban" id="company_iban" class="form-control" value="{{ $user_setting->company_iban }}" disabled>
							</div>

							<div class="form-group">
								<label for="company_phone">Telefon</label>
								<input type="text" name="company_phone" id="company_phone" class="form-control" value="{{ $user_setting->company_phone }}" disabled>
							</div>

							<div class="form-group">
								<label for="return_period">Timp de retur</label>
								<select id="return_period" name="return_period" class="form-control select2" disabled>
									<option value="{{ $user_setting->return_period }}">{{ $user_setting->return_period }} zile</option>
								</select>
							</div>
						</div>

						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label for="contact_name">Persoana de contact</label>
								<input type="text" name="contact_name" id="contact_name" class="form-control" value="{{ $user_setting->contact_name }}" disabled>
							</div>

							<div class="form-group">
								<label for="contact_phone">Telefon persoana de contact</label>
								<input type="text" name="contact_phone" id="contact_phone" class="form-control" value="{{ $user_setting->contact_phone }}" disabled>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</section>
</div>

@stop

@section('js')

@stop

@section('css')

@stop

@section('title','Profil vanzator')