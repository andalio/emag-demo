@extends('main_base' . $view_mode)

@section('content')
<form action="{{ route('product.import_process') }}" method="post" id="import_form" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="modal-header">
		<h5 class="modal-title">Import produse</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Inchide">
			<span aria-hidden="true">×</span>
		</button>
	</div>
	<div class="modal-body">
		<section class="content container-fluid create_awb_content">
			<div class="alert alert-success hide" id="import_products_alert_success">
				<i class="icon fa fa-check"></i>  <span class="message"></span>
			</div>
			<div class="alert alert-error hide" id="import_products_alert_error">
				<i class="icon fa fa-check"></i>  <span class="message"></span>
			</div>
			<div class="box box-primary import_products_select_file">
				<label for="import_file_xls" class="input-file-trigger file-xls">Selecteaza fisier XLS</label>
				<input type="file" id="import_file_xls" name="import_file_xls" class="input-file hide" accept=".xls,.xlsx">
				<br>
				<div class="overlay hide">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
			</div>
			<div class="box box-primary import_products_errors hide"></div>
		</div>
	</section>
</div>
<div class="modal-footer justify-content-between">
	<button type="button" class="btn btn-link float-left" data-dismiss="modal"><i class="fa fa-remove"></i> Anuleaza</button>
	<button type="submit" class="btn btn-success" id="import_products_save"><i class="fa fa-check"></i> Start import</button>
</div>
</form>
@stop

@section('title','Import products')