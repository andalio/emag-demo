@extends('main')

@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Editeaza produs
        </h1>
    </section>

    <section class="content container-fluid">

        <div class="alert alert-success hide" id="alert_success">
            <i class="icon fa fa-check"></i>  <span class="message"></span>
        </div>

        <div class="alert alert-error hide" id="alert_error">
            <i class="icon fa fa-check"></i>  <span class="message"></span>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="box box-primary">
                    <form action="{{ route('product.process', 'edit') }}" method="post" id="form" enctype="multipart/form-data">
                    	<input type="hidden" name="action" value="edit">
                    	<div class="box-body">
                           <div class="form-group category">
                            <label for="category">Categorie <span class="required">*</span></label>
                            <span class="badge bg-red error-badge"></span>
                            <select id="category" name="category" class="form-control select2">
                                <option value="{{ $product->category_id }}">{{ $product->category_name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Denumire <span class="required">*</span></label>
                            <span class="badge bg-red error-badge"></span>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Set mobilier dormitor Elissa Stejar Sonoma" value="{{ $product->name }}">
                        </div>
                        <div class="form-group brand">
                            <label for="brand">Brand <span class="required">*</span></label>
                            <span class="badge bg-red error-badge"></span>
                            <select id="brand" name="brand" class="form-control select2">
                                <option value="{{ $product->manufacturer_id }}">{{ $product->manufacturer }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="model">Cod produs <span class="required">*</span></label>
                            <span class="badge bg-red error-badge"></span>
                            <input type="text" name="model" id="model" class="form-control" placeholder="mob123123" value="{{ $product->model }}">
                        </div>
                        <div class="form-group">
                            <label for="product_images_null">Imagini <span class="required">*</span></label>
                            <span class="badge bg-red error-badge"></span>
                            <label for="product_images" class="input-file-trigger">Adauga imagini</label>
                            <input type="file" id="product_images" name="product_images" class="input-file hide" multiple>
                            <ul id="uploaded_images">
                                @foreach ($product->product_images as $image)
                                <li>
                                    <img src="/uploads/products/{{ str_replace_first('.', '_100x100.', str_replace_first('catalog/products/', '', $image)) }}" class="profile-user-img">
                                    <input type="hidden" name="uploaded_images[]" value="{{ str_replace_first('catalog/products/', '', $image) }}">
                                    <span class="remove-image">X</span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="form-group">
                            <label for="description">Descriere <span class="required">*</span></label>
                            <span class="badge bg-red error-badge"></span>
                            <textarea name="description" id="description" class="form-control">{{ $product->description }}</textarea>
                        </div>
                        <div class="row">
                         <div class="col-md-6">
                          <div class="form-group">
                              <label for="selling_price">Pret vanzare (pret nou) <span class="required">*</span></label>
                              <span class="badge bg-red error-badge"></span>
                              <input type="text" name="selling_price" id="selling_price" class="form-control" placeholder="99" value="{{ $product->special }}">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="recommended_price">Pret recomandat (pret vechi)</label>
                              <span class="badge bg-red error-badge"></span>
                              <input type="text" name="recommended_price" id="recommended_price" class="form-control" placeholder="120" value="{{ $product->price }}">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="quantity">Cantitate stoc <span class="required">*</span></label>
                    <span class="badge bg-red error-badge"></span>
                    <input type="text" name="quantity" id="quantity" class="form-control" placeholder="10" value="{{ $product->quantity }}">
                </div>
                <div class="form-group">
                    <label for="weight">Greutate (kg) <span class="required">*</span></label>
                    <span class="badge bg-red error-badge"></span>
                    <input type="text" name="weight" id="weight" class="form-control" placeholder="15" value="{{ $product->weight }}">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="active" @if($product->status == 1) checked @endif> activ
                    </label>
                </div>
            </div>
            <div class="box-footer">
                <input type="hidden" id="product_id" name="product_id" value="{{ $product->product_id }}">
                <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
        </form>
    </div>
</div>
<div class="col-md-4">
    <div class="box box-default">
        <div class="box-body">
            <b>Denumire</b>
            <br>
            - Foloseste un titlu scurt, descriptiv si concret al produsului
            <br>
            - Trebuie sa contina minim 10 caractere
            <br>
            - Nu trebuie sa contina prescurtari ale cuvintelor
            <br><br>
        </div>
    </div>
</div>


</section>
</div>

@stop

@section('js')
<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
	CKEDITOR.replace( 'description' );

    $(document).ready(function (){
        $('#product_images').on('change', function () {

            $('label[for=product_images]').text('Asteapta...');

            var formData = new FormData(document.getElementById('form'));

            var files = document.getElementById('product_images').files;
            for (var i = files.length - 1; i >= 0; i--) {
                formData.append('files[]', files[i]);
            }

            $.ajax({
                method: 'POST',
                url: '{{ route('product.images_upload') }}',
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSubmit : function(arr, $form, options){
                    console.log('aaaa');
                },
            }).done(function (data) {
                $.each(data, function (id, image) {

                    var to_append = '<li>';
                    to_append += '<img src="' + base_url + '/uploads/products/' + image['100x100'] + '" class="profile-user-img">';
                    to_append += '<input type="hidden" name="uploaded_images[]" value="' + image['full'] + '">';
                    to_append += '<span class="remove-image">X</span>';
                    to_append += '</li>';

                    $('#uploaded_images').append(to_append);

                    $('label[for=product_images]').text('Adauga imagini');

                });
            })
            .fail(function () {
                console.log('Get data failed!');
            });

        });

        $(document).on('click', '.remove-image', function () {
            $(this).parent().remove();
        });
    });
</script>
@stop

@section('title','Editeaza produs')