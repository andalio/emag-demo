@extends('main')

@section('content')

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Produsele mele
		</h1>
		<a href="{{ route('product.add') }}" class="btn btn-sm btn-success add-new" title="Adauga produs"><i class="fa fa-plus"></i> Adauga</a>
	</section>

	<section class="content container-fluid">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Filtre</h3>
			</div>

			<div class="row box-filters">
				<div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-6">
                    <label for="identifier">Identificare produs</label>
                    <input type="text" id="identifier" placeholder="ID, cod produs">
                </div>
				<div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<label for="status">Status</label>
					<select id="status">
						<option value="" selected>Toate</option>
						<option value="1">Active</option>
						<option value="0">Inactive</option>
					</select>
				</div>
			</div>

			<div class="box-footer with-border">
				<div class="pull-left">
					<div id="dataTables_info_new"></div>
				</div>
				<div class="pull-right">
					<button id="search" class="btn btn-primary"><i class="fa fa-search"></i> Cauta</button> 
					<button id="reset" class="btn btn-default"><i class="fa fa-remove"></i> Reseteaza</button>
				</div>
			</div>

		</div>

		<div class="import-products">
			<a href="{{ url('docs/template-import-products-decoplus.xlsx') }}" class="btn btn-link download_template"><i class="fa fa-download"></i> Descarca template Excel</a>
			<button type="button" class="btn btn-sm btn-success import_products" data-toggle="modal" data-target="#modal-import-products"><i class="fa fa-upload"></i> Import XLS</button>
		</div>

		<table id="datatable" class="display" style="width:100%">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nume produs</th>
					<th>Cod produs</th>
					<th>Status</th>
					<th>Actiuni</th>
				</tr>
			</thead>
		</table>
	</section>

	<div class="modal fade" id="modal-import-products" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"><div class="loading"></div></div>
		</div>
	</div>
</div>

@stop

@section('js')
<script src="{{ URL::asset('assets/plugins/DataTables/datatables.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		datatable = $('#datatable').DataTable({
			processing: true,
			serverSide: true,
			bFilter: false,
			sDom: "itrlp",
			initComplete: (settings, json) => {
				$('.dataTables_info').appendTo('#dataTables_info_new');
			},
			ajax: {
				url: "{{ route('script.dp_api') }}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: function (d) {
					d.route = 'api/product/list_by_vendor';
					d.status = $('#status').val();
					d.identifier = $('#identifier').val();
				}
			},
			columns: [
			{
				data: "product_id",
				orderable: false
			},
			{
				data: "name",
				orderable: false
			},
			{
				data: "model",
				orderable: false
			},
			{
				data: 'status', render: function (data, type, row) {
					if (data == 1) {
						return '<i class="fa fa-circle" aria-hidden="true"></i>';
					} else {
						return '<i class="fa fa-circle-o" aria-hidden="true"></i>';
					}
				},
				orderable: false
			},
			{
                data: 'product_id', render: function (data, type, row) {
                    return '<a href="{{ url("/product/edit") }}/' + data + '" class="btn btn-xs btn-primary" title="Editeaza"><i class="fa fa-pencil"></i></a> <a href="{{ url("/product/edit") }}/' + data + '/clone" class="btn btn-xs btn-warning" title="Cloneaza"><i class="fa fa-clone"></i></a>';
                },
                orderable: false
            },
			],
			language: {
				url: "{{ URL::asset('assets/plugins/DataTables/lang/Romanian.json') }}"
			}
		});

		$('#search').on("click", function () {
			datatable.draw();
		});

		$('#reset').on("click", function () {
			$('.box-filters input[type=text], .box-filters select').val(null).trigger('change');
			datatable.draw();
		});
	});
</script>
@stop

@section('css')
<link href="{{ URL::asset('assets/plugins/DataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('title','Produsele mele')