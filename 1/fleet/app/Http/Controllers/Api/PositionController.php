<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Position;
use App\Http\Resources\PositionResource;
use Illuminate\Http\Request;

use DB;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positions = Position::select()
        ->selectRaw('X(lat_lng) AS lat')
        ->selectRaw('Y(lat_lng) AS lng')
        ->limit(1000)
        ->get();

        return PositionResource::collection($positions);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        return new PositionResource($position);
    }

}
