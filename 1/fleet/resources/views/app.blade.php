<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Fleet</title>
</head>
<body>
	<div id="app">
		<router-view />
	</div>
	<script src="{{mix('js/app.js')}}"></script>
</body>
</html>