import { ref } from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';

export default function usePositions(){
	const positions = ref([]);
	const position = ref([]);
	const router = useRouter();

	const getPositions = async () => {
		let response = await axios.get('/fleet/public/api/positions');
		positions.value = response.data.data;
	}

	const getPosition = async (id) => {
		let response = await axios.get('/fleet/public/api/positions/' + id);
		position.value = response.data.data;
	}

	return {
		positions,
		position,
		getPositions,
		getPosition
	}
}