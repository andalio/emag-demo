require('./bootstrap');

import {createApp} from 'vue';

import router from './router/';

import ClientsIndex from './components/clients/ClientsIndex';
import MapIndex from './components/map/MapIndex';

import Home from './components/Home'

createApp({
    components: {
        Home,
        ClientsIndex,
        MapIndex
    }
})
.use(router)
.mount('#app')