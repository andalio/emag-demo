import {createRouter, createWebHistory } from 'vue-router';

import Home from '../components/Home'
import ClientsIndex from '../components/clients/ClientsIndex'
import ClientsCreate from '../components/clients/ClientsCreate'
import ClientsEdit from '../components/clients/ClientsEdit'
import MapIndex from '../components/map/MapIndex'

const routes = [
{
	path: '/fleet/home',
	name: 'home',
	component: Home
},
{
	path: '/fleet/clients',
	name: 'clients.index',
	component: ClientsIndex
},
{
	path: '/fleet/clients/create',
	name: 'clients.create',
	component: ClientsCreate
},
{
	path: '/fleet/clients/:id/edit',
	name: 'clients.edit',
	component: ClientsEdit,
	props: true
},
{
	path: '/fleet/map',
	name: 'map.index',
	component: MapIndex,
	props: true
}
]

export default createRouter({
	history: createWebHistory(),
	routes
})