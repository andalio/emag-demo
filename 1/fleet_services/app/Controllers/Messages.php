<?php

namespace Controllers;

use Models\Message;
use Models\Queue_message;
use Models\Position;
use Illuminate\Database\Capsule\Manager as DB;

class Messages {

	public static function save_message($data){
		
		// Salvam mesajul primit de la echipament
		$message = Message::create([
			'data' => json_encode($data),
			'device_id' => $data['device_id']
		]);

		// Salvam mesajul in coada de procesare
		$queue = Queue_message::create([
			'message_id' => $message->id
		]);

	}

	public static function process_messages(){

		$messages = Queue_message::select(
			'messages.id',
			'messages.data'
		)
		->join('messages', 'messages.id', 'queue_messages.message_id')
		->limit(10)
		->get();

		foreach ($messages as $message) {

			$position = [];
			$new_position = '';

			$message_data = json_decode($message->data, true);

			// Construim setul de date pentru pozitia din mesaj
			$position = [
				'message_id' => $message->id,
				'lat_lng' => DB::raw("ST_GEOMFROMTEXT('POINT(" . $message_data['lat'] . " " . $message_data['lng'] . ")')"),
				'speed' => $message_data['speed'],
				'msg_time' => date('Y-m-d H:i:s', $message_data['timestamp'])
			];

			// Salvam pozitia in baza de date
			$new_position = Position::create($position);

			if($new_position->id){

				// Stergem mesajul din queue
				Queue_message::where('message_id', $message->id)->delete();

			} else {

				echo 'Eroare la salvarea pozitiei!';
				exit;

			}
			

		}

	}

}