<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

	protected $table = 'messages';
	protected $fillable = ['data', 'device_id'];

}