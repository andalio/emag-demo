<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Queue_message extends Model {

	protected $table = 'queue_messages';
	protected $fillable = ['message_id'];
	public $timestamps = false;

}