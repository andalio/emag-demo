<?php

// Cand creeaza un template nou in WebC (RT) se va face automat POST aici cu datele lui
// Apoi template-ul primit trebuie creat si in FS
// Template-urile se regasesc si in b_t_r (asta daca va fi nevoie)
// Legatura dintre RT si FS pentru template-uri se salveaza in b_t_r_f_s
// 		tot aici se vor salva informatii suplimentare, daca va fi cazul



require('DB.class.php');

class Template {

	function __construct()
	{
		$this->db_name = 'DB';
		$this->DB = new DB();

		// Transformam in array datele template-ului primite de la RT
		$this->templateData = json_decode(file_get_contents('php://input'), true);

		$this->log('from_RT', $this->templateData);

		$this->main();
	}

	function log($type, $data){
		// save log data
		file_put_contents(
			'/var/www/html/SRServices/templates_' . $type . '.log',
			date('d-m-Y H:i:s') . " ############################################################\n\n" . print_r($data, true) . "\n\n",
			FILE_APPEND
		);
	}


	function main(){
		// Preluam datele despre client
		$this->client = $this->DB->getData("SELECT
			displayNameFS,
			displayPassFS,
			displayIdCustomerFS
			FROM " . $this->db_name . ".users
			WHERE txpostcode = " . $this->templateData['CustomerId'])[0];

		// Daca avem datele necesare, trecem mai departe
		if($this->client['displayNameFS'] && $this->client['displayPassFS'] && $this->client['displayIdCustomerFS']){
			$this->manageTemplate();
		} else {
			echo 'Nu avem toate datele necesare pentru a interactiona cu FS!';
			$this->log('error', 'Nu avem toate datele necesare pentru a interactiona cu FS!');
			exit;
		}
	}

	function manageTemplate(){

		// Stabilim daca am primit un template nou sau daca unul existent a fost editat
		if($this->templateData['NewId'] == '0'){
			$newTemplate = true;
			$RouteRequestType = '1';	// add
		} else {
			$newTemplate = false;
			$RouteRequestType = '2';	// edit
		}

		// Daca template-ul este nou la FS trimitem IdRoute = 0
		if($newTemplate){

			$IdRoute = 0;

		// Daca template-ul este editat la FS trimitem IdRoute = b_t_r_f_s > id_f_s
		} else {
			$existingTemplate = $this->DB->getData("SELECT *
				FROM " . $this->db_name . ".b_t_r_f_s
				WHERE id_RT = " . $this->templateData['Id'])[0];

			if($existingTemplate){
				$IdRoute = $existingTemplate['id_f_s'];
			}
		}

		// Data actuala in GMT
		$dateObj = new DateTime('NOW', new DateTimeZone("Europe/Bucharest"));
		$dateObj->setTimezone(new DateTimeZone('GMT'));
		

		// Construim XML-ul care va fi trimis la FS

		$xml = '';

		$xml .= '<?xml version="1.0" encoding="utf-8"?>';



		// ...



		$this->log('to_FS', $xml);

		// Trimitem la FS
		$requestResult = $this->FS_request($xml);

		if($requestResult['Error']){
			$this->log('error', 'RouteRequestType: ' . $RouteRequestType . ' - user: ' . $this->client['displayNameFS'] . ' - ErrorCode: ' . $requestResult['Error']['ErrorCode']);
			exit;
		}

		// Daca template-ul este nou, facem insert in b_t_r_f_s
		if($newTemplate){

			// Cum de la FS nu primim Id-ul template-ului nou creat
			// facem o preluare a tuturor rutelor de la client/utilizator
			// si consideram Id-ul cel mai mare ca fiind cel al noului template
			$xml = '';
			


			// ...

			

			// Trimitem la FS pentru preluare template
			$FS_templates = $this->FS_request($xml);

			// Retinem ultimul IdRoute din lista primita
			if($FS_templates['SecRoute']['SecRoute']['IdRoute']){
				$activeTemplates[] = $FS_templates['SecRoute']['SecRoute'];
			} else {
				$activeTemplates = $FS_templates['SecRoute']['SecRoute'];
			}

			$lastId = 0;

			foreach($activeTemplates as $activeTemplate){

				if($activeTemplate['IdRoute'] > $lastId){
					$lastId = $activeTemplate['IdRoute'];
				}			

			}

			if(!$lastId){
				echo 'Nu s-a reusit preluare id-ului ultimei rute activate!';
				$this->log('error', 'Nu s-a reusit preluarea id-ului ultimului template creat!');
			}

			$dataToInsert[] = [
				'id_RT' => $this->templateData['Id'],
				'id_f_s' => $lastId
			];

			$this->DB->insertBulkData($this->db_name . '.b_t_r_f_s', $dataToInsert);


		// Daca template-ul este editat, facem update in b_t_r_f_s > id_RT cu noul id (NewId)
		// se pare ca id-ul din FS nu se schimba la editare template
		} else {

			$alertRunData_sql = 'UPDATE ' . $this->db_name . '.b_t_r_f_s
			SET
			id_RT = ' . $this->templateData['NewId'] . '
			WHERE id_RT = ' . $this->templateData['Id'];

			$this->DB->update($alertRunData_sql);

		}

	}


	function FS_request($xml){

		// Folosim CURL pentru a trimite la FS

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://.../ManageSecRoute');

		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
		curl_setopt($ch, CURLOPT_USERPWD, $this->client['displayNameFS'] . ':' . $this->client['displayPassFS']);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);

		$requestResponse = curl_exec($ch);

		curl_close($ch);

		$this->log('from_FS', $requestResponse);

		$requestResponseObj = new SimpleXMLElement($requestResponse);

		return json_decode(json_encode($requestResponseObj), true);
	}


}

new Template();