<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Template_RT;
use App\Models\Template_FS;
use App\Models\VehRefMsg;
use DateTime;
use DateTimeZone;
use DB;

class Routes extends Controller {

    public function templates($cuid) {
        $data['cuid'] = $cuid;
        return view('routes/templates', $data);
    }

    // Setare ruta
    public function set_route($cuid, $idVeh, $safeVehPlate, $idEquip, Request $request) {
        $data['cuid'] = $cuid;
        $data['vehId'] = $idVeh;
        $data['safeVehPlate'] = $safeVehPlate;
        $data['idEquip'] = $idEquip;
        $data['queryString'] = str_replace($request->url(), '',$request->fullUrl());
        return view('routes/set_route', $data);
    }

    // Ruta in derulare
    public function route_in_progress($cuid, $idVeh, $safeVehPlate, $idEquip, Request $request) {
        $data['cuid'] = $cuid;
        $data['vehId'] = $idVeh;
        $data['safeVehPlate'] = $safeVehPlate;
        $data['idEquip'] = $idEquip;
        $data['queryString'] = str_replace($request->url(), '',$request->fullUrl());
        return view('routes/route_in_progress', $data);
    }
    
    // Toate rutele in derulare
    public function all_route_in_progress($cuid, Request $request) {
        $data['cuid'] = $cuid;;
        $data['queryString'] = str_replace($request->url(), '',$request->fullUrl());
        return view('routes/all_route_in_progress', $data);
    }

    // Rute active
    public function active_routes($cuid, $idVeh, $safeVehPlate, $idEquip, Request $request) {
        $data['cuid'] = $cuid;
        $data['vehId'] = $idVeh;
        $data['safeVehPlate'] = $safeVehPlate;
        $data['idEquip'] = $idEquip;
        $data['queryString'] = str_replace($request->url(), '',$request->fullUrl());
        return view('routes/active_routes', $data);
    }

    // Istoric rute
    public function routes_history($cuid, $idVeh, $safeVehPlate, $idEquip, Request $request) {
        $data['cuid'] = $cuid;
        $data['vehId'] = $idVeh;
        $data['safeVehPlate'] = $safeVehPlate;
        $data['idEquip'] = $idEquip;
        $data['queryString'] = str_replace($request->url(), '',$request->fullUrl());
        return view('routes/routes_history', $data);
    }
    
    // Istoric rute - toate vehiculele
    public function all_routes_history($cuid, Request $request) {
        $data['cuid'] = $cuid;
        $data['queryString'] = str_replace($request->url(), '',$request->fullUrl());
        return view('routes/all_routes_history', $data);
    }

    // Template-uri - in functie de tipul de RT - 5 sau 6
    public function viewtemplateroutes(Request $request){

        if($request->input('idveh')){
            $RTVersion = $this->getRTVersion($request->input('idveh'));
            
            // Daca nu avem vehId inseamna ca se vine din pagina de Templates
            // si consideram RT 5 pentru a afisa toate template-urile disponibile
        } else {
            $RTVersion = 5;
        }

        $data = $request->input();

        // Preluam template-urile de la RT
        $RT_templates = $this->curl_request(5, 'viewtemplateroutes', $data);

        if($RTVersion === 5){
            echo $RT_templates;
        }

        // Iar pentru RT 6 facem verificare si pastram doar pe cele care exista in FS
        if($RTVersion === 6){

            foreach (json_decode($RT_templates, true) as $key => $RT_template) {

                $FS_template = [];

                $FS_template = Template_FS::select()
                ->where('id_rt', $RT_template['idtemplate'])
                ->first();

                if($FS_template){
                    $FS_templates[] = $RT_template;
                }

            }

            echo json_encode($FS_templates);
        }

    }

    // Stergere template -  in functie de RT
    public function deletetemplate(Request $request){

        $data = $request->input();

        $ok = false;

        // Stergem din RT
        if($this->curl_request(5, 'deleteroute', $data) === 'ERROR'){
            $ok = true;
        }

        // Stergem din FS
        $data['action'] = 'delete_template';
        if($this->curl_request(6, '', $data) === 'ERROR'){
            $ok = true;
        }

        if($ok){
            echo 'OK';
        } else {
            echo 'ERROR';
        }

    }

    // Preluare rute active
    public function viewactiveroutes(Request $request){

        $RTVersion = $this->getRTVersion($request->input('idveh'));

        if($RTVersion === 5){

            $data = $request->input();

            $routesJSON = $this->curl_request($RTVersion, 'viewactiveroutes', $data);

            echo $this->setRTVersion($routesJSON, 5);
        }

        if($RTVersion === 6){

            $data['action'] = 'view_active_routes';
            $data['idveh'] = $request->input('idveh');

            $routesJSON = $this->curl_request($RTVersion, '', $data);

            echo $this->setRTVersion($routesJSON, 6);
        }

    }

    // Preluare date ruta activa
    public function viewactiveroute(Request $request){

        $routeId = $request->input('routeId');
        $RT = (int)$request->input('RT');

        $route = [];

        if($RT === 5){

            $route = DB::select("SELECT
                btr.waypoints,
                btr.line,
                btr.polygon,
                blr.start,
                blr.stop,
                blr.id_veh
                FROM bat_live_routes blr
                JOIN bat_template_routes btr ON btr.id = blr.id_template
                WHERE blr.id = " . $routeId . " LIMIT 1")[0];

            if($route){
                $routeReturn['waypoints'] = $route['waypoints'];
                $routeReturn['line'] = $route['line'];
                $routeReturn['polygon'] = $route['polygon'];
                $routeReturn['positions'] = $this->getPositions($route['id_veh'], $route['start'], $route['stop']);
            }

        }

        if($RT === 6){

            $route = DB::select("SELECT
                btr.waypoints,
                btr.line,
                btr.polygon,
                r_fs.start,
                r_fs.stop,
                r_fs.id_veh
                FROM bat_live_routes_fs r_fs
                JOIN bat_template_routes_fs t_fs ON t_fs.id_fs = r_fs.id_template
                JOIN bat_template_routes btr ON btr.id = t_fs.id_rt
                WHERE r_fs.id = " . $routeId . " LIMIT 1")[0];

            if($route){
                $routeReturn['waypoints'] = $route['waypoints'];
                $routeReturn['line'] = $route['line'];
                $routeReturn['polygon'] = $route['polygon'];
                $routeReturn['positions'] = $this->getPositions($route['id_veh'], $route['start'], $route['stop']);
            }

        }

        if($route){
            echo json_encode($routeReturn);
        }

    }

    // Preluare pozitii in interval
    public function getPositions($id_veh, $start, $stop){

        $positions = DB::select("SELECT
            CONCAT('[', amLat, ',', amLong, ']') AS point,
            idTrigger,
            tmMsg AS date
            FROM msgPos
            WHERE idVeh = " . $id_veh . "
            AND tmMsg >= '" . $start . "'
            AND tmMsg <= '" . $stop . "'");

        if($positions){
            return json_encode($positions);
        }

    }

    // Istoric rute active
    public function viewactiverouteshistory(Request $request){

        $RTVersion = $this->getRTVersion($request->input('idveh'));

        $data = $request->input();

        if($RTVersion === 5){

            $routesJSON = $this->curl_request($RTVersion, 'viewactiverouteshistory', $data);

            echo $this->setRTVersion($routesJSON, 5);
        }

        if($RTVersion === 6){

            $data['action'] = 'view_active_routes_history';

            $routesJSON = $this->curl_request($RTVersion, '', $data);

            echo $this->setRTVersion($routesJSON, 6);
        }

    }

    // Toate rutele active
    public function viewallactiveroutes(Request $request){

        $data = $request->input();

        // Preluam rutele de la RT
        $routesRT = json_decode($this->curl_request(5, 'viewallactiveroutes', $data), true);

        if($routesRT){
            // Setam tipul de echipament RT = 5
            foreach($routesRT as $key => $routeRT){
                $routesRT[$key]['RT'] = 5;
            }
        } else {
            $routesRT = [];
        }

        // Preluam rutele de la FS
        $data['action'] = 'view_all_active_routes';
        $routesFS = json_decode($this->curl_request(6, '', $data), true);

        if(!$routesFS){
            $routesFS = [];
        }

        echo json_encode(array_merge($routesRT, $routesFS));

    }

    // Istoric pentru toate rutele active
    public function viewallactiverouteshistory(Request $request){

        $data = $request->input();

        // Preluam rutele de la RT
        $data = $request->input();
        $routesRT = json_decode($this->curl_request(5, 'viewallactiverouteshistory', $data), true);

        if($routesRT){
            // Setam tipul de echipament RT = 5
            foreach($routesRT as $key => $routeRT){
                $routesRT[$key]['RT'] = 5;
            }
        } else {
            $routesRT = [];
        }
        
        // Preluam rutele de la FS
        $data['action'] = 'view_all_active_routes_history';
        $routesFS = json_decode($this->curl_request(6, '', $data), true);

        if(!$routesFS){
            $routesFS = [];
        }

        echo json_encode(array_merge($routesRT, $routesFS));

    }

    // Activare ruta
    public function activateroute(Request $request){

        $RTVersion = $this->getRTVersion($request->input('idveh'));

        if($RTVersion === 5){

            $data = $request->input();

            // Adaugam Waypoints
            $data['waypoints'] = Template_RT::select('waypoints')
            ->where('id', $request->input('idtemplate'))
            ->first()['waypoints'];

            echo $this->curl_request($RTVersion, 'activateroute', $data);

        }

        if($RTVersion === 6){

            $dateObjStart = new DateTime(date('Y-m-d H:i:s', $request->input('start') / 1000), new DateTimeZone("GMT"));
            $dateObjStart->setTimezone(new DateTimeZone('Europe/Bucharest'));

            $dateObjStop = new DateTime(date('Y-m-d H:i:s', $request->input('stop') / 1000), new DateTimeZone("GMT"));
            $dateObjStop->setTimezone(new DateTimeZone('Europe/Bucharest'));

            $IdDevice = $this->getRT6IdDevice($request->input('idveh'));

            $template = Template_FS::select(
                'id_fs'
            )
            ->where('id_rt', $request->input('idtemplate'))
            ->first();
            
            // Daca nu gasit Id-ul template-ului pentru FS nu mergem mai departe
            if(!$template['id_fs']){
                echo 'Nu a am gasit id-ul template-ului pentru FS!';
                exit;
            }

            if($request->input('display') === 'y'){
                $HasDisplay = 1;
                $ServiceType = 5;
            } else {
                $HasDisplay = 0;
                $ServiceType = 1;
            }

            $data['action'] = 'activate_route';
            $data['IdRoute'] = $template['id_fs'];
            $data['IdDevice'] = $IdDevice;
            $data['FromDate'] = $dateObjStart->format('Y-m-d\TH:i:s');
            $data['ToDate'] = $dateObjStop->format('Y-m-d\TH:i:s');
            $data['HasDisplay'] = $HasDisplay;
            $data['ServiceType'] = $ServiceType;
            $data['RouteRequestType'] = '10';
            $data['idveh'] = $request->input('idveh');

            echo $this->curl_request($RTVersion, '', $data);

        }

    }

    // Sterdere/dezactivare ruta
    public function deleteroute(Request $request){

        $RTVersion = $this->getRTVersion($request->input('idveh'));

        if($RTVersion === 5){

            $data = $request->input();

            echo $this->curl_request($RTVersion, 'deleteroute', $data);

        }

        if($RTVersion === 6){

            $IdDevice = $this->getRT6IdDevice($request->input('idveh'));

            $data['action'] = 'deactivate_route';
            $data['IdRoute'] = $request->input('id');
            $data['IdDevice'] = $IdDevice;
            $data['idveh'] = $request->input('idveh');

            echo $this->curl_request($RTVersion, '', $data);

        }

    }

    public function curl_request($RTVersion, $path, $data){

        // Preluare rute active din RT
        if($RTVersion === 5){
            $url = 'https://.../ui/';
        }

        // Preluare rute active din FS
        if($RTVersion === 6){
            $url = 'https://...FSRequest.php';
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url . $path . '?' . http_build_query($data));

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }


    public function getRTVersion($vehId){

        $vehicleWithRT6 = VehRefMsg::select()
        ->where('idveh', $vehId)
        ->where('idProviderComm', 123)  // V LINE
        ->where('idOwnMsg', 'LIKE', 'V%')
        ->first();

        if($vehicleWithRT6){
            return 6;
        } else {
            return 5;
        }

    }

    public function getRT6IdDevice($vehId){

        $vehicleWithRT6 = VehRefMsg::select()
        ->where('idveh', $vehId)
        ->where('idProviderComm', 123)  // V LINE
        ->where('idOwnMsg', 'LIKE', 'V%')
        ->first();

        if($vehicleWithRT6){
            return str_replace('V', '', $vehicleWithRT6['idOwnMsg']);
        } else {
            die('Nu s-a gasit ID-ul device-ului!');
        }

    }

    public function setRTVersion($routesJSON, $RT){

        $routes = json_decode($routesJSON, true);

        if($routes){
            foreach($routes as $key => $routeRT){
                $routes[$key]['RT'] = $RT;
            }
        } else {
            $routes = [];
        }

        return json_encode($routes);
    }

}
