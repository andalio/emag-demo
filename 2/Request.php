<?php

// Interactiuni cu FS

require 'checkAccess.php';

if($access !== true){
	exit;
}

require('db_adv.class.php');

class Request {

	function __construct(){

		$this->db_name = 'DB';
		$this->db_adv = new db_adv();

		$this->get_data = $_GET;

		// Preluam datele despre client folosind Id-ul template-ului
		if($this->get_data['id']){

			$this->client = $this->db_adv->getData("SELECT
				displayNameFS,
				displayPassFS,
				displayIdCustomerFS
				FROM b_t_r
				JOIN users ON users.txpostcode = b_t_r.cuid
				WHERE b_t_r.id = " . $this->get_data['id'])[0];

		// Preluam datele despre client folosind Id-ul vehiculului
		} elseif($this->get_data['idveh']){

			$this->client = $this->db_adv->getData("SELECT
				displayNameFS,
				displayPassFS,
				displayIdCustomerFS
				FROM sat_vehicle
				JOIN users ON users.id = sat_vehicle.idClient
				WHERE idVeh = " . $this->get_data['idveh'])[0];

		// Preluam datele despre Id-ul de client din RT
		} elseif($this->get_data['cuid']){
			$this->client = $this->db_adv->getData("SELECT
				displayNameFS,
				displayPassFS,
				displayIdCustomerFS
				FROM users
				WHERE txpostcode = " . $this->get_data['cuid'])[0];
		}

		// Daca avem datele necesare, trecem mai departe
		if($this->client['displayNameFS'] && $this->client['displayPassFS'] && $this->client['displayIdCustomerFS']){
			// Mergem mai departe
		} else {
			echo 'Nu avem toate datele necesare pentru a interactiona cu FS!';
			$this->log('error', 'Nu avem toate datele necesare pentru a interactiona cu FS!');
			exit;
		}

		// Stabilire actiuni
		match($this->get_data['action']) {
			'get_templates' => this->get_templates(),
			'activate_route' => this->activate_route(),
			'deactivate_route' => this->deactivate_route(),
			'view_active_routes' => this->view_active_routes(),
			'view_active_routes_history' => this->view_active_routes_history(),
			'view_all_active_routes_history' => this->view_all_active_routes_history(),
			'view_all_active_routes' => this->view_all_active_routes(),
			'delete_template' => this->delete_template(),
			default => $this->log('error', $this->get_data['action'] . ' - nu exista!')
		};

	}

	function get_templates(){

		// Construim XML-ul
		$xml = '';



		// ...



		$this->log('to_FS', $xml);

		// Trimitem la FS
		$requestResult = $this->FS_request($xml);

		$this->log('from_FS', $requestResult);

		if($requestResult['Error']){
			$this->log('error', 'Get templates - user: ' . $this->client['displayNameFS'] . ' - ErrorCode: ' . $requestResult['Error']['ErrorCode']);
			echo 'ERROR';
		} else {
			echo json_encode($requestResult);
		}

	}

	function activate_route(){

		$xml = '';



		// ...



		$this->log('to_FS', $xml);

		// Trimitem la FS
		$requestResult = $this->FS_request($xml);

		$this->log('from_FS', $requestResult);

		if($requestResult['Error']){
			$this->log('error', 'Activate route - user: ' . $this->client['displayNameFS'] . ' - ErrorCode: ' . $requestResult['Error']['ErrorCode']);
			echo 'ERROR';
		} else {

			$dateObjStart = new DateTime($this->get_data['FromDate'], new DateTimeZone("Europe/Bucharest"));
			$dateObjStart->setTimezone(new DateTimeZone('GMT'));

			$dateObjStop = new DateTime($this->get_data['ToDate'], new DateTimeZone("Europe/Bucharest"));
			$dateObjStop->setTimezone(new DateTimeZone('GMT'));

			// Salvam ruta in "b_l_r_f_s"
			$dataToInsert[] = [
				'id' => $this->get_last_active_route_id(),
				'start' => $dateObjStart->format('Y-m-d H:i:s'),
				'stop' => $dateObjStop->format('Y-m-d H:i:s'),
				'display' => $this->get_data['HasDisplay'],
				'service_type' => $this->get_data['ServiceType'],
				'id_template' => $this->get_data['IdRoute'],
				'id_veh' => $this->get_data['idveh']
			];

			// Insert data
			$this->db_adv->insertBulkData('b_l_r_f_s', $dataToInsert);

			echo 'OK';

		}

	}

	function deactivate_route(){

		$xml = '';

		

		// ...



		$this->log('to_FS', $xml);

		// Trimitem la FS
		$requestResult = $this->FS_request($xml);

		$this->log('from_FS', $requestResult);

		if($requestResult['Error']){
			$this->log('error', 'Deactivate route - user: ' . $this->client['displayNameFS'] . ', IdRoute: ' . $this->get_data['IdRoute'] . ' - ErrorCode: ' . $requestResult['Error']['ErrorCode']);
			echo json_encode(['ErrorCode' => $requestResult['Error']['ErrorCode']]);
			echo 'ERROR';
		} else {

			// UPDATE "b_l_r_f_s > (stop, isDeleted)"
			$update_sql = "UPDATE b_l_r_f_s
			SET stop = '" . date('Y-m-d H:i:s') . "', isDeleted = 1
			WHERE id = " . $this->get_data['IdRoute'];

			if($this->db_adv->update($update_sql)){
				echo 'OK';
			} else {
				echo 'ERROR';
			}
		}
	}

	function get_last_active_route_id(){

		// Cum de la FS nu primim Id-ul rutei nou create
		// facem o preluare a tuturor rutelor active
		// si consideram Id-ul cel mai mare ca fiind cel al noii rute

		$xml = '';

		

		// ...



		$this->log('to_FS', $xml);

		// Trimitem la FS
		$requestResult = $this->FS_request($xml);

		$this->log('from_FS', $requestResult);

		// Setul de date difera in functie de numarul de rute
		// Daca avem o ruta sau mai multe
		if($requestResult['SecureRoute']['SecureRoute']['IdRoute']){
			$activeRoutes[] = $requestResult['SecureRoute']['SecureRoute'];
		} else {
			$activeRoutes = $requestResult['SecureRoute']['SecureRoute'];
		}

		$lastId = 0;

		foreach($activeRoutes as $activeRoute){

			if($activeRoute['IdRoute'] > $lastId){
				$lastId = $activeRoute['IdRoute'];
			}			

		}

		if($lastId > 0){
			return $lastId;
		} else {
			echo 'Nu s-a reusit preluarea id-ului ultimei rute activate!';
			$this->log('error', 'Nu s-a reusit preluarea id-ului ultimei rute activate!');
		}

	}

	function view_active_routes(){

		// SELECT din "b_l_r_f_s"

		$activeRoutes = $this->db_adv->getData("SELECT
			blrfs.display,
			blrfs.id,
			btr.name,
			blrfs.start,
			blrfs.stop,
			btr.line,
			blrfs.id_template,
			btr.polygon,
			btr.waypoints,
			blrfs.ss
			FROM b_l_r_f_s blrfs
			JOIN b_t_r_f_s btrfs ON btrfs.id_f_s = blrfs.id_template
			JOIN b_t_r btr ON btr.id = btrfs.id_RT
			WHERE id_veh = " . $this->get_data['idveh'] . " AND blrfs.isDeleted = 0
			AND blrfs.start > '" . date('Y-m-d H:i:s') . "'
			AND blrfs.stop > '" . date('Y-m-d H:i:s') . "'");

		foreach($activeRoutes as $activeRoute){

			$active_routes_data[] = [
				'display' => (int)$activeRoute['display'],
				'idlive' => $activeRoute['id'],
				'name' => $activeRoute['name'],
				'start' => $activeRoute['start'] . '.000Z',
				'stop' => $activeRoute['stop'] . '.000Z',
				'line' => $activeRoute['line'],
				'idtemplate' => $activeRoute['id_template'],
				'polygon' => $activeRoute['polygon'],
				'waypoints' => $activeRoute['waypoints'],
				'ss' => $activeRoute['ss']
			];
		}

		if($active_routes_data){
			echo json_encode($active_routes_data);
		} else {
			echo json_encode([]);
		}
	}

	function view_active_routes_from_FS(){

		$xml = '';

		

		// ...



		$this->log('to_FS', $xml);

		// Trimitem la FS
		$requestResult = $this->FS_request($xml);

		$this->log('from_FS', $requestResult);

		if($requestResult['Error']){
			$this->log('error', 'Get active routes - user: ' . $this->client['displayNameFS'] . ' - ErrorCode: ' . $requestResult['Error']['ErrorCode']);
			echo json_encode(['ErrorCode' => $requestResult['Error']['ErrorCode']]);
		} else {

			// Daca avem o ruta sau mai multe
			if($requestResult['SecureRoute']['SecureRoute']['IdRoute']){
				$activeRoutes[] = $requestResult['SecureRoute']['SecureRoute'];
			} else {
				$activeRoutes = $requestResult['SecureRoute']['SecureRoute'];
			}

			foreach($activeRoutes as $activeRoute){
				$template = $this->db_adv->getData("SELECT
					line,
					polygon,
					waypoints
					FROM b_t_r
					WHERE name = '" . $activeRoute['Name'] . "'")[0];

				$dateObjStart = new DateTime($activeRoute['FromDate'], new DateTimeZone("GMT"));
				$dateObjStart->setTimezone(new DateTimeZone('Europe/Bucharest'));

				$dateObjStop = new DateTime($activeRoute['ToDate'], new DateTimeZone("GMT"));

				// Daca ToDate (Stop) este mai mic decat Prezent, inseamna ca ruta a fost dezactivata
				// Nu stiu de ce, dar cand se dezactiveaza o ruta la ToDate se pune doar cu 2 ore in urma, nu cu 3 cum ar fi normal la GMT-ul actual
				if($dateObjStop->getTimestamp() > time()){

					$dateObjStop->setTimezone(new DateTimeZone('Europe/Bucharest'));

					$active_routes_data[] = [
						'display' => $activeRoute['HasDisplay'],
						'idlive' => $activeRoute['IdRoute'],
						'name' => $activeRoute['Name'],
						'start' => $dateObjStart->format('Y-m-d\TH:i:s') . '.000Z',
						'stop' => $dateObjStop->format('Y-m-d\TH:i:s') . '.000Z',
						'line' => $template['line'],
						'idtemplate' => $template['IdRoute'],
						'polygon' => $template['polygon'],
						'waypoints' => $template['waypoints'],
						'ss' => 0
					];

				}
				
			}

			echo json_encode($active_routes_data);
		}

	}

	function view_active_routes_history(){

		// SELECT din "b_l_r_f_s"

		$activeRoutes = $this->db_adv->getData("SELECT
			blrfs.display,
			blrfs.id,
			btr.name,
			blrfs.start,
			blrfs.stop,
			btr.line,
			blrfs.id_template,
			btr.polygon,
			btr.waypoints,
			blrfs.ss
			FROM b_l_r_f_s blrfs
			JOIN b_t_r_f_s btrfs ON btrfs.id_f_s = blrfs.id_template
			JOIN b_t_r btr ON btr.id = btrfs.id_RT
			WHERE id_veh = " . $this->get_data['idveh'] . "
			-- AND blrfs.isDeleted = 0
			AND blrfs.start >= '" . date('Y-m-d H:i:s', $this->get_data['from'] / 1000) . "'
			AND blrfs.stop <= '" . date('Y-m-d H:i:s', $this->get_data['to'] / 1000) . "'");

		foreach($activeRoutes as $activeRoute){

			$active_routes_data[] = [
				'display' => (int)$activeRoute['display'],
				'idlive' => $activeRoute['id'],
				'name' => $activeRoute['name'],
				'start' => $activeRoute['start'] . '.000Z',
				'stop' => $activeRoute['stop'] . '.000Z',
				'line' => $activeRoute['line'],
				'idtemplate' => $activeRoute['id_template'],
				'polygon' => $activeRoute['polygon'],
				'waypoints' => $activeRoute['waypoints'],
				'ss' => $activeRoute['ss']
			];
		}

		if($active_routes_data){
			echo json_encode($active_routes_data);
		} else {
			echo json_encode([]);
		}

	}

	function view_all_active_routes_history(){

		// SELECT din "b_l_r_f_s"

		$activeRoutes = $this->db_adv->getData("SELECT
			blrfs.display,
			blrfs.id,
			btr.name,
			blrfs.start,
			blrfs.stop,
			btr.line,
			blrfs.id_template,
			btr.polygon,
			btr.waypoints,
			blrfs.ss,
			blrfs.id_veh,
			sv.idLabelVeh
			FROM b_l_r_f_s blrfs
			JOIN b_t_r_f_s btrfs ON btrfs.id_f_s = blrfs.id_template
			JOIN b_t_r btr ON btr.id = btrfs.id_RT
			JOIN sat_vehicle sv ON sv.idVeh = blrfs.id_veh
			WHERE btr.cuid = " . $this->get_data['cuid'] . "
			-- AND blrfs.isDeleted = 0
			AND blrfs.start >= '" . date('Y-m-d H:i:s', $this->get_data['from'] / 1000) . "'
			AND blrfs.stop <= '" . date('Y-m-d H:i:s', $this->get_data['to'] / 1000) . "'");

		foreach($activeRoutes as $activeRoute){

			$active_routes_data[] = [
				'display' => (int)$activeRoute['display'],
				'idEquip' => '',
				'idlive' => $activeRoute['id'],
				'name' => $activeRoute['name'],
				'safeVehPlate' => $activeRoute['idLabelVeh'],
				'ss' => $activeRoute['ss'],
				'start' => $activeRoute['start'] . '.000Z',
				'stop' => $activeRoute['stop'] . '.000Z',
				'vehId' => $activeRoute['id_veh'],
				'RT' => 6
			];
		}

		if($active_routes_data){
			echo json_encode($active_routes_data);
		} else {
			echo json_encode([]);
		}

	}

	function view_all_active_routes(){

		// SELECT din "b_l_r_f_s"

		$activeRoutes = $this->db_adv->getData("SELECT
			blrfs.display,
			blrfs.id,
			btr.name,
			blrfs.start,
			blrfs.stop,
			btr.line,
			blrfs.id_template,
			btr.polygon,
			btr.waypoints,
			blrfs.ss
			FROM b_l_r_f_s blrfs
			JOIN b_t_r_f_s btrfs ON btrfs.id_f_s = blrfs.id_template
			JOIN b_t_r btr ON btr.id = btrfs.id_RT
			WHERE blrfs.isDeleted = 0 AND btr.cuid = " . $this->get_data['cuid']);

		foreach($activeRoutes as $activeRoute){

			$active_routes_data[] = [
				'display' => (int)$activeRoute['display'],
				'idlive' => $activeRoute['id'],
				'name' => $activeRoute['name'],
				'start' => $activeRoute['start'] . '.000Z',
				'stop' => $activeRoute['stop'] . '.000Z',
				'line' => $activeRoute['line'],
				'idtemplate' => $activeRoute['id_template'],
				'polygon' => $activeRoute['polygon'],
				'waypoints' => $activeRoute['waypoints'],
				'ss' => $activeRoute['ss']
			];
		}

		if($active_routes_data){
			echo json_encode($active_routes_data);
		} else {
			echo json_encode([]);
		}

	}

	function delete_template(){

		// Preluam id-ul de FS
		$id_f_s = $this->db_adv->getData("SELECT id_f_s
			FROM b_t_r_f_s
			WHERE id_RT = " . $this->get_data['id'])[0]['id_f_s'];

		$xml = '';

		

		// ...



		$this->log('to_FS', $xml);

		// Trimitem la FS
		$requestResult = $this->FS_request($xml);

		$this->log('from_FS', $requestResult);

		if($requestResult['Error']){
			$this->log('error', 'Delete template from FS (id_RT: ' . $this->get_data['id'] . ') - ErrorCode: ' . $requestResult['Error']['ErrorCode']);
			// echo json_encode(['ErrorCode' => $requestResult['Error']['ErrorCode']]);
			echo 'ERROR';
		} else {
			// echo json_encode(['Success' => 'Ok']);
			echo 'OK';
		}
	}

	function FS_request($xml){

		// Folosim CURL pentru a trimite la FS

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://.../ManageSecRoute');

		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
		curl_setopt($ch, CURLOPT_USERPWD, $this->client['displayNameFS'] . ':' . $this->client['displayPassFS']);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);

		$requestResponse = curl_exec($ch);

		curl_close($ch);

		$requestResponseObj = new SimpleXMLElement($requestResponse);

		return json_decode(json_encode($requestResponseObj), true);
	}

	function log($type, $data){
		// save log data
		file_put_contents(
			'/var/www/html/SRServices/templates_' . $type . '.log',
			date('d-m-Y H:i:s') . " ############################################################\n\n" . print_r($data, true) . "\n\n",
			FILE_APPEND
		);
	}

}

new Request();